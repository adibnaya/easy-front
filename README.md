### Bynet Control Panel
##### Scripts
##### `yarn start - run the application`
##### `yarn build - build the application and creats productyion ready build folder`
##### `yarn test - run the application test`
##### `yarn format - format the application code using Prettier`
##### `yarn lint - run ESLint rules on the application code`

##### CI/CD
* The application contains CI/CD configuration for automatic build and deployment on the cloud.
* **Every push to the Development branch** will run the CI pipeline, meaning the following scripts:
    * install
    * test
    * build - Will produce the build folder as artifact
* **Every push to the master branch** will run the following script in addition to the CI scripts:
    * deploy - Publish the application to AWS.
##### Environments 
#####    
* Test - https://dev.dy0ihorvhkcpd.amplifyapp.com/
##### State Management
* Redux Toolkit 
