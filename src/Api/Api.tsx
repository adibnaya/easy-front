import axios from 'axios'
import * as https from 'https'

// TODO - remove httpsAgent after we implement valid certificate
const httpsAgent = new https.Agent({ rejectUnauthorized: false })

const api = axios.create({
    httpsAgent,
    baseURL: 'http://web-api.local.easyapi.com',
    // baseURL: 'https://web-api.easyapi.com:9443',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
    },
})

api.interceptors.request.use((config) => {
    const token = localStorage.getItem('token')
    if (token) {
        config.headers.Authorization = `Bearer ${token}`
    }
    return config
})

// Handle Errors
api.interceptors.response.use(
    (res) => res,
    function (error) {
        // Do something with response error
        // console.error('Error:', error.response)
        const { status } = error.response
        if (status === 401) {
            localStorage.removeItem('token')
        }
        return Promise.reject(error)
    }
)

export default api
