import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        appContainer: {
            width: '100%',
            height: '100%',
        },
    })
)

export default useStyles
