import React, { useEffect } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import Login from '../Containers/Login/Login'
import DrawerAndBar from '../Containers/DrawerAndBar/DrawerAndBar'
import { RootState } from '../Redux/Store'
import useStyles from './App.style'
import { setIsAuthenticated } from '../Containers/Login/LoginReducer'

const App = () => {
    const dispatch = useDispatch()
    useEffect(() => {
        const token = localStorage.getItem('token')
        dispatch(setIsAuthenticated(!!token))
    }, [dispatch])

    const { isAuthenticated } = useSelector((state: RootState) => state.user)
    const classes = useStyles()

    return (
        <div className={classes.appContainer}>
            <Switch>
                <Route path="/login">
                    <Login />
                </Route>
                <PrivateRoute
                    isAuthenticated={isAuthenticated}
                    path="/solutions/:solutionId"
                    // isExact
                >
                    <DrawerAndBar />
                </PrivateRoute>
            </Switch>
        </div>
    )
}

interface PrivateRouteProps {
    children: React.ReactNode
    isAuthenticated: boolean
    path?: string
    isExact?: boolean
}

// A wrapper for <Route> that redirects to the login
// screen if you're not yet authenticated.
const PrivateRoute = ({
    children,
    isAuthenticated,
    ...rest
}: PrivateRouteProps) => {
    return (
        <Route
            {...rest}
            render={({ location }) => {
                return isAuthenticated ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: location },
                        }}
                    />
                )
            }}
        />
    )
}

export default App
