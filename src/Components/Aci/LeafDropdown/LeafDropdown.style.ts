import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            width: 'calc(100% - 100px)',
            height: 713,
            padding: '30px 50px',
            display: 'flex',
            flexDirection: 'column',
            backgroundColor: theme.colors.white,
            borderBottomRightRadius: 3,
            borderBottomLeftRadius: 3,
            boxShadow: '4px 7px 29px #29292E4A',
            justifyContent: 'space-between',
        },
        topContainer: {},
        bodyContainer: {
            width: 283,
            display: 'flex',
            justifyContent: 'flex-start',
            flexDirection: 'column',
            flex: 1,
            padding: '20px 0',
        },
        formField: {
            marginTop: 50,
        },
        titleText: {
            marginTop: 10,
            color: '#8C8C97',
        },
        label: {
            color: '#A3A5BD', // #40414E
        },
        searchContainer: {
            width: 283,
            marginTop: 10,
        },
        bottomContainer: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: '100%',
        },
        popper: {
            padding: 20,
        },
    })
)
export default useStyles
