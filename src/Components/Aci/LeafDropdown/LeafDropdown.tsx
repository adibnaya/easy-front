import React, {useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {Box} from '@material-ui/core'
import {useTranslation} from 'react-i18next'
import useStyles from './LeafDropdown.style'
import SelectAutocomplete from '../../SelectAutocomplete/SelectAutocomplete'
import TextBox from '../../TextBox/TextBox'
import {AciLeaf} from '../../../Containers/Aci/Models/AciModels'
import {Option} from '../../SearchAutocomplete/SearchAutocomplete'
import {RootState} from "../../../Redux/Store";
import {getSolutionLeafs} from "../../../Containers/Aci/AciThunks";

interface LeafDropdownProps {
    getLeafInterfaces: (leafId: string, podId: string) => void
    setLeaf: (val: AciLeaf) => void
}

const LeafDropdown = (props: LeafDropdownProps) => {
    const {
        getLeafInterfaces,
        setLeaf,
    } = props

    const solutionLeafs = useSelector((state: RootState) => state.aci.solutionLeafs)
    const leafId = useSelector((state: RootState) => state.aci.leaf.id)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getSolutionLeafs())
    }, [dispatch])

    const validatePageValues = () => {
        const isValid = !(
            leafId === ''
        )
        console.log(isValid);
    }


    const handleSelectLeafId = (selectedOption: Option) => {
        let leaf: AciLeaf
        if (selectedOption) {
            leaf = solutionLeafs && solutionLeafs[selectedOption.key]
            if (leaf) {
                setLeaf(leaf)
                getLeafInterfaces(leaf.id, leaf.pod_id)
            }
        } else {
            setLeaf({id: '', name: '', pod_id: ''})
        }
        validatePageValues()
    }

    const classes = useStyles()
    const {t} = useTranslation()
    const selectedLeafIndex = solutionLeafs.findIndex(
        (l) => l && l.id === leafId
    )

    useEffect(() => {
        validatePageValues()
    })

    return (

            <Box component="div" className={classes.bodyContainer}>

                <Box component="div" className={classes.formField}>
                    <TextBox variant="body2" className={classes.label}>
                        {t('leafDropdown.title')}
                    </TextBox>
                    <Box component="div" className={classes.searchContainer}>
                        <SelectAutocomplete
                            selectedOptionIndex={
                                selectedLeafIndex >= 0
                                    ? selectedLeafIndex
                                    : undefined
                            }
                            onOptionClicked={handleSelectLeafId}
                            data={solutionLeafs.map(
                                (leaf, leafIndex): Option => ({
                                    key: leafIndex,
                                    title: leaf ? leaf.name : '',
                                })
                            )}
                            placeholder={t(
                                'leafDropdown.placeholder'
                            )}
                            validateFunc={(value) => value !== undefined}
                        />
                    </Box>
                </Box>
            </Box>
    )
}

export default LeafDropdown
