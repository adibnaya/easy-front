import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        modal: {
            display: 'flex',
            justifyContent: 'center',
        },
        container: {
            position: 'absolute',
            top: 52,
            minWidth: 1200,
            opacity: 1,
            '&:focus': {
                border: 0,
                outline: 'none',
            },
        },
        tab: {
            height: 57,
            opacity: 1,
            color: '#CFCFCF',
            borderBottomRightRadius: 50,
            borderTopRightRadius: 50,
            boxShadow: '10px 0px 16px -2px #A3A5BD30',
        },
        selectedTab: {
            opacity: 1,
            color: theme.colors.white,
            backgroundColor: '#67B1FA',
            borderBottomRightRadius: 50,
            borderTopRightRadius: 50,
            boxShadow: '10px 0px 16px -2px #004F9D30',
            '&:disabled': {
                opacity: 1,
            },
        },
        finishTab: {
            height: 57,
            opacity: 1,
            color: '#4497E9',
            backgroundColor: '#C0DCFA',
            borderBottomRightRadius: 50,
            borderTopRightRadius: 50,
            boxShadow: '10px 0px 16px -2px #004F9D30',
        },
        lastTab: {
            height: 57,
            opacity: 1,
            color: '#CFCFCF',
            boxShadow: '10px 0px 16px -2px #A3A5BD30',
        },
        lastSelectedTab: {
            opacity: 1,
            color: theme.colors.white,
            backgroundColor: '#67B1FA',
            '&:disabled': {
                opacity: 1,
            },
        },
        firstTab: {
            zIndex: 3,
        },
        secondTab: {
            zIndex: 2,
        },
        thirdTab: {
            zIndex: 1,
        },
        blaRoot: {
            height: 29,
            backgroundColor: '#F4F5FC',
            padding: '0 !important',
            borderRadius: 200,
            border: '1px solid transparent',
            '&:hover': {
                border: '1px solid #D9DAE4',
            },
        },
        blaOptions: {
            '& li': {
                padding: '6px 0 0 15px',
                backgroundColor: theme.colors.selectBackground,
                minHeight: '23px',
            },
            '& li[data-focus="true"]': {
                backgroundColor: theme.colors.secondary,
                color: theme.colors.selectTextSelected,
                cursor: 'pointer',
            },
            '& li:active': {
                backgroundColor: theme.colors.primary,
                color: theme.colors.selectTextSelected,
            },
            '& li[aria-selected="true"]': {
                backgroundColor: theme.colors.primary,
                color: theme.colors.selectTextSelected,
            },
        },
        blaInput: {
            height: 29,
            color: '#8C8C97',
            padding: '0 20px 0 15px !important',
            fontFamily: theme.fonts.primaryText,
        },
        blaNotchedOutline: {
            border: 0,
        },
    })
)

export default useStyles
