import React from 'react'
import Modal from '@material-ui/core/Modal'
import Tab from '@material-ui/core/Tab'
import useStyles from './AddStandardEndPiontModal.style'
import TabsContainer from './TabsContainer/TabsContainer'
import TabContent from './TabContent/TabContent'
import TabPanel from './TabPanel/TabPanel'
import EndPointAndLeaf from './EndPointAndLeaf/EndPointAndLeaf'
import Interface from './Interface/Interface'
import Policy from './Policy/Policy'
import {
    CreateAaepPostReq,
    AciLeaf,
    CreatePolicyPostReq,
    InterfaceData,
} from '../../Containers/Aci/Models/AciModels'

const VARIABLES = {
    steps: [
        { id: 0, name: 'End Point & Leaf' },
        { id: 1, name: 'Interface' },
        { id: 2, name: 'Policy' },
    ],
}

interface Props {
    solutionLeafs: AciLeaf[]
    solutionPolicies: { name: string }[]
    solutionAaeps: { name: string }[]
    wizardEndpointName: string
    setWizardEndpointName: (val: string) => void
    leafId: string
    setLeaf: (val: AciLeaf) => void
    wizardAaep: string
    setAttachableAccessEntityProfile: (val: string) => void
    wizardInterfaceName: string
    setWizardInterfaceName: (val: string) => void
    interfacePolicyGroupName: string
    policySelectedTab: number
    setPolicySelectedTab: (val: number) => void
    setInterfacePolicyGroupName: (val: string) => void
    connectedToSwitch: boolean
    setConnectedToSwitch: (val: boolean) => void
    wizardPolicyName: string
    setWizardPolicyName: (val: string) => void
    modalOpen: boolean
    toggleModal: () => void

    isEpAndLeafValid: boolean
    setIsEpAndLeafValid: (val: boolean) => void
    isInterfaceValid: boolean
    setIsInterfaceValid: (val: boolean) => void
    isPolicyValid: boolean
    setIsPolicyValid: (val: boolean) => void

    getLeafInterfaces: (leafId: string, podId: string) => void
    wizardLeaf: AciLeaf
    leafInterfaces: InterfaceData[]
    dispatch: any
    createAaepResponse: string
    createAaep: (aaep: CreateAaepPostReq) => void
    createPolicy: (policy: CreatePolicyPostReq) => void
    onDone: () => void
}

const AddStandardEndPointModal = (props: Props) => {
    const {
        modalOpen,
        toggleModal,
        isEpAndLeafValid,
        setIsEpAndLeafValid,
        isInterfaceValid,
        setIsInterfaceValid,
        isPolicyValid,
        setIsPolicyValid,
        solutionLeafs,
        solutionPolicies,
        solutionAaeps,
        wizardEndpointName,
        setWizardEndpointName,
        leafId,
        setLeaf,
        policySelectedTab,
        setPolicySelectedTab,
        wizardInterfaceName,
        setWizardInterfaceName,
        interfacePolicyGroupName,
        setInterfacePolicyGroupName,
        connectedToSwitch,
        setConnectedToSwitch,
        wizardAaep,
        setAttachableAccessEntityProfile,
        wizardPolicyName,
        setWizardPolicyName,
        getLeafInterfaces,
        wizardLeaf,
        leafInterfaces,
        dispatch,
        createAaep,
        createPolicy,
        createAaepResponse,
        onDone,
    } = props
    const classes = useStyles()
    const [stepIndex, setStepIndex] = React.useState(0)

    const a11yProps = (index: any) => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        }
    }

    const handleTabChange = (
        event: React.ChangeEvent<any>,
        newValue: number
    ) => {
        if (newValue === 0) {
            setStepIndex(newValue)
        } else if (newValue === 1 && isEpAndLeafValid) {
            setStepIndex(newValue)
        } else if (newValue === 2 && isInterfaceValid) {
            setStepIndex(newValue)
        }
    }
    const handleNextStep = () => {
        if (stepIndex < VARIABLES.steps.length) {
            setStepIndex(stepIndex + 1)
        }
    }
    const handlePrevStep = () => {
        if (stepIndex > 0) setStepIndex(stepIndex - 1)
    }

    return (
        <div>
            <Modal
                className={classes.modal}
                open={modalOpen}
                onClose={toggleModal}
            >
                <div className={classes.container}>
                    <TabsContainer
                        handleTabsChange={handleTabChange}
                        value={stepIndex}
                    >
                        <Tab
                            onClick={() => {}}
                            disableRipple
                            classes={{
                                root: `${classes.firstTab} ${
                                    stepIndex > VARIABLES.steps[0].id
                                        ? classes.finishTab
                                        : classes.tab
                                }`,
                                selected: classes.selectedTab,
                            }}
                            label={
                                <TabContent
                                    index={VARIABLES.steps[0].id}
                                    tabsValue={stepIndex}
                                    title={VARIABLES.steps[0].name}
                                    isDone={isEpAndLeafValid}
                                />
                            }
                            {...a11yProps(0)}
                        />

                        <Tab
                            onClick={() => {}}
                            disableRipple
                            classes={{
                                root: `${classes.secondTab} ${
                                    stepIndex > VARIABLES.steps[1].id
                                        ? classes.finishTab
                                        : classes.tab
                                }`,
                                selected: classes.selectedTab,
                            }}
                            label={
                                <TabContent
                                    index={VARIABLES.steps[1].id}
                                    tabsValue={stepIndex}
                                    title={VARIABLES.steps[1].name}
                                    isDone={isInterfaceValid}
                                />
                            }
                            {...a11yProps(1)}
                        />
                        <Tab
                            onClick={() => {}}
                            disableRipple
                            classes={{
                                root: `${classes.thirdTab} ${
                                    stepIndex > VARIABLES.steps[2].id
                                        ? classes.finishTab
                                        : classes.lastTab
                                }`,
                                selected: classes.lastSelectedTab,
                            }}
                            label={
                                <TabContent
                                    index={VARIABLES.steps[2].id}
                                    tabsValue={stepIndex}
                                    title={VARIABLES.steps[2].name}
                                    isDone={isPolicyValid}
                                />
                            }
                            {...a11yProps(2)}
                        />
                    </TabsContainer>

                    <TabPanel value={stepIndex} index={0}>
                        <EndPointAndLeaf
                            closeModal={toggleModal}
                            goForward={handleNextStep}
                            solutionLeafs={solutionLeafs}
                            wizardEndpointName={wizardEndpointName}
                            setWizardEndpointName={setWizardEndpointName}
                            leafId={leafId}
                            setLeaf={setLeaf}
                            isEpAndLeafValid={isEpAndLeafValid}
                            setIsEpAndLeafValid={setIsEpAndLeafValid}
                            getLeafInterfaces={getLeafInterfaces}
                        />
                    </TabPanel>
                    <TabPanel value={stepIndex} index={1}>
                        <Interface
                            dispatch={dispatch}
                            title={`Interfaces of Leaf ${wizardLeaf?.name}`}
                            data={leafInterfaces}
                            closeModal={toggleModal}
                            goBack={handlePrevStep}
                            goForward={handleNextStep}
                            wizardInterfaceName={wizardInterfaceName}
                            setWizardInterfaceName={setWizardInterfaceName}
                            isInterfaceValid={isInterfaceValid}
                            setIsInterfaceValid={setIsInterfaceValid}
                        />
                    </TabPanel>
                    <TabPanel value={stepIndex} index={2}>
                        <Policy
                            solutionPolicies={solutionPolicies}
                            solutionAaeps={solutionAaeps}
                            closeModal={toggleModal}
                            goBack={handlePrevStep}
                            finish={() => {
                                setStepIndex(0)
                                onDone()
                            }}
                            wizardAaep={wizardAaep}
                            setAttachableAccessEntityProfile={
                                setAttachableAccessEntityProfile
                            }
                            interfacePolicyGroupName={interfacePolicyGroupName}
                            setInterfacePolicyGroupName={
                                setInterfacePolicyGroupName
                            }
                            wizardPolicyName={wizardPolicyName}
                            setWizardPolicyName={setWizardPolicyName}
                            policySelectedTab={policySelectedTab}
                            setPolicySelectedTab={setPolicySelectedTab}
                            connectedToSwitch={connectedToSwitch}
                            setConnectedToSwitch={setConnectedToSwitch}
                            createAaep={createAaep}
                            createAaepResponse={createAaepResponse}
                            createPolicy={createPolicy}
                            isPolicyValid={isPolicyValid}
                            setIsPolicyValid={setIsPolicyValid}
                        />
                    </TabPanel>
                </div>
            </Modal>
        </div>
    )
}

export default AddStandardEndPointModal
