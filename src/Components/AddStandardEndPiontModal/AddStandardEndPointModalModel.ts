import { Policy } from '../../Containers/Aci/Models/AciModels'

export type AddStandardEndpoint = {
    endPointAndLeaf?: EndPointAndLeaf
    interface?: Interface
    policy?: Policy
}

export type EndPointAndLeaf = {
    endpointName?: string
    leafId?: string
}

export type Interface = {
    leafInterfaces?: LeafInterface[]
}

export type LeafInterface = {
    interface?: string
    description?: string
}
