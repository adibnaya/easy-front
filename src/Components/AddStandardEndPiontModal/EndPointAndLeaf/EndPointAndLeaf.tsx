import React, { useState, useEffect } from 'react'
import { Box } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import TooltipInput from '../../TooltipInput/TooltipInput'
import AppButton from '../../Button/Button'
import useStyles from './EndPointAndLeaf.style'
import SelectAutocomplete from '../../SelectAutocomplete/SelectAutocomplete'
import TextBox from '../../TextBox/TextBox'
import { AciLeaf } from '../../../Containers/Aci/Models/AciModels'
import { Option } from '../../SearchAutocomplete/SearchAutocomplete'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'

interface EndPointAndLeafProps {
    closeModal: () => void
    goForward: () => void
    solutionLeafs: AciLeaf[]
    selectedLeaf?: AciLeaf
    wizardEndpointName: string
    setWizardEndpointName: (val: string) => void
    leafId: string
    setLeaf: (val: AciLeaf) => void
    isEpAndLeafValid: boolean
    setIsEpAndLeafValid: (val: boolean) => void
    getLeafInterfaces: (leafId: string, podId: string) => void
}

const EndPointAndLeaf = (props: EndPointAndLeafProps) => {
    const {
        closeModal,
        isEpAndLeafValid,
        setIsEpAndLeafValid,
        getLeafInterfaces,
        goForward,
        solutionLeafs,
        wizardEndpointName,
        setWizardEndpointName,
        leafId,
        setLeaf,
    } = props

    const [
        showEndpointNameInputPopper,
        setshowEndpointNameInputPopper,
    ] = useState(false)

    const validateEndpointNameValue = (value: string): boolean => {
        if (value !== undefined) {
            return value.match(/^[a-zA-Z0-9]{5,30}$/i) !== null
        }

        return false
    }

    const validatePageValues = () => {
        const isValid = !(
            leafId === '' || !validateEndpointNameValue(wizardEndpointName)
        )
        setIsEpAndLeafValid(isValid)
    }

    const handleEndpointNameInputChange = (value) => {
        setshowEndpointNameInputPopper(
            !validateEndpointNameValue(value) && value.length > 0
        )
        setWizardEndpointName(value)
        validatePageValues()
    }

    const handleSelectLeafId = (selectedOption: Option) => {
        let leaf: AciLeaf
        if (selectedOption) {
            leaf = solutionLeafs && solutionLeafs[selectedOption.key]
            if (leaf) {
                setLeaf(leaf)
                getLeafInterfaces(leaf.id, leaf.pod_id)
            }
        } else {
            setLeaf({ id: '', name: '', pod_id: '' })
        }
        validatePageValues()
    }

    const classes = useStyles()
    const { t } = useTranslation()
    const selectedLeafIndex = solutionLeafs.findIndex(
        (l) => l && l.id === leafId
    )

    useEffect(() => {
        validatePageValues()
    })

    return (
        <Box component="div" className={classes.container}>
            <Box component="div" className={classes.topContainer}>
                <Box component="div">
                    <TextBox variant="h3" display="inline">
                        {`${t('dse.wizard.endPointAndLeaf.title')} `}
                    </TextBox>
                    <TextBox variant="h4" display="inline">
                        {t('dse.wizard.endPointAndLeaf.subtitle')}
                    </TextBox>
                </Box>
                <TextBox variant="body2" className={classes.titleText}>
                    {t('dse.wizard.endPointAndLeaf.description')}
                </TextBox>
            </Box>

            <Box component="div" className={classes.bodyContainer}>
                <Box component="div" className={classes.formField}>
                    <TextBox variant="body2" className={classes.label}>
                        {t('dse.wizard.endPointAndLeaf.field1.title')}
                    </TextBox>
                    <Box component="div" className={classes.searchContainer}>
                        <TooltipInput
                            renderPopper={() => <InputPopper />}
                            inputMaxLength={30}
                            open={showEndpointNameInputPopper}
                            popperPlacement="right"
                            inputPlaceHolder={t(
                                'dse.wizard.endPointAndLeaf.field1.placeholder'
                            )}
                            handleInputChange={(e) =>
                                handleEndpointNameInputChange(e.target.value)
                            }
                            inputValue={wizardEndpointName}
                            validateFunc={(value) =>
                                validateEndpointNameValue(value)
                            }
                        />
                    </Box>
                </Box>

                <Box component="div" className={classes.formField}>
                    <TextBox variant="body2" className={classes.label}>
                        {t('dse.wizard.endPointAndLeaf.field2.title')}
                    </TextBox>
                    <Box component="div" className={classes.searchContainer}>
                        <SelectAutocomplete
                            selectedOptionIndex={
                                selectedLeafIndex >= 0
                                    ? selectedLeafIndex
                                    : undefined
                            }
                            onOptionClicked={handleSelectLeafId}
                            data={solutionLeafs.map(
                                (leaf, leafIndex): Option => ({
                                    key: leafIndex,
                                    title: leaf ? leaf.name : '',
                                })
                            )}
                            placeholder={t(
                                'dse.wizard.endPointAndLeaf.field2.placeholder'
                            )}
                            validateFunc={(value) => value !== undefined}
                        />
                    </Box>
                </Box>
            </Box>

            <Box component="div" className={classes.bottomContainer}>
                <AppButton data-outline onClick={closeModal}>
                    {t(
                        'dse.wizard.endPointAndLeaf.bottomContainerButtons.close'
                    )}
                </AppButton>
                <AppButton disabled={!isEpAndLeafValid} onClick={goForward}>
                    {t(
                        'dse.wizard.endPointAndLeaf.bottomContainerButtons.next'
                    )}
                    <NavigateNextIcon />
                </AppButton>
            </Box>
        </Box>
    )
}

export const InputPopper = () => {
    const classes = useStyles()
    const { t } = useTranslation()
    const popperText1: string = t('dse.wizard.endPointAndLeaf.popperText1')
    const popperText2: string = t('dse.wizard.endPointAndLeaf.popperText2')
    return (
        <Box component="div" className={classes.popper}>
            {[popperText1, popperText2].map((text, textIndex) => {
                return (
                    <TextBox
                        key={textIndex}
                        variant="body2"
                        className={classes.label}
                    >
                        {text}
                    </TextBox>
                )
            })}
        </Box>
    )
}

export default EndPointAndLeaf
