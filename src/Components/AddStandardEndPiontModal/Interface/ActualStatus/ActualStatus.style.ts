import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        actualStatusContainer: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
            paddingLeft: 16,
            paddingRight: 16,
        },
        icon: {
            marginRight: 11,
        },
    })
)

export default useStyles
