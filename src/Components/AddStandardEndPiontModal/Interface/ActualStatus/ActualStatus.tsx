import React from 'react'
import { ReactComponent as CircleBlueIcon } from '../../../../Assets/circle_blue.svg'
import TextBox from '../../../TextBox/TextBox'
import { ReactComponent as CircleBlueEmptyIcon } from '../../../../Assets/circle_blue_empty.svg'
import useStyles from './ActualStatus.style'

interface Props {
    value: string
}

const ActualStatus = ({ value }: Props) => {
    const classes = useStyles()
    if (value === 'up') {
        return (
            <div className={classes.actualStatusContainer}>
                <CircleBlueIcon className={classes.icon} />
                <TextBox variant="body2" display="inline">
                    Up
                </TextBox>
            </div>
        )
    }
    return (
        <div className={classes.actualStatusContainer}>
            <CircleBlueEmptyIcon className={classes.icon} />
            <TextBox variant="body2" display="inline">
                Down
            </TextBox>
        </div>
    )
}

export default ActualStatus
