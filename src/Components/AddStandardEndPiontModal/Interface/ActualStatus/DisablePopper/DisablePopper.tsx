import React from 'react'
import useStyles from './DisablePopper.style'
import TextBox from '../../../../TextBox/TextBox'
import AppButton from '../../../../Button/Button'

interface Props {
    closePopper: () => void
    disableFunc: () => void
}

const DisablePopper = ({ closePopper, disableFunc }: Props) => {
    const classes = useStyles()
    return (
        <div className={classes.disablePopperContainer}>
            <TextBox variant="body2">Disable this interface?</TextBox>
            <div className={classes.buttonsContainer}>
                <AppButton
                    className={classes.textButton}
                    onClick={closePopper}
                    data-outline
                >
                    Close
                </AppButton>
                <AppButton onClick={disableFunc} className={classes.textButton}>
                    Yes, Disable
                </AppButton>
            </div>
        </div>
    )
}

export default DisablePopper
