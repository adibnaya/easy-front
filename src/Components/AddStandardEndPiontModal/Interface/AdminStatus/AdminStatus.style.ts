import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        adminStatusContainer: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        enableText: {
            marginRight: 10,
        },
    })
)

export default useStyles
