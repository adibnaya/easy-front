import React from 'react'
import TextBox from '../../../TextBox/TextBox'
import useStyles from './AdminStatus.style'
import TooltipText from '../../../TooltipText/TooltipText'
import DisablePopper from '../ActualStatus/DisablePopper/DisablePopper'

interface Props {
    value: string
    disableFunction: () => void
}

const AdminStatus = ({ value, disableFunction }: Props) => {
    const classes = useStyles()
    if (value === 'up') {
        return (
            <div className={classes.adminStatusContainer}>
                <TextBox
                    variant="body2"
                    display="inline"
                    className={classes.enableText}
                >
                    Enabled
                </TextBox>
                <TooltipText
                    underline
                    popperPlacement="top"
                    textValue="Disable"
                    renderPopper={(closeFunc) => (
                        <DisablePopper
                            closePopper={closeFunc}
                            disableFunc={disableFunction}
                        />
                    )}
                />
            </div>
        )
    }
    return (
        <div className={classes.adminStatusContainer}>
            <TextBox variant="body2" display="inline">
                Disabled
            </TextBox>
        </div>
    )
}

export default AdminStatus
