import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            width: 'calc(100% - 100px)',
            height: 713,
            padding: '30px 50px',
            display: 'flex',
            flexDirection: 'column',
            backgroundColor: theme.colors.white,
            borderBottomRightRadius: 3,
            borderBottomLeftRadius: 3,
            boxShadow: '4px 7px 29px #29292E4A',
        },
        primaryTitle: {
            marginBottom: 10,
        },
        descContainer: {
            marginTop: 10,
        },
        titleText: {
            color: '#8C8C97',
            lineHeight: '20px',
        },
        buttonsRow: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            position: 'absolute',
            bottom: 30,
            width: 'calc(100% - 100px)',
        },
    })
)
export default useStyles
