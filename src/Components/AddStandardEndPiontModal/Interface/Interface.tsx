import React, { useEffect } from 'react'
import { Box } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import useStyles from './Interface.style'
import AppButton from '../../Button/Button'
import InterfaceTable from './InterfaceTable/InterfaceTable'
import TextBox from '../../TextBox/TextBox'
import { InterfaceData } from '../../../Containers/Aci/Models/AciModels'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore'

interface InterfaceProps {
    closeModal: () => void
    goBack: () => void
    goForward: () => void
    title: string
    data: InterfaceData[]
    dispatch: any

    wizardInterfaceName: string
    setWizardInterfaceName: (value: string) => void

    isInterfaceValid: boolean
    setIsInterfaceValid: (val: boolean) => void
}

const Interface = (props: InterfaceProps) => {
    const {
        closeModal,
        isInterfaceValid,
        setIsInterfaceValid,
        data,
        goBack,
        goForward,
        title,
        dispatch,
        wizardInterfaceName,
        setWizardInterfaceName,
    } = props
    const classes = useStyles()
    const { t } = useTranslation()
    const description1: string = t('dse.wizard.interface.description1')
    const description2: string = t('dse.wizard.interface.description2')
    const description3: string = t('dse.wizard.interface.description3')
    // const [isStepValuesValid, setIsStepValuesValid] = useState(false)

    const validatePageValues = () => {
        const isValid = !(wizardInterfaceName === '')
        setIsInterfaceValid(isValid)
    }

    useEffect(() => {
        validatePageValues()
    })

    const handleWizardInterfaceNameChange = (value) => {
        setWizardInterfaceName(value)
        validatePageValues()
    }

    return (
        <div className={classes.container}>
            <Box component="div">
                <TextBox variant="h3" display="inline">
                    {t('dse.wizard.interface.title')}
                </TextBox>
                <TextBox variant="h4" display="inline">
                    {t('dse.wizard.interface.subtitle')}
                </TextBox>
            </Box>

            <Box component="div" className={classes.descContainer}>
                {[description1, description2, description3].map(
                    (desc, dIndex) => {
                        return (
                            <TextBox
                                key={dIndex}
                                variant="body2"
                                className={classes.titleText}
                            >
                                {desc}
                            </TextBox>
                        )
                    }
                )}
            </Box>

            <div>
                {data && (
                    <InterfaceTable
                        dispatch={dispatch}
                        data={data}
                        title={title}
                        wizardInterfaceName={wizardInterfaceName}
                        setWizardInterfaceName={(value) =>
                            handleWizardInterfaceNameChange(value)
                        }
                    />
                )}
            </div>

            <div className={classes.buttonsRow}>
                <AppButton data-outline onClick={closeModal}>
                    {t('dse.wizard.interface.buttons.close')}
                </AppButton>
                <div>
                    <AppButton
                        data-outline
                        onClick={goBack}
                        style={{ marginRight: '10px' }}
                    >
                        <NavigateBeforeIcon />
                        {t('dse.wizard.interface.buttons.back')}
                    </AppButton>
                    <AppButton disabled={!isInterfaceValid} onClick={goForward}>
                        {t('dse.wizard.interface.buttons.next')}
                        <NavigateNextIcon />
                    </AppButton>
                </div>
            </div>
        </div>
    )
}

export default Interface
