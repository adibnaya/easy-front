import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
        },
        container: {
            width: '100%',
            fontFamily: theme.fonts.primaryText,
            maxHeight: 440,
            border: 0,
        },
        headerRowText: {
            fontSize: theme.sizes.smallText,
            fontWeight: 'bold',
            // padding: '20px 30px',
            backgroundColor: theme.colors.white,
            color: '#A3A5BD',
        },
        bodyRowText: {
            fontSize: theme.sizes.secondaryText,
            color: '#40414E',
        },
        rootBodyRow: {
            height: 55,
            '&:hover': {
                backgroundColor: '#67B1FA0F',
            },
        },
        selectedBodyRow: {
            backgroundColor: '#67B1FA24 !important',
        },
    })
)

export default useStyles
