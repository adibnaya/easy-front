import React, { useState } from 'react'
import CustomTable, { Column } from '../../../Table/Table'
import { InterfaceData } from '../../../../Containers/Aci/Models/AciModels'
import ActualStatus from '../ActualStatus/ActualStatus'
import AdminStatus from '../AdminStatus/AdminStatus'
import { disableLeafInterface } from '../../../../Containers/Aci/DeployStandardEndpoint/DeployStandardEndpointReducer'

const columns: Column[] = [
    { id: 'interface', label: 'INTERFACE' },
    { id: 'description', label: 'DESCRIPTION' },
    {
        id: 'actual_status',
        label: 'ACTUAL STATUS',
        noPadding: true,
        renderCell: (value, index, row) => {
            return <ActualStatus value={value} />
        },
    },
    {
        id: 'admin_status',
        label: 'ADMIN STATUS',
        renderCell: (value, index, row, dispatch) => {
            return (
                <AdminStatus
                    value={value}
                    disableFunction={() =>
                        dispatch(disableLeafInterface(row.interface))
                    }
                />
            )
        },
    },
    { id: 'duplex', label: 'DUPLEX' },
    { id: 'speed', label: 'SPEED' },
    { id: 'status_reason', label: 'STATUS REASON' },
    {
        id: 'last_change',
        label: 'LINK STATUS LAST CHANGED',
    },
]

interface Props {
    data: InterfaceData[]
    title: string
    dispatch: () => void
    wizardInterfaceName: string
    setWizardInterfaceName: (value: string) => void
}

function filterByValue(array, string) {
    return array.filter((o) =>
        Object.keys(o).some((k) => {
            if (typeof o[k] === 'string') {
                return o[k].toLowerCase().includes(string.toLowerCase())
            }

            return false
        })
    )
}

const InterfaceTable = ({
    data,
    title,
    dispatch,
    setWizardInterfaceName,
    wizardInterfaceName,
}: Props) => {
    const [stringFilter, setStringFilter] = useState('')
    const [selectedRowIndex, setSelectedRowIndex] = useState(
        data.findIndex((r) =>
            r.interface.includes('/')
                ? r.interface.split('/')[1] === wizardInterfaceName
                : -1
        )
    )

    const onSearchChanged = (val: string) => {
        setStringFilter(val)

        if (val.length > 2) {
            const filteredData = filterByValue(data, val)
            const selectedFilterRowIndex = filteredData.findIndex((r) =>
                r.interface.includes('/')
                    ? r.interface.split('/')[1] === wizardInterfaceName
                    : -1
            )
            setSelectedRowIndex(selectedFilterRowIndex)
        }
    }

    const onRowClick = (row: InterfaceData, rowIndex: number) => {
        if (row && row.interface.includes('/')) {
            const interfaceNameForDeploy = row.interface.split('/')[1]
            setWizardInterfaceName(interfaceNameForDeploy)
        }
        setSelectedRowIndex(rowIndex)
    }

    return (
        <div>
            <CustomTable
                dispatch={dispatch}
                title={title}
                columns={columns}
                rows={
                    stringFilter.length > 2
                        ? filterByValue(data, stringFilter).map((d) => ({
                              disable: !(
                                  d.admin_status === 'up' &&
                                  d.actual_status === 'down' &&
                                  d.description === ''
                              ),
                              ...d,
                          }))
                        : data.map((d) => ({
                              disable: !(
                                  d.admin_status === 'up' &&
                                  d.actual_status === 'down' &&
                                  d.description === ''
                              ),
                              ...d,
                          }))
                }
                isLoading={data && data.length < 1 ? true : false}
                maxHeight="430px"
                totalRows={data.length}
                selectedRow={selectedRowIndex}
                onRowClick={(row, rowIndex) => onRowClick(row, rowIndex)}
                search
                onSearchChanged={onSearchChanged}
            />
        </div>
    )
}

export default InterfaceTable
