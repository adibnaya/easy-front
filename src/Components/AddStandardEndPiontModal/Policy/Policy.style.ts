import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: theme.palette.background.paper,
        },
        imageOverlayContainer: {
            position: 'fixed',
            left: 0,
            top: 0,
            width: '100%',
            height: '100vh',
            backgroundColor: 'rgba(0,0,0,0.6)',
            display: 'flex',
            alignItems: 'center',
            zIndex: 5,
        },
        imageOverlay: {
            width: '100%',
            padding: '5%',
            boxSizing: 'border-box',
        },
        container: {
            width: 'calc(100% - 100px)',
            height: 713,
            padding: '30px 50px',
            display: 'flex',
            flexDirection: 'column',
            backgroundColor: theme.colors.white,
            borderBottomRightRadius: 3,
            borderBottomLeftRadius: 3,
            boxShadow: '4px 7px 29px #29292E4A',
        },
        titleText: {
            margin: '10px 0 70px 0',
            color: '#8C8C97',
        },
        label: {
            color: '#A3A5BD',
        },
        link: {
            lineHeight: '29px',
            color: '#67B1FA',
            cursor: 'pointer',
        },
        tabTitle: {
            textTransform: 'none',
        },
        policyTabsContainer: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            borderBottom: '1px solid #EBEBF0',
        },
        tabsHeaderRightSide: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: '140px',
            cursor: 'pointer',
        },
        searchContainer: {
            width: 283,
            marginTop: 10,
        },
        switchContainer: {
            marginTop: 10,
            display: 'flex',
            alignItems: 'center',
        },
        switchText: {
            marginLeft: 10,
            fontFamily: theme.fonts.primaryText,
        },
        buttonsRow: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            position: 'absolute',
            bottom: 30,
            width: 'calc(100% - 100px)',
        },
        bodyContainer: {
            display: 'flex',
            justifyContent: 'flex-start',
            flexDirection: 'column',
            flex: 1,
        },
        panelContainer: {
            // backgroundColor: 'pink',
        },
        formField: {
            marginTop: 50,
        },
        input: {
            marginTop: 10,
            height: 29,
            backgroundColor: '#F4F5FC',
            border: 0,
            width: '283px',
            fontFamily: theme.fonts.primaryText,
            paddingLeft: '14px',
            borderRadius: '300px',
            color: 'rgba(64, 65, 78, 1)',
            boxSizing: 'border-box',
            margin: 2,
            '&:hover': {
                marginTop: 10,
                border: '1px solid #D9DAE4',
                // margin: 1,
            },
            '&:focus': {
                outline: 'none',
            },
        },
    })
)
export default useStyles
