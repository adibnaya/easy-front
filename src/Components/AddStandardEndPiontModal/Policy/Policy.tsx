import React, { useEffect, useState } from 'react'
import { Box, Tab, Tabs } from '@material-ui/core'
import { useTranslation } from 'react-i18next'
import useStyles from './Policy.style'
import AppButton from '../../Button/Button'
import SelectAutocomplete from '../../SelectAutocomplete/SelectAutocomplete'
import TextBox from '../../TextBox/TextBox'
import TabPanel from '../TabPanel/TabPanel'
import { Option } from '../../SearchAutocomplete/SearchAutocomplete'
import TooltipInput from '../../TooltipInput/TooltipInput'
import { InputPopper } from '../EndPointAndLeaf/EndPointAndLeaf'
import TooltipText from '../../TooltipText/TooltipText'
import AppSwitch from '../../Switch/Switch'
import {
    CreateAaepPostReq,
    CreatePolicyPostReq,
} from '../../../Containers/Aci/Models/AciModels'
import { ReactComponent as PoliciesHierarchIcon } from '../../../Assets/policies_hierarchy.svg'
import HierarchyExample from '../../../Assets/hierarchyExample.png'
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore'

interface PolicyProps {
    solutionPolicies: { name: string }[]
    solutionAaeps: { name: string }[]
    closeModal: () => void
    goBack: () => void
    finish: () => void
    policySelectedTab: number
    setPolicySelectedTab: (val: number) => void
    wizardAaep: string
    setAttachableAccessEntityProfile: (val: string) => void
    interfacePolicyGroupName: string
    setInterfacePolicyGroupName: (val: string) => void
    wizardPolicyName: string
    setWizardPolicyName: (val: string) => void
    connectedToSwitch: boolean
    setConnectedToSwitch: (val: boolean) => void
    createAaep: (aaep: CreateAaepPostReq) => void
    createAaepResponse: string
    createPolicy: (policy: CreatePolicyPostReq) => void

    isPolicyValid: boolean
    setIsPolicyValid: (val: boolean) => void
}

const Policy = (props: PolicyProps) => {
    const {
        solutionPolicies,
        closeModal,
        isPolicyValid,
        setIsPolicyValid,
        goBack,
        finish,
        interfacePolicyGroupName,
        setInterfacePolicyGroupName,
        policySelectedTab,
        setPolicySelectedTab,
        wizardAaep,
        setAttachableAccessEntityProfile,
        solutionAaeps,
        connectedToSwitch,
        setConnectedToSwitch,
        wizardPolicyName,
        setWizardPolicyName,
        createAaep,
        createPolicy,
    } = props
    const classes = useStyles()
    const { t } = useTranslation()

    const [showImage, toggleShowImage] = useState(false)

    const [
        showEndpointNameInputPopper,
        setshowEndpointNameInputPopper,
    ] = useState(false)

    const validatePageValues = (policySelectedTabIndex: number) => {
        if (policySelectedTab === 0) {
            if (wizardPolicyName === '') {
                setIsPolicyValid(false)
            } else {
                setIsPolicyValid(true)
            }
        } else {
            // tabIndex = 1
            if (
                wizardAaep === '' ||
                !validateEndpointNameValue(interfacePolicyGroupName)
            ) {
                setIsPolicyValid(false)
            } else {
                setIsPolicyValid(true)
            }
        }
    }

    const handleSelectPolicyName = (selectedOption: Option) => {
        let policy
        if (selectedOption) {
            policy = solutionPolicies && solutionPolicies[selectedOption.key]
            if (policy) {
                setWizardPolicyName(policy.name)
            }
        } else {
            setWizardPolicyName('')
        }
        validatePageValues(policySelectedTab)
    }

    const handleSelectAAEP = (selectedOption: Option) => {
        let aaep
        if (selectedOption) {
            aaep = solutionAaeps && solutionAaeps[selectedOption.key]
            if (aaep) {
                setAttachableAccessEntityProfile(aaep.name)
            }
        } else {
            setAttachableAccessEntityProfile('')
        }
        validatePageValues(policySelectedTab)
    }

    const handleInterfacePolicyGroupName = (value) => {
        setshowEndpointNameInputPopper(
            !validateEndpointNameValue(value) && value.length > 0
        )
        setInterfacePolicyGroupName(value)
        validatePageValues(policySelectedTab)
    }

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setPolicySelectedTab(newValue)
        validatePageValues(policySelectedTab)
    }

    const validateEndpointNameValue = (value: string): boolean => {
        if (value) return value.match(/^[a-zA-Z0-9]{5,30}$/i) !== null
        return false
    }

    useEffect(() => {
        validatePageValues(policySelectedTab)
    })

    const a11yProps = (index: any) => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        }
    }

    const handleOnDone = () => {
        if (policySelectedTab === 0) {
            finish()
        } else {
            let policy: CreatePolicyPostReq = {
                name: interfacePolicyGroupName,
                aaep_name: wizardAaep,
                connected_to_switch: connectedToSwitch,
            }
            createPolicy(policy)
        }
    }

    const handleSwitchToggle = (newToggleState: boolean) => {
        setConnectedToSwitch(newToggleState)
    }

    const selectedAEEPindex = solutionAaeps.findIndex(
        (a) => a && a.name === wizardAaep
    )
    const selectedPolicyIndex = solutionPolicies.findIndex(
        (a) => a && a.name === wizardPolicyName
    )

    const handleHierarchyImageToggle = () => {
        toggleShowImage(!showImage)
    }

    return (
        <div className={classes.container}>
            {showImage && (
                <Box
                    className={classes.imageOverlayContainer}
                    onClick={() => handleHierarchyImageToggle()}
                >
                    <img
                        className={classes.imageOverlay}
                        src={HierarchyExample}
                        alt="hierarchy"
                    />
                </Box>
            )}
            <Box component="div">
                <TextBox variant="h3" display="inline">
                    {`${t('dse.wizard.policy.title')} `}
                </TextBox>
                <TextBox variant="h4" display="inline">
                    {t('dse.wizard.policy.subtitle')}
                </TextBox>
            </Box>

            <TextBox variant="body2" className={classes.titleText}>
                {t('dse.wizard.policy.description')}
            </TextBox>

            <Box component="div" className={classes.bodyContainer}>
                <Box component="div" className={classes.policyTabsContainer}>
                    <Tabs
                        value={policySelectedTab || 0}
                        indicatorColor="primary"
                        textColor="primary"
                        onChange={handleChange}
                    >
                        <Tab
                            disableRipple
                            label={
                                <TextBox
                                    variant="body2"
                                    className={classes.tabTitle}
                                >
                                    {t('dse.wizard.policy.tab1Name')}
                                </TextBox>
                            }
                            {...a11yProps(0)}
                        />
                        <Tab
                            disableRipple
                            label={
                                <TextBox
                                    variant="body2"
                                    className={classes.tabTitle}
                                >
                                    {t('dse.wizard.policy.tab2Name')}
                                </TextBox>
                            }
                            {...a11yProps(1)}
                        />
                    </Tabs>

                    <Box
                        className={classes.tabsHeaderRightSide}
                        onClick={() => handleHierarchyImageToggle()}
                    >
                        <PoliciesHierarchIcon />
                        <TextBox variant="body2" className={classes.link}>
                            {t('dse.wizard.policy.hierarchy')}
                        </TextBox>
                    </Box>
                </Box>

                <TabPanel value={policySelectedTab || 0} index={0}>
                    <Box component="div" className={classes.panelContainer}>
                        <Box component="div" className={classes.formField}>
                            <TextBox variant="body2" className={classes.label}>
                                Policy
                            </TextBox>
                            <Box
                                component="div"
                                className={classes.searchContainer}
                            >
                                <SelectAutocomplete
                                    onOptionClicked={handleSelectPolicyName}
                                    selectedOptionIndex={
                                        selectedPolicyIndex >= 0
                                            ? selectedPolicyIndex
                                            : undefined
                                    }
                                    data={
                                        solutionPolicies &&
                                        solutionPolicies.map(
                                            (policy, policyIndex): Option => {
                                                return {
                                                    key: policyIndex,
                                                    title: policy.name,
                                                }
                                            }
                                        )
                                    }
                                    placeholder="Start typing.."
                                />
                            </Box>
                        </Box>
                    </Box>
                </TabPanel>
                <TabPanel value={policySelectedTab || 0} index={1}>
                    <Box component="div" className={classes.panelContainer}>
                        <Box component="div" className={classes.formField}>
                            <TextBox variant="body2" className={classes.label}>
                                {t('dse.wizard.policy.field1.title')}
                            </TextBox>
                            <Box
                                component="div"
                                display="flex"
                                marginTop="15px"
                            >
                                <Box component="div" style={{ width: 283 }}>
                                    <SelectAutocomplete
                                        onOptionClicked={handleSelectAAEP}
                                        selectedOptionIndex={
                                            selectedAEEPindex >= 0
                                                ? selectedAEEPindex
                                                : undefined
                                        }
                                        data={
                                            solutionAaeps &&
                                            solutionAaeps.map(
                                                (
                                                    policy,
                                                    policyIndex
                                                ): Option => {
                                                    return {
                                                        key: policyIndex,
                                                        title: policy.name,
                                                    }
                                                }
                                            )
                                        }
                                        placeholder={t(
                                            'dse.wizard.policy.field2.placeholder'
                                        )}
                                    />
                                </Box>
                                <Box component="div" marginLeft="20px">
                                    <Box component="div">
                                        <TooltipText
                                            renderPopper={(closeFunc) => (
                                                <CreateNewPopperContent
                                                    onSave={(
                                                        aaep: CreateAaepPostReq
                                                    ) => {
                                                        createAaep(aaep)
                                                        closeFunc()
                                                    }}
                                                    closePopper={closeFunc}
                                                />
                                            )}
                                            popperPlacement="right"
                                            textValue="+ Create New"
                                            disablePortal
                                        />
                                    </Box>
                                </Box>
                            </Box>
                        </Box>

                        <Box component="div" className={classes.formField}>
                            <TextBox variant="body2" className={classes.label}>
                                {t('dse.wizard.policy.field2.title')}
                            </TextBox>
                            <Box
                                component="div"
                                className={classes.searchContainer}
                                marginTop="15px"
                            >
                                <TooltipInput
                                    renderPopper={() => <InputPopper />}
                                    inputMaxLength={30}
                                    open={showEndpointNameInputPopper}
                                    popperPlacement="right"
                                    inputPlaceHolder={t(
                                        'dse.wizard.policy.field1.placeholder'
                                    )}
                                    handleInputChange={(e) =>
                                        handleInterfacePolicyGroupName(
                                            e.target.value
                                        )
                                    }
                                    inputValue={interfacePolicyGroupName}
                                />
                            </Box>
                        </Box>

                        <Box component="div" className={classes.formField}>
                            <TextBox variant="body2" className={classes.label}>
                                {t('dse.wizard.policy.field3.title')}
                            </TextBox>
                            <Box
                                component="div"
                                className={classes.switchContainer}
                                marginTop="15px"
                            >
                                <AppSwitch
                                    checked={connectedToSwitch}
                                    onChange={(e) =>
                                        handleSwitchToggle(e.target.checked)
                                    }
                                />
                                {connectedToSwitch && (
                                    <TextBox
                                        variant="body2"
                                        className={classes.switchText}
                                    >
                                        On
                                    </TextBox>
                                )}
                            </Box>
                        </Box>
                    </Box>
                </TabPanel>
            </Box>

            <div className={classes.buttonsRow}>
                <AppButton data-outline onClick={closeModal}>
                    Close
                </AppButton>
                <div>
                    <AppButton
                        data-outline
                        onClick={goBack}
                        style={{ marginRight: '10px' }}
                    >
                        <NavigateBeforeIcon />
                        {t('dse.wizard.policy.bottomContainerButtons.back')}
                    </AppButton>
                    <AppButton disabled={!isPolicyValid} onClick={handleOnDone}>
                        {t('dse.wizard.policy.bottomContainerButtons.done')}
                    </AppButton>
                </div>
            </div>
        </div>
    )
}

interface Props {
    closePopper: () => void
    onSave: (aaep: CreateAaepPostReq) => void
}

const CreateNewPopperContent = ({ closePopper, onSave }: Props) => {
    const classes = useStyles()
    const [value, setValue] = useState('')

    const handleInputChange = (val: any) => {
        setValue(val)
    }

    const handleOnSaveAaepName = () => {
        onSave({ name: value })
        closePopper()
    }

    return (
        <Box component="div" padding="20px">
            <TextBox>Create New</TextBox>
            <Box component="div" marginTop="20px">
                <TextBox variant="body2" className={classes.label}>
                    Name
                </TextBox>
                <input
                    className={classes.input}
                    placeholder="Start typing"
                    value={value}
                    onChange={(e) => handleInputChange(e.target.value)}
                />
            </Box>
            <Box
                component="div"
                display="flex"
                justifyContent="flex-end"
                marginTop="30px"
            >
                <AppButton
                    style={{ marginRight: '10px' }}
                    onClick={closePopper}
                >
                    Close
                </AppButton>
                <AppButton
                    disabled={value === ''}
                    onClick={() => handleOnSaveAaepName()}
                >
                    Save
                </AppButton>
            </Box>
        </Box>
    )
}

export default Policy
