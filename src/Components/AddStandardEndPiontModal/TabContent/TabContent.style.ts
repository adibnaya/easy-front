import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        tabContentNumber: {
            color: '#F4F5FC',
            backgroundColor: '#CFCFCF',
            borderRadius: 20,
            width: 27,
            height: 27,
            fontSize: theme.sizes.primaryText,
            display: 'inline-block',
        },
        selectedTabContentNumber: {
            color: '#67B1FA',
            backgroundColor: theme.colors.white,
            borderRadius: 20,
            width: 27,
            height: 27,
            fontSize: theme.sizes.primaryText,
            display: 'inline-block',
        },
        finishTabContent: {
            backgroundColor: '#67B1FA',
            color: theme.colors.white,
        },
        title: {
            marginLeft: 10,
            textTransform: 'capitalize',
        },
    })
)

export default useStyles
