import React from 'react'
import useStyles from './TabContent.style'

interface TabContentProps {
    index: number
    title: string
    tabsValue?: number
    isDone: boolean
}
const TabContent = ({ index, title, tabsValue, isDone }: TabContentProps) => {
    const classes = useStyles()
    const isSelected = tabsValue === index
    const isFinish = tabsValue && tabsValue > index
    return (
        <div>
            <span
                className={`${
                    isSelected
                        ? classes.selectedTabContentNumber
                        : classes.tabContentNumber
                }
                 ${isFinish ? classes.finishTabContent : ''}`}
            >
                {isDone ? <span>&#10003;</span> : index + 1}
            </span>
            <span className={classes.title}>{title}</span>
        </div>
    )
}

export default TabContent
