import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        indicator: {
            display: 'none',
        },
        tabsContainer: {
            backgroundColor: '#F4F5FC',
            opacity: 1,
            borderTopRightRadius: 3,
            borderTopLeftRadius: 3,
        },
        secondTabSelected: {
            background: 'linear-gradient(90deg, #67B1FA 50%, #F4F5FC 50%);',
            opacity: 1,
        },
        lastTabSelected: {
            background: 'linear-gradient(90deg, #C0DCFA 50%, #67B1FA 50%);',
            opacity: 1,
        },
    })
)

export default useStyles
