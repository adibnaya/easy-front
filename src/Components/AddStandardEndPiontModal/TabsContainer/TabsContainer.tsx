import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import useStyles from './TabsContainer.style'

interface Props {
    handleTabsChange: any
    value: number
    children?: React.ReactNode
}
const TabsContainer = ({ handleTabsChange, value, children }: Props) => {
    const classes = useStyles()
    return (
        <AppBar position="static">
            <Tabs
                TabIndicatorProps={{ className: classes.indicator }}
                classes={{
                    root: `${classes.tabsContainer} ${
                        value === 1 ? classes.secondTabSelected : ''
                    } ${value === 2 ? classes.lastTabSelected : ''}`,
                }}
                variant="fullWidth"
                value={value}
                onChange={handleTabsChange}
                aria-label="wizard tabs"
            >
                {children}
            </Tabs>
        </AppBar>
    )
}

export default TabsContainer
