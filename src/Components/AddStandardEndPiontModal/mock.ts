import { StandardEndpoint } from '../../Containers/Aci/Models/AciModels'

export const TempEndpoint: StandardEndpoint = {
    endpoint: {
        name: 'test1',
    },
    leaf: {
        id: 'test2',
        interfaces: [
            {
                actual_status: 'a',
                admin_status: 'b',
                broadcast_packets: 'c',
                byte_rx: 'd',
                crc_errors: 'e',
                collisions: 'f',
                duplex: 'g',
                jabbers: 'h',
                speed: 'i',
                status_reason: 'j',
                byte_tx: 'k',
                description: 'l',
                drop_events: 'm',
                fragments: 'n',
                interface: 'o',
                last_change: 'p',
                last_cleared: 'q',
                multicast_packets: 'r',
            },
        ],
    },
    policy: {
        aaep: 'test3',
        interfacePolicyGroupName: 'test4',
        isConnectedToSwitch: false,
    },
}
