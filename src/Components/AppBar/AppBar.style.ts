import { createStyles, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) =>
    createStyles({
        container: {
            backgroundColor: theme.colors.white,
            display: 'flex',
            height: 68,
            width: 'calc(100% - 80px)',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: '0 40px',
            boxShadow: '4px 7px 9px #EFEFF5',
        },
    })
)

export default useStyles
