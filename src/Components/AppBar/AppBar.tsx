import React from 'react'
import useStyles from './AppBar.style'

interface Props {
    children: React.ReactNode
}

const AppBar = ({ children }: Props) => {
    const classes = useStyles()
    return <div className={classes.container}>{children}</div>
}
export default AppBar
