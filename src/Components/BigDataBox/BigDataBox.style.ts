import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            cursor: 'pointer',
            width: 523,
            height: 214,
            flex: '0 1 auto',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            boxShadow: '0px 3px 6px #D3D3E9CF',
            borderRadius: '3px',
            '&:hover': {
                boxShadow: '0px 6px 16px #D3D3E9',
            },
        },
        pieChart: { width: 150, height: 150, margin: '32px 47px 0 0' },
    })
)

export default useStyles
