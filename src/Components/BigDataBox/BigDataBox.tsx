import React from 'react'
import MiniTable from '../MiniTable/MiniTable'
import HomeTableRow, {
    TableRowData,
} from '../MiniTable/HomeTableRow/HomeTableRow'
import CustomPieChart from '../PieChart/PieChart'
import useStyles from './BigDataBox.style'

const getPieChartColor = (type: string, index) => {
    if (type === 'general_pie_chart') {
        if (index === 0) {
            return '#3787D6'
        }
        if (index === 1) {
            return '#67B1FA'
        }
    } else if (type === 'faults_pie_chart') {
        if (index === 0) {
            return '#FD5C63'
        }
        if (index === 1) {
            return '#F8911C'
        }
        if (index === 2) {
            return '#F8C51C'
        }
    }
    return '#F8C51C'
}

interface Props {
    data: TableRowData[] | any
    title: string
    className?: string
    type: string
    onClick: () => void
}

const BigDataBox = ({ data, title, className, type, onClick }: Props) => {
    const classes = useStyles()
    return (
        <div
            className={`${classes.container} ${className || ''}`}
            onClick={onClick}
        >
            {data.length > 0 && (
                <MiniTable
                    title={title}
                    data={data}
                    renderRow={(datum, i) => (
                        <HomeTableRow
                            type={type}
                            index={i}
                            key={datum.title + i}
                            datum={datum}
                        />
                    )}
                />
            )}
            {data.length > 0 && (
                <CustomPieChart
                    className={classes.pieChart}
                    data={data.map((datum, index) => ({
                        name: datum.name,
                        value: datum.value,
                        color: getPieChartColor(type, index),
                    }))}
                    type={type}
                />
            )}
        </div>
    )
}

export default BigDataBox
