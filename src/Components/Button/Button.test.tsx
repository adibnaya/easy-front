import React from 'react'
// @ts-ignore
import { screen } from '@testing-library/react'
import { render } from '../../Utils/TestUtils'
import Button from './Button'

describe('Button', () => {
    test('renders Button component', () => {
        render(<Button>Hello</Button>)
        expect(screen.getByText('Hello')).toBeInTheDocument()
    })
})
