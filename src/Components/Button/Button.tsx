import { withStyles, Theme as AugmentedTheme } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

export interface IAppButtonProps {
    disabled: boolean
}
const AppButton = withStyles((theme: AugmentedTheme) => ({
    root: {
        color: '#FFFFFF',
        background: `${theme.colors.primary} 0% 0% no-repeat padding-box`,
        textTransform: 'none',
        padding: '4px 12px',
        minWidth: 80,
        width: 'fit-content',
        borderRadius: 300,
        '&:hover': {
            background: '#3E7BB7 0% 0% no-repeat padding-box',
        },
        '&:focus': {
            background: '#3E7BB7 0% 0% no-repeat padding-box',
        },
        '&:disabled': {
            background: '#ABABAB 0% 0% no-repeat padding-box',
            color: '#FFFFFF',
        },
        '&[data-outline]': {
            background: 'transparent',
            border: '1px solid #67B1FA',
            color: '#67B1FA',
            '&:hover': {
                border: '1px solid #3E7BB7',
                color: '#3E7BB7',
            },
            '&:focus': {
                border: '1px solid #3E7BB7',
                color: '#3E7BB7',
            },
            '&:disabled': {
                border: '1px solid #ABABAB',
                color: '#ABABAB',
            },
        },
    },
}))(Button)

export default AppButton
