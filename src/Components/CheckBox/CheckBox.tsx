import { withStyles, Theme as AugmentedTheme } from '@material-ui/core/styles'
import Checkbox from '@material-ui/core/Checkbox'

const CustomCheckBox = withStyles((theme: AugmentedTheme) => ({
    root: {},
    checked: {},
    disabled: {},
    indeterminate: {},
    colorPrimary: {},
    colorSecondary: {},
}))(Checkbox)

export default CustomCheckBox
