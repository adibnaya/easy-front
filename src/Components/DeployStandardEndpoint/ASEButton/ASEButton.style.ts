import { createStyles, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) =>
    createStyles({
        buttonContainer: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            cursor: 'pointer',
        },
        ASEText: {
            color: '#67B1FA',
        },
        addIcon: {
            backgroundColor: '#67B1FA',
            color: 'white',
            lineHeight: '22px',
            height: 22,
            width: 22,
            borderRadius: 15,
            fontSize: 16,
            marginRight: 8,
            textAlign: 'center',
        },
    })
)

export default useStyles
