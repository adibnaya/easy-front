import React from 'react'
import Icon from '@material-ui/core/Icon'
import { useTranslation } from 'react-i18next'
import useStyles from './ASEButton.style'
import TextBox from '../../TextBox/TextBox'

interface Props {
    onClick: () => void
}

const ASEButton = ({ onClick }: Props) => {
    const classes = useStyles()
    const { t } = useTranslation()
    return (
        <div className={classes.buttonContainer} onClick={() => onClick()}>
            <Icon className={classes.addIcon}>add</Icon>
            <TextBox variant="body2" className={classes.ASEText}>
                {t('dse.ASEButton')}
            </TextBox>
        </div>
    )
}

export default ASEButton
