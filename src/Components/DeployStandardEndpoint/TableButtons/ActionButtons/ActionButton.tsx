import React from 'react'
import useStyles from './ActionButton.style'
import { ReactComponent as DeleteIcon } from '../../../../Assets/delete.svg'
import { ReactComponent as EditIcon } from '../../../../Assets/edit.svg'

interface Props {
    onDelete: () => void
    onEdit: () => void
}

const ActionButtons = ({ onDelete, onEdit }: Props) => {
    const classes = useStyles()
    return (
        <div className={classes.actionButtonsContainer}>
            <EditIcon className={classes.button} onClick={() => onEdit()} />
            <DeleteIcon className={classes.button} onClick={() => onDelete()} />
        </div>
    )
}

export default ActionButtons
