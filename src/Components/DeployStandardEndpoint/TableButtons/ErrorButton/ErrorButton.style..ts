import { createStyles, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) =>
    createStyles({
        errorButtonContainer: {
            marginRight: 15,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
        },
        errorButton: {
            cursor: 'pointer',
            color: 'red',
        },
        divider: {
            height: 24,
            marginRight: 15,
            width: 1,
            backgroundColor: '#7A96CE4F',
        },
        tooltip: {
            fontSize: 16,
        },
    })
)

export default useStyles
