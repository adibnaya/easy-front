import React from 'react'
import Icon from '@material-ui/core/Icon'
import Tooltip from '@material-ui/core/Tooltip'
import useStyles from './ErrorButton.style.'

interface Props {
    errorText: string
}

const ErrorButton = ({ errorText }: Props) => {
    const classes = useStyles()
    return (
        <div className={classes.errorButtonContainer}>
            <div className={classes.divider} />
            <Tooltip
                classes={{
                    tooltip: classes.tooltip,
                }}
                title={errorText}
            >
                <Icon className={classes.errorButton}>block</Icon>
            </Tooltip>
        </div>
    )
}

export default ErrorButton
