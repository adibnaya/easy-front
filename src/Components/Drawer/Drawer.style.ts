import { createStyles, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) =>
    createStyles({
        paper: {
            width: 260,
            overflow: 'hidden',
            backgroundColor: '#262C31',
        },
        toggleButton: {
            position: 'fixed',
            cursor: 'pointer',
            width: 23,
            height: 73,
            top: 0,
            left: 0,
        },
        drawerModal: {
            backgroundColor: 'transparent',
            opacity: 0,
        },
        toggleIcon: {
            position: 'relative',
            left: '-20px',
            top: '-18px',
        },
        drawerIcon: {
            position: 'absolute',
            top: '28px',
            left: '7px',
        },
    })
)

export default useStyles
