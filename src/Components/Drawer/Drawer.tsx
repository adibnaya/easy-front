import React from 'react'
import Drawer, { DrawerProps } from '@material-ui/core/Drawer'
import useStyles from './Drawer.style'
import { ReactComponent as ToggleDrawerIcon } from '../../Assets/toggle_drawer.svg'
import { ReactComponent as OpenDrawerIcon } from '../../Assets/open_drawer.svg'

interface Props extends DrawerProps {
    openDrawer: () => void
    closeDrawer: () => void
    switchDrawerVariant: (variant: Props['variant']) => void
}

const CustomDrawer = ({
    open,
    openDrawer,
    closeDrawer,
    variant,
    children,
}: Props) => {
    const classes = useStyles()

    return (
        <div>
            <div className={classes.toggleButton} onClick={() => openDrawer()}>
                <ToggleDrawerIcon className={classes.toggleIcon} />
                <OpenDrawerIcon className={classes.drawerIcon} />
            </div>
            <Drawer
                ModalProps={{
                    BackdropProps: { classes: { root: classes.drawerModal } },
                }}
                anchor="left"
                variant={variant}
                open={open}
                onClose={() => closeDrawer()}
                classes={{
                    paper: classes.paper,
                }}
            >
                {children}
            </Drawer>
        </div>
    )
}

export default CustomDrawer
