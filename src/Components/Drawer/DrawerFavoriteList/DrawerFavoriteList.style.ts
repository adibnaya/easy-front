import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        listContainer: {
            backgroundColor: '#262C31',
        },
        url: {
            display: 'flex',
            width: 'calc(100% - 21px)',
            height: '100%',
            '&:hover': {
                fontWeight: 'bolder',
                cursor: 'pointer',
            },
        },
        circleSkeleton: {
            margin: '0 15px 15px 0',
        },
    })
)

export default useStyles
