import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import Skeleton from 'react-loading-skeleton'
import useStyles from './DrawerFavoriteList.style'
import SecondaryListItem from '../DrawerList/SecondaryListItem/SecondaryListItem'

// interface FavoritePage {
//     id: number
//     title: string
//     url: string
// }

interface Props {
    pages: any
    onClickPage: (url) => void
    fetchMoreData: () => void
    hasMore: boolean
}

const DrawerFavoriteList = ({
    pages,
    onClickPage,
    fetchMoreData,
    hasMore,
}: Props) => {
    const classes = useStyles()

    return (
        <div className={classes.listContainer}>
            <InfiniteScroll
                dataLength={pages.length}
                next={fetchMoreData}
                hasMore={hasMore}
                loader={
                    <div>
                        <Skeleton height={39} width="80%" />
                        <Skeleton height={39} width="80%" />
                    </div>
                }
                height={800}
            >
                {pages.map((p, index) => (
                    <SecondaryListItem
                        key={p.url}
                        title={p.url}
                        handleClick={() => onClickPage(p.url)}
                        isSelected={false}
                    />
                ))}
            </InfiniteScroll>
        </div>
    )
}

export default DrawerFavoriteList
