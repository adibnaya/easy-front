import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            height: 73,
            padding: '0 0 0 20px',
            backgroundColor: theme.colors.drawerHeader,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            display: 'flex',
        },
        logo: {
            width: 119,
            height: 25,
            marginLeft: 8,
        },
        toggleContainer: {
            flexGrow: 1,
            height: 73,
            backgroundColor: theme.colors.drawerHeader,
            display: 'flex',
            justifyContent: 'flex-end',
        },
        toggleParabola: {
            cursor: 'pointer',
            width: 23,
            height: 73,
            // background: '#304961 0% 0% no-repeat padding-box',
            position: 'relative',
            boxShadow: '-3px 3px 11px #00000014',
        },
        appsIconContainer: {
            width: 13,
            height: 13,
        },
        appsIcon: {
            position: 'relative',
            top: -4,
            fill: 'white !important',
        },
        toggleIcon: {
            transform: 'rotate(180deg)',
            position: 'relative',
            left: '-14px',
            top: '-22px',
        },
        drawerIcon: {
            position: 'absolute',
            top: '30px',
            left: '7px',
        },
    })
)

export default useStyles
