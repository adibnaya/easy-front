import React from 'react'
import { DrawerProps } from '@material-ui/core/Drawer'
import bynetLogo from '../../../Assets/bynet_logo.png'
import { ReactComponent as AppsIcon } from '../../../Assets/apps.svg'
import { ReactComponent as ToggleDrawerIcon } from '../../../Assets/toggle_drawer.svg'
import { ReactComponent as CloseDrawerIcon } from '../../../Assets/close_drawer.svg'
import { ReactComponent as PinDrawerIcon } from '../../../Assets/pin_drawer.svg'
import useStyles from './DrawerHeader.style'

interface Props {
    variant: DrawerProps['variant']
    pinDrawer: () => void
    unPinDrawer: () => void
}

const DrawerHeader = ({ unPinDrawer, variant, pinDrawer }: Props) => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.appsIconContainer}>
                <AppsIcon className={classes.appsIcon} />
            </div>
            <img className={classes.logo} src={bynetLogo} alt="bynet logo" />
            <div className={classes.toggleContainer}>
                {variant === 'permanent' && (
                    <div
                        className={classes.toggleParabola}
                        onClick={() => unPinDrawer()}
                    >
                        <ToggleDrawerIcon className={classes.toggleIcon} />
                        <CloseDrawerIcon className={classes.drawerIcon} />
                    </div>
                )}
                {variant === 'temporary' && (
                    <div
                        className={classes.toggleParabola}
                        onClick={() => pinDrawer()}
                    >
                        <ToggleDrawerIcon className={classes.toggleIcon} />
                        <PinDrawerIcon className={classes.drawerIcon} />
                    </div>
                )}
            </div>
        </div>
    )
}

export default DrawerHeader
