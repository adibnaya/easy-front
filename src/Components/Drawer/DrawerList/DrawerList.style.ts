import { createStyles, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) =>
    createStyles({
        secondList: {
            backgroundColor: theme.colors.drawerHeader,
        },
        fullList: {
            width: 'auto',
            backgroundColor: theme.colors.drawerItemBackground,
        },
        pageItem: {
            height: 37,
            fontFamily: theme.fonts.primaryText,
            fontSize: theme.sizes.secondaryText,
            width: 'calc(100% - 10px)',
            borderBottomRightRadius: 20,
            borderTopRightRadius: 20,
            padding: '0 0 0 19px',
            margin: '8px 0 8px 0',

            '&:hover': {
                backgroundColor: theme.colors.secondary,
            },
        },
        selectedBackground: {
            backgroundColor: theme.colors.primary,
        },
        homeSelected: {
            backgroundColor: `${theme.colors.secondary}!important`,
        },
        selectedListItem: {
            backgroundColor: theme.colors.drawerHeader,
        },
        listText: {
            display: 'flex',
            alignItems: 'center',
            color: theme.colors.white,
            fontFamily: theme.fonts.primaryText,
            fontSize: theme.sizes.secondaryText,
        },
        textAlignRight: {
            justifyContent: 'flex-end',
        },
        leftIcon: {
            marginRight: 10.5,
        },

        listPageText: {
            paddingLeft: 31,
        },
        dividerContainer: {
            backgroundColor: theme.colors.drawerHeader,
        },
        divider: {
            backgroundColor: '#7A96CE4F',
            margin: '0 20px',
        },
    })
)

export default useStyles
