import List, { ListProps } from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import React from 'react'
import { ReactComponent as HomeIcon } from '../../../Assets/home.svg'
import useStyles from './DrawerList.style'
import RecursiveListItem from './RecursiveListItem/RecursiveListItem'
import { Page } from '../../../Containers/DrawerAndBar/DrawerReducer'
import { useTranslation } from 'react-i18next'

interface Props extends ListProps {
    pagesList: Page[]
    pathName: string
    solutionId: string
    handleChildClick: (slug) => void
}

const DrawerList = ({
    pagesList,
    pathName,
    solutionId,
    handleChildClick,
    ...props
}: Props) => {
    const classes = useStyles()
    const { t } = useTranslation()

    const isHomeSelected =
        pathName === `/solutions/${solutionId}` ||
        pathName === `/solutions/${solutionId}/`

    return (
        <div className={classes.fullList} role="presentation">
            <List disablePadding>
                <ListItem
                    className={`${classes.pageItem} ${
                        isHomeSelected ? classes.homeSelected : ''
                    }`}
                    button
                    key="home"
                    onClick={() =>
                        handleChildClick(`/solutions/${solutionId}/`)
                    }
                >
                    <ListItemText
                        disableTypography
                        className={classes.listText}
                    >
                        <HomeIcon className={classes.leftIcon} />
                        {t('drawerAndBar.drawerList.homeLinkName')}
                    </ListItemText>
                </ListItem>

                {pagesList.map((pa) => {
                    return (
                        <RecursiveListItem
                            parentSlug={`/solutions/${solutionId}`}
                            key={pa.id}
                            page={pa}
                            handleChildClick={(slug) => handleChildClick(slug)}
                        />
                    )
                })}
            </List>
        </div>
    )
}

export default DrawerList
