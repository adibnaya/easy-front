import { createStyles, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) =>
    createStyles({
        selectedBackground: {
            backgroundColor: theme.colors.primary,
        },
        listItem: {
            height: 37,
            fontFamily: theme.fonts.primaryText,
            fontSize: theme.sizes.secondaryText,
            padding: '0 20px 0 19px',
            margin: '8px 0 8px 0',
            '&:hover': {
                backgroundColor: theme.colors.secondary,
            },
        },
        selectedListItem: {
            backgroundColor: theme.colors.drawerHeader,
        },
        listText: {
            display: 'flex',
            alignItems: 'center',
            color: theme.colors.white,
            fontFamily: theme.fonts.primaryText,
            fontSize: theme.sizes.secondaryText,
        },
        textAlignRight: {
            justifyContent: 'flex-end',
        },
        leftIcon: {
            marginRight: 10.5,
        },
        rightIcon: {
            marginLeft: 6,
        },
        listPageText: {
            paddingLeft: 31,
        },
    })
)

export default useStyles
