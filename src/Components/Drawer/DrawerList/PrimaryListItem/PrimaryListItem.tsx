import ListItemText from '@material-ui/core/ListItemText'
import ListItem from '@material-ui/core/ListItem'
import React from 'react'
import { ReactComponent as OpenedFolderIcon } from '../../../../Assets/opened_folder.svg'
import { ReactComponent as ClosedFolderIcon } from '../../../../Assets/closed_folder.svg'
import { ReactComponent as UpArrowIcon } from '../../../../Assets/up_arrow.svg'
import { ReactComponent as DownArrowIcon } from '../../../../Assets/down_arrow.svg'
import useStyles from './PrimaryListItem.style'

interface Props {
    title: string
    handleClick: () => void
    isOpen: boolean
    numOfItems: number | undefined
}

const PrimaryListItem = ({ title, handleClick, isOpen, numOfItems }: Props) => {
    const classes = useStyles()
    return (
        <ListItem
            className={`${classes.listItem} ${
                isOpen ? classes.selectedListItem : ''
            }`}
            button
            onClick={() => handleClick()}
        >
            {isOpen ? (
                <OpenedFolderIcon className={classes.leftIcon} />
            ) : (
                <ClosedFolderIcon className={classes.leftIcon} />
            )}
            <ListItemText disableTypography className={classes.listText}>
                {title}
            </ListItemText>
            <ListItemText
                disableTypography
                className={`${classes.listText} ${classes.textAlignRight}`}
            >
                {`(${numOfItems})`}
                {isOpen ? (
                    <UpArrowIcon className={classes.rightIcon} />
                ) : (
                    <DownArrowIcon className={classes.rightIcon} />
                )}
            </ListItemText>
        </ListItem>
    )
}
export default PrimaryListItem
