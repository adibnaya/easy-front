import { createStyles, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) =>
    createStyles({
        secondList: {
            backgroundColor: theme.colors.drawerHeader,
        },
        dividerContainer: {
            backgroundColor: theme.colors.drawerHeader,
        },
        divider: {
            backgroundColor: '#7A96CE4F',
            margin: '0 20px',
        },
    })
)

export default useStyles
