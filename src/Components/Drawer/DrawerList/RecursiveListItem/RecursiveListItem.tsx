import React from 'react'
import { useSelector } from 'react-redux'
import { Collapse, List } from '@material-ui/core'
import Divider from '@material-ui/core/Divider/Divider'
import useStyles from './RecursiveListItem.style'
import PrimaryListItem from '../PrimaryListItem/PrimaryListItem'
import SecondaryListItem from '../SecondaryListItem/SecondaryListItem'
import { Page } from '../../../../Containers/DrawerAndBar/DrawerReducer'
import { RootState } from '../../../../Redux/Store'

interface Props {
    page: Page
    closeDrawer?: () => void
    handleChildClick: (slug: string) => void
    depth?: number
    parentSlug?: string
}

export const variables = {
    depth: 3,
}

const RecursiveListItem = ({
    page,
    handleChildClick,
    depth = 0,
    parentSlug,
}: Props) => {
    const { id, name, sub_pages } = page
    const classes = useStyles()
    const { location } = useSelector((state: RootState) => state.router)

    const { pathname } = location
    const paths = pathname.split('/')
    const isSelected = paths[depth + variables.depth] === page.slug
    const [open, setOpen] = React.useState(paths[depth + 3] === page.slug)
    const urlSlug = `${parentSlug}/${page.slug}`

    const handleClick = () => {
        setOpen(!open)
    }

    if (sub_pages.length === 0) {
        return (
            <SecondaryListItem
                key={id}
                title={name}
                isSelected={isSelected}
                handleClick={() => handleChildClick(urlSlug)}
            />
        )
    }

    return (
        <List className={`${open ? classes.secondList : ''}`}>
            <PrimaryListItem
                title={name}
                handleClick={handleClick}
                numOfItems={sub_pages.length}
                isOpen={open}
            />

            {open && (
                <div className={classes.dividerContainer}>
                    <Divider classes={{ root: classes.divider }} />
                </div>
            )}

            <Collapse in={open} timeout="auto" unmountOnExit>
                {sub_pages.map((pa) => {
                    return (
                        <RecursiveListItem
                            key={pa.id}
                            page={pa}
                            depth={depth + 1}
                            handleChildClick={(slug) => handleChildClick(slug)}
                            parentSlug={urlSlug}
                        />
                    )
                })}
            </Collapse>
        </List>
    )
}

export default RecursiveListItem
