import { createStyles, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) =>
    createStyles({
        secondList: {
            // backgroundColor: theme.colors.drawerHeader,
        },
        pageItem: {
            height: 37,
            fontFamily: theme.fonts.primaryText,
            fontSize: theme.sizes.secondaryText,
            width: 'calc(100% - 10px)',
            borderBottomRightRadius: 20,
            borderTopRightRadius: 20,
            padding: '0 0 0 19px',
            margin: '8px 0 8px 0',
            '&:hover': {
                backgroundColor: theme.colors.secondary,
            },
        },
        selectedBackground: {
            backgroundColor: theme.colors.primary,
        },
        listText: {
            display: 'flex',
            alignItems: 'center',
            color: theme.colors.white,
            fontFamily: theme.fonts.primaryText,
            fontSize: theme.sizes.secondaryText,
        },
        listPageText: {
            paddingLeft: 31,
        },
    })
)

export default useStyles
