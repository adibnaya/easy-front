import ListItemText from '@material-ui/core/ListItemText'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import React from 'react'
import useStyles from './SecondaryListItem.style'

interface Props {
    title: string
    isSelected: boolean
    handleClick: () => void
}
const SecondaryListItem = ({ title, isSelected, handleClick }: Props) => {
    const classes = useStyles()
    return (
        <List className={classes.secondList} component="div" disablePadding>
            <ListItem
                button
                onClick={() => handleClick()}
                className={`${classes.pageItem} ${
                    isSelected ? classes.selectedBackground : ''
                }`}
            >
                <ListItemText
                    disableTypography
                    className={`${classes.listText} ${classes.listPageText}`}
                >
                    {title}
                </ListItemText>
            </ListItem>
        </List>
    )
}

export default SecondaryListItem
