import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            cursor: 'pointer',
            height: 73,
            width: '100%',
            backgroundColor: theme.colors.drawerHeader,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
            justifyContent: 'space-between',
        },
        tabContainer: {
            height: 62,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'flex-end',
            flex: 1,
            color: theme.colors.white,
            paddingBottom: 10,
            textTransform: 'capitalize',
        },
        selectedTabContainer: {
            color: theme.colors.drawerTabsText,
            paddingBottom: 8,
        },
        img: {
            marginBottom: 9,
        },
    })
)

export default useStyles
