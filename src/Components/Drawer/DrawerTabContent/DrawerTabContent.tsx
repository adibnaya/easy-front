import React from 'react'
import useStyles from './DrawerTabContent.style'
import TextBox from '../../TextBox/TextBox'

interface TabContentProps {
    index: number
    title: string
    tabsValue: number
    renderIcon: () => React.ReactNode
    renderSelectedIcon: () => React.ReactNode
}
const DrawerTabContent = ({
    index,
    title,
    tabsValue,
    renderIcon,
    renderSelectedIcon,
}: TabContentProps) => {
    const classes = useStyles()
    const isSelected = tabsValue === index
    return (
        <div>
            <TextBox
                variant="subtitle1"
                className={`${classes.tabContainer} ${
                    isSelected ? classes.selectedTabContainer : ''
                }`}
            >
                {!isSelected && renderIcon()}
                {isSelected && renderSelectedIcon()}
                {title}
            </TextBox>
        </div>
    )
}

export default DrawerTabContent
