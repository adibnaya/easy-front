import React from 'react'

interface TabPanelProps {
    children?: React.ReactNode
    index: any
    value: any
    className?: string
}

const DrawerTabPanel = (props: TabPanelProps) => {
    const { children, value, index, className, ...other } = props

    return (
        <div
            style={{ backgroundColor: '#1C1C1C' }}
            role="tabpanel"
            className={className || ''}
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && <div>{children}</div>}
        </div>
    )
}

export default DrawerTabPanel
