import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            cursor: 'pointer',
            height: 73,
            width: '100%',
            backgroundColor: theme.colors.drawerHeader,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
            justifyContent: 'space-between',
        },
        tabContainer: {
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'flex-end',
            flex: 1,
            color: theme.colors.white,
            fontFamily: theme.fonts.primaryText,
            fontSize: theme.sizes.mediumText,
            paddingBottom: 10,
        },
        selectedTabContainer: {
            color: theme.colors.drawerTabsText,
            borderBottom: '3px solid #67B1FA',
            paddingBottom: 7,
        },
        img: {
            marginBottom: 9,
        },
    })
)

export default useStyles
