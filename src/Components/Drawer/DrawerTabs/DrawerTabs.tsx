import React from 'react'
import { ReactComponent as FavoriteWhiteIcon } from '../../../Assets/favorite_white.svg'
import { ReactComponent as FavoriteBlueIcon } from '../../../Assets/favorite_blue.svg'
import { ReactComponent as AllPagesWhiteIcon } from '../../../Assets/all_pages_white.svg'
import { ReactComponent as AllPagesBlueIcon } from '../../../Assets/all_pages_blue.svg'
import useStyles from './DrawerTabs.style'
import TextBox from '../../TextBox/TextBox'

export enum SelectedTabEnum {
    AllPages = 'allPages',
    Favorite = 'favorite',
}

export type SelectedTab = 'allPages' | 'favorite'

interface Props {
    setTab: (type) => void
    selectedTab: SelectedTab
}

const DrawerTabs = ({ setTab, selectedTab }: Props) => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <TextBox
                variant="subtitle1"
                className={`${classes.tabContainer} ${
                    selectedTab === SelectedTabEnum.AllPages
                        ? classes.selectedTabContainer
                        : ''
                }`}
                onClick={() => setTab(SelectedTabEnum.AllPages)}
            >
                {selectedTab === SelectedTabEnum.AllPages && (
                    <AllPagesBlueIcon className={classes.img} />
                )}
                {selectedTab !== SelectedTabEnum.AllPages && (
                    <AllPagesWhiteIcon className={classes.img} />
                )}
                All Pages
            </TextBox>
            <TextBox
                variant="subtitle1"
                className={`${classes.tabContainer} ${
                    selectedTab === SelectedTabEnum.Favorite
                        ? classes.selectedTabContainer
                        : ''
                }`}
                onClick={() => setTab(SelectedTabEnum.Favorite)}
            >
                {selectedTab === SelectedTabEnum.Favorite && (
                    <FavoriteBlueIcon className={classes.img} />
                )}
                {selectedTab !== SelectedTabEnum.Favorite && (
                    <FavoriteWhiteIcon className={classes.img} />
                )}
                Favorite
            </TextBox>
        </div>
    )
}

export default DrawerTabs
