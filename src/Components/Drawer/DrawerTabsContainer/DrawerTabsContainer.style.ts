import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        indicator: {
            display: 'none',
        },
        tabsContainer: {
            backgroundColor: '#262C31',
            opacity: 1,
        },
    })
)

export default useStyles
