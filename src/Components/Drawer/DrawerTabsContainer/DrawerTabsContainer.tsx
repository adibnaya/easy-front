import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import useStyles from './DrawerTabsContainer.style'

interface Props {
    handleTabsChange: any
    value: number
    children?: React.ReactNode
}
const DrawerTabsContainer = ({ handleTabsChange, value, children }: Props) => {
    const classes = useStyles()
    return (
        <AppBar position="static">
            <Tabs
                TabIndicatorProps={{ className: classes.indicator }}
                classes={{
                    root: classes.tabsContainer,
                }}
                variant="fullWidth"
                value={value}
                onChange={handleTabsChange}
                aria-label="drawer tabs"
            >
                {children}
            </Tabs>
        </AppBar>
    )
}

export default DrawerTabsContainer
