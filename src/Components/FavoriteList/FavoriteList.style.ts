import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        listContainer: {
            width: 'calc(100% - 40px)',
            height: 'calc(100% - 40px)',
            backgroundColor: '#FFFAE9',
            color: theme.colors.selectText,
            position: 'relative',
            padding: 20,
            boxShadow: '0px 3px 6px #D3D3E9CF',
            minWidth: 255,
            borderRadius: 3,
        },
        listTitle: {
            marginBottom: 33,
        },
        listItem: {
            display: 'flex',
            flexDirection: 'row',
            marginBottom: 25,
            alignItems: 'center',
        },
        favoriteIconContainer: {
            width: 15,
            height: 15,
            borderRadius: 20,
            backgroundColor: '#FFCF2C',
            marginRight: 10,
            padding: 3,
            '&:hover': {
                cursor: 'pointer',
                backgroundColor: '#A3A5BD',
            },
        },
        workingIconContainer: {
            position: 'absolute',
            bottom: 0,
            right: 0,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
        },
        url: {
            display: 'flex',
            width: 'calc(100% - 21px)',
            height: '100%',
            '&:hover': {
                fontWeight: 'bolder',
                cursor: 'pointer',
            },
        },
        circleSkeleton: {
            margin: '0 15px 15px 0',
        },
    })
)

export default useStyles
