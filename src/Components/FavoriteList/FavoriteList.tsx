import React from 'react'
import InfiniteScroll from 'react-infinite-scroll-component'
import Skeleton from 'react-loading-skeleton'
import { useTranslation } from 'react-i18next'
import useStyles from './FavoriteList.style'
import { ReactComponent as WorkingIcon } from '../../Assets/working_2.svg'
import TextBox from '../TextBox/TextBox'
import FavoriteListItem from './FavoriteListItem/FavoriteListItem'

interface Props {
    pages: any
    onClickPage: (url) => void
    onRemovePage: (id) => void
    fetchMoreData: () => void
    hasMore: boolean
}

const FavoriteList = ({
    pages,
    onClickPage,
    fetchMoreData,
    hasMore,
    onRemovePage,
}: Props) => {
    const classes = useStyles()
    const { t } = useTranslation()
    return (
        <div className={classes.listContainer}>
            <TextBox variant="h4" className={classes.listTitle}>
                {t('dashboard.favoriteList.title')}
            </TextBox>
            <InfiniteScroll
                dataLength={pages.length}
                next={fetchMoreData}
                hasMore={hasMore}
                loader={
                    <div>
                        <Skeleton
                            className={classes.circleSkeleton}
                            circle
                            height={21}
                            width={21}
                        />
                        <Skeleton height={21} width="80%" />
                        <Skeleton
                            className={classes.circleSkeleton}
                            circle
                            height={21}
                            width={21}
                        />
                        <Skeleton height={21} width="80%" />
                    </div>
                }
                height={450}
                style={{ paddingRight: '20px' }}
            >
                {pages.map((p) => (
                    <FavoriteListItem
                        key={p.id}
                        page={p}
                        onClickPage={(url) => onClickPage(url)}
                        onClickRemoveFavorite={(pageId) => onRemovePage(pageId)}
                    />
                ))}
            </InfiniteScroll>
            <div className={classes.workingIconContainer}>
                <WorkingIcon />
            </div>
        </div>
    )
}

export default FavoriteList
