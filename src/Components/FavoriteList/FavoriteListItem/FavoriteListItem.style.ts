import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        listItem: {
            display: 'flex',
            flexDirection: 'row',
            marginBottom: 25,
            alignItems: 'center',
        },
        favoriteIconContainer: {
            width: 15,
            height: 15,
            borderRadius: 20,
            backgroundColor: '#FFCF2C',
            marginRight: 10,
            padding: 3,
            '&:hover': {
                cursor: 'pointer',
                backgroundColor: '#A3A5BD',
            },
        },
        url: {
            display: 'flex',
            width: 'calc(100% - 21px)',
            height: '100%',
            '&:hover': {
                fontWeight: 'bolder',
                cursor: 'pointer',
            },
        },
    })
)

export default useStyles
