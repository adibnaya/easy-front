import React from 'react'
import { ReactComponent as FavoriteIcon } from '../../../Assets/favorite_white.svg'
import TextBox from '../../TextBox/TextBox'
import useStyles from './FavoriteListItem.style'

interface FavoritePage {
    id: string
    title: string
    url: string
}

interface Props {
    page: FavoritePage
    onClickPage: (url: string) => void
    onClickRemoveFavorite: (id: string) => void
}

const FavoriteListItem = ({
    page,
    onClickPage,
    onClickRemoveFavorite,
}: Props) => {
    const classes = useStyles()
    return (
        <div className={classes.listItem}>
            <FavoriteIcon
                onClick={() => onClickRemoveFavorite(page.id)}
                className={classes.favoriteIconContainer}
            />
            <TextBox
                onClick={() => onClickPage(page.url)}
                variant="body2"
                className={classes.url}
            >
                {page.title}
            </TextBox>
        </div>
    )
}

export default FavoriteListItem
