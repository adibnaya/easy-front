import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            position: 'relative',
        },
        workingImg: {
            position: 'relative',
            zIndex: 10,
            marginLeft: 50,
        },
        blueRow: {
            display: 'flex',
            position: 'absolute',
            bottom: 0,
            backgroundColor: '#D0E6FC',
            height: 192,
            borderRadius: 3,
            width: '100%',
            alignItems: 'center',
            justifyContent: 'flex-end',
        },
        hiText: {
            marginBottom: 8,
        },
        summaryText: {
            marginRight: 140,
        },
    })
)

export default useStyles
