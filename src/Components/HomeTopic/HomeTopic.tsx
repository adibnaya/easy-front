import React from 'react'
import { useTranslation } from 'react-i18next'
import { ReactComponent as WorkingIcon } from '../../Assets/working.svg'
import useStyles from './HomeTopic.style'
import TextBox from '../TextBox/TextBox'

interface Props {
    name: string
    className?: string
}
const HomeTopic = ({ name, className }: Props) => {
    const classes = useStyles()
    const { t } = useTranslation()

    return (
        <div className={`${classes.container} ${className || ''}`}>
            <WorkingIcon className={classes.workingImg} />
            <div className={classes.blueRow}>
                <div>
                    <TextBox variant="h1" className={classes.hiText}>
                        {t('dashboard.homeTopic.title')}
                        {' '}
                        {name}
                    </TextBox>
                    <TextBox variant="h4" className={classes.summaryText}>
                        {t('dashboard.homeTopic.topicText')}
                    </TextBox>
                </div>
            </div>
        </div>
    )
}

export default HomeTopic
