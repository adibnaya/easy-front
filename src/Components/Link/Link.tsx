import { withStyles, Theme as AugmentedTheme } from '@material-ui/core/styles'
import Link from '@material-ui/core/Link/Link'

const AppLink = withStyles((theme: AugmentedTheme) => ({
    root: {
        fontFamily: theme.fonts.primaryText,
        cursor: 'pointer',
        color: '#699FF3',
        textUnderlinePosition: 'under',
    },
}))(Link)

export default AppLink
