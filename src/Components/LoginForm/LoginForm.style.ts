import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        a: {
            color: '#08c',
        },
        code: {
            background: '#eee',
            padding: '.1rem',
            fontFamily: 'Menlo',
            fontSize: '13px',
            color: '#ff00aa',
        },
        container: {
            width: '100%',
            color: 'white',
        },
        input: {
            padding: '7px',
            fontSize: 16,
            width: 'calc(100% - 14px)',
            display: 'block',
            borderRadius: 4,
            border: '1px solid #ccc',
            '&:focus': {
                borderColor: '#007eff',
                boxShadow:
                    'inset 0 1px 1px rgba(0, 0, 0, 0.075),0 0 0 3px rgba(0, 126, 255, 0.1)',
                outline: 'none',
            },
        },
        inputError: {
            borderColor: 'red',
            boxShadow:
                'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 0 3px rgba(255, 0, 0, 0.1)',
        },
        label: {
            fontWeight: 'bold',
            display: 'block',
            margin: '8px 0 8px 0',
        },
        inputFeedback: {
            color: 'red',
            marginTop: '.25rem',
        },
        logo: {
            width: 240,
            height: 50,
            // marginLeft: 8,
        },
        button: {
            display: 'block',
            textAlign: 'center',
            width: '100%',
            margin: '40px 0 0 0',
            padding: '12px 20px',
            borderStyle: 'none',
            borderRadius: 5,
            backgroundColor: '#08c',
            boxShadow: '0px 2px 2px rgba(0, 0, 0, 0.15)',
            fontSize: 17,
            fontWeight: 500,
            color: '#fff',
            cursor: 'pointer',
            outline: 'none',
            // left: '72%',
            // position: 'relative',
            '&:disabled': {
                opacity: 0.5,
                cursor: 'not-allowed !important',
            },
        },
    })
)

export default useStyles
