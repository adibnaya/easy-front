import * as React from 'react'
import { withFormik, FormikProps, FormikErrors, Form, Field } from 'formik'
import useStyles from './LoginForm.style'
import bynetLogo from '../../Assets/bynet_logo.png'

// Shape of form values
export interface LoginFormValues {
    email: string
    password: string
}

interface OtherProps {
    message: string
}

const InnerForm = (props: OtherProps & FormikProps<LoginFormValues>) => {
    const { touched, errors, isSubmitting, message } = props
    const classes = useStyles()
    return (
        <Form className={classes.container}>
            <img className={classes.logo} src={bynetLogo} alt="bynet logo" />
            <h1>{message}</h1>
            <span className={classes.label}>Email</span>
            <Field
                type="email"
                name="email"
                placeholder="email"
                className={`${classes.input}
                    ${errors.email && touched.email ? classes.inputError : ''}`}
            />
            {touched.email && errors.email && (
                <div className={classes.inputFeedback}>{errors.email}</div>
            )}

            <span className={classes.label}>Password</span>
            <Field
                type="password"
                name="password"
                placeholder="password"
                className={`${classes.input}
                    ${
                        errors.password && touched.password
                            ? classes.inputError
                            : ''
                    }`}
            />
            {touched.password && errors.password && (
                <div className={classes.inputFeedback}>{errors.password}</div>
            )}

            <button
                className={classes.button}
                type="submit"
                disabled={isSubmitting}
            >
                Submit
            </button>
        </Form>
    )
}

// The type of props MyForm receives
interface MyFormProps {
    initialEmail?: string
    message: string // if this passed all the way through you might do this or make a union type
    handleSubmit: (cred: LoginFormValues) => Promise<string>
}

// Wrap our form with the withFormik HoC
const MyForm = withFormik<MyFormProps, LoginFormValues>({
    // Transform outer props into form values
    mapPropsToValues: (props) => {
        return {
            email: props.initialEmail || '',
            password: '',
        }
    },

    // Add a custom validation function (this can be async too!)
    validate: (values: LoginFormValues) => {
        const errors: FormikErrors<any> = {}
        if (!values.email) {
            errors.email = 'Required'
        } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)
        ) {
            errors.email = 'Invalid email address'
        }
        if (!values.password) {
            errors.password = 'Required'
        }
        return errors
    },

    handleSubmit: (values, props) => {
        const { handleSubmit } = props.props
        const { setSubmitting, resetForm } = props
        setSubmitting(true)
        handleSubmit({
            email: values.email,
            password: values.password,
        })
            .then((res) => {
                resetForm()
                setSubmitting(false)
            })
            .catch((er) => {
                resetForm()
                setSubmitting(false)
            })
    },
})(InnerForm)

export default MyForm
