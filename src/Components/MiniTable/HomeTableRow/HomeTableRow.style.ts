import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

export enum DataNameEnum {
    ip = 'IP',
    mac = 'MAC',
    high = 'High',
    medium = 'Medium',
    low = 'Low',
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
            fontSize: 16,
            textAlign: 'center',
            marginBottom: 20,
        },
        leftContainer: {
            display: 'flex',
            alignItems: 'center',
            marginRight: '60px',
            color: '#40414E',
        },
        general_pie_chart0: {
            color: '#3787D6',
        },
        general_pie_chart1: {
            color: '#67B1FA',
        },
        faults_pie_chart0: {
            color: '#FD5C63',
        },
        faults_pie_chart1: {
            color: '#F8911C',
        },
        faults_pie_chart2: {
            color: '#F8C51C',
        },
        name: {
            paddingLeft: 15,
            textTransform: 'capitalize',
        },
        value: {
            fontSize: 26,
            fontWeight: 'bold',
        },
    })
)

export default useStyles
