import React from 'react'
import Icon from '@material-ui/core/Icon'
import useStyles from './HomeTableRow.style'
import { ReactComponent as HighIcon } from '../../../Assets/warning_high.svg'
import { ReactComponent as MediumIcon } from '../../../Assets/warning_medium.svg'
import { ReactComponent as LowIcon } from '../../../Assets/warning_low.svg'
import TextBox from '../../TextBox/TextBox'

export interface TableRowData {
    title: string
    value: number
    icon: string | null
}
export interface Props {
    datum: TableRowData
    index: number
    type: string
}

const tryRequire = (path) => {
    try {
        const IconLocal = require(`../../../Assets/${path}.svg`)
        return <IconLocal />
    } catch (err) {
        return <Icon>{path}</Icon>
    }
}

const renderIcon = (type: string | null, index, icon) => {
    if (type === 'general_pie_chart') {
        if (index === 0) {
            return tryRequire(icon)
        }
        if (index === 1) {
            return tryRequire(icon)
        }
    } else if (type === 'faults_pie_chart') {
        if (index === 0) {
            return <HighIcon />
        }
        if (index === 1) {
            return <MediumIcon />
        }
        if (index === 2) {
            return <LowIcon />
        }
    }
    return <LowIcon />
}

const HomeTableRow = ({ datum, index, type }: Props) => {
    const { title, value, icon } = datum
    const classes = useStyles()
    return (
        <div className={`${classes.container} ${classes[type + index]}`}>
            <div className={classes.leftContainer}>
                {renderIcon(type, index, icon)}
                <TextBox className={classes.name}>{title}</TextBox>
            </div>
            <div>
                <TextBox variant="h3" className={classes.value}>
                    {value}
                </TextBox>
            </div>
        </div>
    )
}

export default HomeTableRow
