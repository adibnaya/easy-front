import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            margin: '26px 0 0 25px',
            minWidth: 154,
        },
        titleContainer: {
            width: '100%',
            borderBottom: '1px solid #EBEBF0',
            marginBottom: 20,
            paddingBottom: 10,
        },
    })
)

export default useStyles
