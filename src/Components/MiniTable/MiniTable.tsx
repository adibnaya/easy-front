import React from 'react'
import useStyles from './MiniTable.style'
import TextBox from '../TextBox/TextBox'

interface Props {
    title: string
    data: any[]
    renderRow: (datum, i) => any
}

const MiniTable = ({ title, renderRow, data }: Props) => {
    const classes = useStyles()
    return (
        <div className={classes.container}>
            <div className={classes.titleContainer}>
                <TextBox variant="h4">{title}</TextBox>
            </div>
            {Array.isArray(data) && data.map((datum, i) => renderRow(datum, i))}
        </div>
    )
}

export default MiniTable
