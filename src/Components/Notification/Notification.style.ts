import { createStyles, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) =>
    createStyles({
        container: {
            position: 'relative',
        },
        notificationOn: {
            position: 'absolute',
            top: 3,
            right: 0,
        },
    })
)

export default useStyles
