import React from 'react'
import useStyles from './Notification.style'
import { ReactComponent as NotificationIcon } from '../../Assets/notification.svg'
import { ReactComponent as NotificationOnIcon } from '../../Assets/notification_on.svg'

interface Props {
    notificationOn: boolean
}

const Notification = ({ notificationOn }: Props) => {
    const classes = useStyles()
    return (
        <div className={classes.container}>
            <NotificationIcon />
            {notificationOn && (
                <NotificationOnIcon className={classes.notificationOn} />
            )}
        </div>
    )
}

export default Notification
