import React from 'react'
import { PieChart } from 'react-minimal-pie-chart'

export interface PieChartData {
    name: string
    value: number
    color: string
}

interface Props {
    data: PieChartData[]
    type: string
    className: string
}

const CustomPieChart = ({ data, type, className }: Props) => {
    // sum the values of data array
    const total = data.map((datum) => datum.value).reduce((a, c) => a + c)
    return (
        <div className={className || ''}>
            <PieChart
                paddingAngle={3}
                lineWidth={15}
                label={({ dataIndex }) => {
                    return dataIndex === 0 ? total : ''
                }}
                labelStyle={{
                    fontSize: '31px',
                    fontWeight: 'bold',
                    fill: '#40414E',
                }}
                labelPosition={0}
                data={data}
            />
        </div>
    )
}

export default CustomPieChart
