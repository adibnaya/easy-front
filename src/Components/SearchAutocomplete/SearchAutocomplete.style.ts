import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            height: 29,
            padding: '0 !important',
            borderRadius: 200,
            backgroundColor: 'white',
        },
        popper: {
            boxShadow: '0px 3px 6px #D3D3E9CF',
            top: '4px !important',
        },
        paper: {
            margin: 0,
            boxShadow: 'none',
        },
        options: {
            padding: 0,
            margin: 0,
            fontSize: '13px',
            '& li': {
                borderRadius: 3,
                padding: '6px 0 0 15px',
                backgroundColor: theme.colors.selectBackground,
                minHeight: 39,
                width: '100%',
                lineHeight: '39px',
            },
            '& li[data-focus="true"]': {
                backgroundColor: theme.colors.secondary,
                color: theme.colors.selectTextSelected,
                cursor: 'pointer',
            },
            '& li:active': {
                backgroundColor: theme.colors.primary,
                color: theme.colors.selectTextSelected,
            },
            '& li[aria-selected="true"]': {
                backgroundColor: theme.colors.primary,
                color: theme.colors.selectTextSelected,
            },
        },
        input: {
            backgroundColor: '#F4F5FC',
            height: '27px',
            fontSize: '13px',
            color: '#8C8C97',
            fontFamily: theme.fonts.primaryText,
            padding: '0 20px 0 15px !important',
            border: '1px solid white',
            borderRadius: 200,
            '&:hover': {
                border: '1px solid #D9DAE4',
            },
            '&:focus': {
                border: '1px solid #D9DAE4',
                color: '#40414E',
            },
        },
        selectedInput: {
            // border: '1px solid #D9DAE4',
            color: '#40414E',
        },
        notchedOutline: {
            border: 0,
        },
        inputAdornment: {
            position: 'absolute',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            right: 0,
            width: 23,
            height: 23,
            borderRadius: 20,
            border: 'white 2px solid',
        },
    })
)

export default useStyles
