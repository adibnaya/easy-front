import React, { useState } from 'react'
import { InputAdornment, TextField } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import parse from 'autosuggest-highlight/parse'
import match from 'autosuggest-highlight/match'
import { ReactComponent as SearchIconWhite } from '../../Assets/search-white.svg'
import useStyles from './SearchAutocomplete.style'
import TextBox from '../TextBox/TextBox'

export interface Option {
    title: string
    key: number
    slug?: string
}
interface Props {
    data: Option[]
    placeholder: string
    onOptionClicked: (e) => void
    width?: number
}

const SearchAutocomplete = ({
    data,
    placeholder,
    onOptionClicked,
    width,
    ...props
}: Props) => {
    const classes = useStyles()
    const [open, setOpen] = useState(false)
    const [value, setValue] = useState(null)
    const onChange = (e, v) => {
        if (v) {
            setValue(v.title)
            onOptionClicked(v)
        }
    }

    return (
        <Autocomplete
            onChange={(e, v) => onChange(e, v)}
            style={{ width }}
            options={data}
            getOptionLabel={(option) => option.title}
            classes={{
                listbox: classes.options,
                popper: classes.popper,
                paper: classes.paper,
            }}
            renderInput={(params) => (
                <TextField
                    {...params}
                    variant="outlined"
                    label={false}
                    onFocus={() => setOpen(true)}
                    onBlur={() => setOpen(false)}
                    inputProps={{
                        ...params.inputProps,
                        placeholder,
                    }}
                    InputProps={{
                        ...params.InputProps,
                        notched: false,
                        classes: {
                            root: classes.root,
                            input: `${classes.input} ${
                                value ? classes.selectedInput : ''
                            }`,
                            notchedOutline: classes.notchedOutline,
                        },
                        endAdornment: (
                            <InputAdornment
                                style={{
                                    backgroundColor: open
                                        ? '#40414E'
                                        : '#A3A5BD',
                                    // open || value ? '#40414E' : '#A3A5BD',
                                }}
                                className={classes.inputAdornment}
                                position="end"
                            >
                                <SearchIconWhite />
                            </InputAdornment>
                        ),
                    }}
                />
            )}
            renderOption={(option, { inputValue }) => {
                const matches = match(option.title, inputValue)
                const parts = parse(option.title, matches)

                return (
                    <div>
                        {parts.map((part, index) => (
                            <TextBox
                                display="inline"
                                variant="body2"
                                key={index}
                                style={{
                                    fontWeight: part.highlight ? 700 : 400,
                                }}
                            >
                                {part.text}
                            </TextBox>
                        ))}
                    </div>
                )
            }}
        />
    )
}

export default SearchAutocomplete
