import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            height: 29,
            padding: '0 !important',
            borderRadius: 200,
            backgroundColor: 'white',
        },
        input: {
            backgroundColor: '#F4F5FC',
            height: '27px',
            fontSize: '13px',
            color: '#8C8C97',
            padding: '0 20px 0 15px !important',
            fontFamily: theme.fonts.primaryText,
            border: '1px solid white',
            borderRadius: 200,
            '&:hover': {
                border: '1px solid #D9DAE4',
            },
            '&:focus': {
                border: '1px solid #D9DAE4',
                color: '#40414E',
            },
        },
        selectedInput: {
            // border: '1px solid #D9DAE4',
            color: '#40414E',
        },
        notchedOutline: {
            border: 0,
        },
        inputAdornment: {
            position: 'absolute',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            right: 0,
            width: 23,
            height: 23,
            borderRadius: 20,
            border: 'white 2px solid',
        },
    })
)

export default useStyles
