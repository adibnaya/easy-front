import { InputAdornment, TextField } from '@material-ui/core'
import React, { useState } from 'react'
import { ReactComponent as SearchIconWhite } from '../../Assets/search-white.svg'
import useStyles from './SearchInput.style'

interface Props {
    placeholder: string
    onInputChanged: (e) => void
    width?: number
}

const SearchInput = ({ placeholder, onInputChanged, width }: Props) => {
    const classes = useStyles()
    const [focus, setFocus] = useState(false)
    const [value, setValue] = useState(null)
    const onChange = (e) => {
        setValue(e.target.value)
        onInputChanged(e.target.value)
    }
    return (
        <TextField
            variant="outlined"
            label={false}
            inputProps={{
                placeholder,
            }}
            onChange={onChange}
            onFocus={() => setFocus(true)}
            onBlur={() => setFocus(false)}
            // onMouseOutCapture={() => setFocus(false)}
            InputProps={{
                notched: false,
                classes: {
                    root: classes.root,
                    input: `${classes.input} ${
                        value ? classes.selectedInput : ''
                    }`,
                    notchedOutline: classes.notchedOutline,
                },
                endAdornment: (
                    <InputAdornment
                        style={{
                            backgroundColor: focus ? '#40414E' : '#A3A5BD',
                            // open || value ? '#40414E' : '#A3A5BD',
                        }}
                        className={classes.inputAdornment}
                        position="end"
                    >
                        <SearchIconWhite />
                    </InputAdornment>
                ),
            }}
        />
    )
}

export default SearchInput
