import { createStyles, Theme, withStyles } from '@material-ui/core'
import InputBase from '@material-ui/core/InputBase'

const FilledSelectInput = withStyles((theme: Theme) =>
    createStyles({
        root: {
            fontFamily: theme.fonts.primaryText,
            fontSize: theme.sizes.secondaryText,
            backgroundColor: theme.colors.selectBackground,
            height: '29px',
            borderRadius: '300px',
            width: '100%',
        },
        input: {
            padding: '6px 0 0 15px',
            backgroundColor: theme.colors.selectBackground,
            height: '23px',
            borderRadius: '300px',
            '&:hover': {
                backgroundColor: theme.colors.selectBackground,
                border: '1px solid #D9DAE4',
                borderRadius: '300px',
                height: '23px',
                padding: '6px 0 0 14px',
            },
            '&:focus': {
                backgroundColor: theme.colors.selectBackground,
                height: '23px',
                borderRadius: '300px',
            },
        },
    })
)(InputBase)

export default FilledSelectInput
