import React from 'react'
import Select, { SelectProps } from '@material-ui/core/Select'
import SelectItem from './SelectItem'
import FilledSelectInput from './FilledSelectInput'
import SelectInput from './SelectInput'

interface Item {
    text: string
    value: string
}

interface Props extends SelectProps {
    items: Item[]
    setSelectedLabel: (r) => void
    filled?: boolean
}

const CustomSelect = ({ items, setSelectedLabel, filled, ...props }: Props) => {
    const [label, setLabel] = React.useState(items[0].value)
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setLabel(event.target.value as string)
        setSelectedLabel(items[event.target.value as number].text)
    }
    return (
        <Select
            value={label}
            onChange={handleChange}
            input={filled ? <FilledSelectInput /> : <SelectInput />}
            {...props}
        >
            {items &&
                items.map((item) => (
                    <SelectItem key={item.value} value={item.value}>
                        {item.text}
                    </SelectItem>
                ))}
        </Select>
    )
}

export default CustomSelect
