import { createStyles, Theme, withStyles } from '@material-ui/core'
import InputBase from '@material-ui/core/InputBase'

const FilledSelectInput = withStyles((theme: Theme) =>
    createStyles({
        root: {
            fontFamily: theme.fonts.primaryText,
            fontSize: theme.sizes.secondaryText,
            height: '29px',
            width: '100%',
        },
        input: {
            padding: '6px 0 0 15px',
            backgroundColor: theme.colors.white,
            height: '23px',
            '&:hover': {
                backgroundColor: `${theme.colors.selectBackground.toString()}!important`,
            },
            '&:focus': {
                backgroundColor: theme.colors.white,
            },
        },
    })
)(InputBase)

export default FilledSelectInput
