import { createStyles, Theme, withStyles } from '@material-ui/core'
import MenuItem from '@material-ui/core/MenuItem'

const SelectItem = withStyles((theme: Theme) =>
    createStyles({
        root: {
            backgroundColor: theme.colors.selectBackground,
            fontFamily: theme.fonts.primaryText,
            fontSize: theme.sizes.secondaryText,
            borderRadius: '3px',
            '&:hover': {
                color: theme.colors.selectTextSelected,
                backgroundColor: theme.colors.secondary,
            },
            '&:focus': {
                backgroundColor: theme.colors.primary,
            },
            '&$selected': {
                backgroundColor: theme.colors.primary,
                color: theme.colors.selectTextSelected,
                '&:hover': {
                    backgroundColor: theme.colors.primary,
                    color: theme.colors.selectTextSelected,
                },
            },
        },
        selected: {},
    })
)(MenuItem)

export default SelectItem
