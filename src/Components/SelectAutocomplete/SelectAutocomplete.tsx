import React, { useState } from 'react'
import { TextField } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import useStyles from './SelectAutocomplete.style'

interface Option {
    title: string
    key: number
}
interface Props {
    data: Option[]
    placeholder?: string
    onOptionClicked: (e) => void
    width?: number
    selectedOptionIndex?: number
    disableClearable?: boolean
    validateFunc?: (value) => boolean
}

const SelectAutocomplete = ({
    data,
    placeholder,
    onOptionClicked,
    width,
    disableClearable,
    selectedOptionIndex,
    validateFunc,
    ...props
}: Props) => {
    const classes = useStyles()

    const [isValid, setIsValid] = useState(true)

    const onChange = (e, v) => {
        onOptionClicked(v)
    }

    const onBlurValidation = () => {
        if (validateFunc) {
            setIsValid(validateFunc(selectedOptionIndex))
        }
    }

    const onItemSelect = (event, item: Option | null) => {
        setIsValid(true)
        onChange(event, item)
    }

    return (
        <Autocomplete
            onChange={(e, v) => onItemSelect(e, v)}
            style={{ width }}
            options={data}
            getOptionLabel={(option) => option.title}
            disableClearable={disableClearable}
            classes={{
                listbox: classes.options,
                popper: classes.popper,
                paper: classes.paper,
            }}
            value={
                selectedOptionIndex !== undefined && selectedOptionIndex >= 0
                    ? data[selectedOptionIndex]
                    : disableClearable
                    ? data[0]
                    : null
            }
            onBlur={onBlurValidation}
            renderInput={(params) => (
                <TextField
                    {...params}
                    variant="outlined"
                    label={false}
                    inputProps={{
                        ...params.inputProps,
                        placeholder,
                    }}
                    InputProps={{
                        ...params.InputProps,
                        notched: false,
                        classes: {
                            root: classes.root,
                            input: isValid
                                ? classes.input
                                : classes.inputInvalid,
                            notchedOutline: classes.notchedOutline,
                        },
                    }}
                />
            )}
        />
    )
}

export default SelectAutocomplete
