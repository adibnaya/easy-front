import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            borderRadius: 3,
        },
        paper: {
            borderRadius: 3,
            boxShadow: '0px 3px 6px #D3D3E9CF',
        },
        content: {
            borderRadius: 3,
            minWidth: 120,
            cursor: 'pointer',
        },
        contentItem: {
            padding: 10,
            width: '100%',
            minHeight: 30,
            lineHeight: '30px',
            '&:hover': {
                backgroundColor: '#F4F5FC',
            },
        },
        nameContainer: {
            borderRadius: 3,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            padding: 10,
            cursor: 'pointer',
            '&:hover': {
                backgroundColor: '#F4F5FC',
            },
        },
        arrowIcon: {
            marginLeft: 10,
        },
    })
)

export default useStyles
