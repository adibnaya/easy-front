import React from 'react'
import Popover from '@material-ui/core/Popover'
import { useTranslation } from 'react-i18next'
import { ReactComponent as DownBlackIcon } from '../../Assets/down_black.svg'
import useStyles from './SettingsPopper.style'

interface Props {
    name?: string
    handleLogOutClick: () => void
}

const SettingsPopper = ({ name, handleLogOutClick }: Props) => {
    const classes = useStyles()
    const { t } = useTranslation()
    const [anchorEl, setAnchorEl] = React.useState(null)

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }

    const handleLogOut = () => {
        handleLogOutClick()
        handleClose()
    }

    const open = Boolean(anchorEl)
    const id = open ? 'settings-popover' : undefined

    return (
        <div>
            <div
                className={classes.nameContainer}
                aria-describedby={id}
                onClick={handleClick}
            >
                {name || 'Name'} <DownBlackIcon className={classes.arrowIcon} />
            </div>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                classes={{
                    root: classes.root,
                    paper: classes.paper,
                }}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <div className={classes.content}>
                    <div onClick={handleLogOut} className={classes.contentItem}>
                        {t('drawerAndBar.settings.logout')}
                    </div>
                </div>
            </Popover>
        </div>
    )
}

export default SettingsPopper
