import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            cursor: 'pointer',
            width: 245,
            height: 214,
            flex: '0 1 auto',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            boxShadow: '0px 3px 6px #D3D3E9CF',
            borderRadius: '3px',
            backgroundColor: '#FBFBFD',
            color: theme.colors.selectText,
            '&:hover': {
                boxShadow: '0px 6px 16px #D3D3E9',
            },
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: 204,
            marginTop: 20,
        },
        text: {
            marginLeft: 16,
        },
        headerText: {
            marginLeft: 16,
        },
        headerRow: {
            display: 'flex',
            alignItems: 'center',
            marginLeft: 10,
        },
        number: {
            fontSize: 26,
            fontWeight: 'bold',
            marginRight: 10,
        },
        noNumber: {
            color: '#A3A5BD',
        },
        undiscovered: {
            color: '#F8911C',
        },
        inactive: {
            color: '#FD5C63',
        },
        secondRow: {
            backgroundColor: theme.colors.white,
            boxShadow: '0px 3px 6px #D3D3E9CF',
            borderRadius: 3,
            height: 49,
        },
        lastRow: {
            marginTop: `10px !important`,
        },
    })
)

export default useStyles
