import React from 'react'
import apicsIcon from '../../Assets/apic.png'
import useStyles from './SmallDataBox.style'
import { ReactComponent as WarningHighIcon } from '../../Assets/warning_high.svg'
import { ReactComponent as WarningGrayIcon } from '../../Assets/warning_gray.svg'
import { ReactComponent as WarningMediumIcon } from '../../Assets/warning_medium.svg'
import TextBox from '../TextBox/TextBox'

export enum SmallDataBoxDataTypeEnum {
    apics = 'APICS',
    ipn = 'IPN',
    leaf = 'LEAF',
    spins = 'SPINS',
}

interface SmallDataBoxData {
    total: number
    inactive: number
    undiscovered: number
}

interface Props {
    data: SmallDataBoxData | any
    icon: null | string
    className?: string
    title: string
    onClick: () => void
}

const getIcon = (type: string | null) => {
    switch (type) {
        case null:
            return <img src={apicsIcon} alt="default icon" />
        default:
            return <img src={apicsIcon} alt="default icon" />
    }
}

const SmallDataBox = ({ data, className, icon, title, onClick }: Props) => {
    const classes = useStyles()
    return (
        <div
            className={`${classes.container} ${className || ''}`}
            onClick={onClick}
        >
            <div className={classes.row}>
                <div className={classes.headerRow}>
                    {getIcon(icon)}
                    <TextBox variant="h4" className={classes.headerText}>
                        {title}
                    </TextBox>
                </div>
                <TextBox
                    variant="h3"
                    className={`${
                        data.total && data.total > 0
                            ? classes.number
                            : classes.noNumber
                    }`}
                >
                    {data.total && data.total > 0 ? data.total : '-'}
                </TextBox>
            </div>

            <div className={`${classes.row} ${classes.secondRow}`}>
                <div className={classes.headerRow}>
                    {data.inactive && data.inactive > 0 ? (
                        <WarningHighIcon />
                    ) : (
                        <WarningGrayIcon />
                    )}
                    <TextBox className={classes.headerText}>Inactive</TextBox>
                </div>
                <TextBox
                    variant="h3"
                    className={`${classes.number} ${
                        data.inactive && data.inactive > 0
                            ? classes.inactive
                            : classes.noNumber
                    }`}
                >
                    {data.inactive && data.inactive > 0 ? data.inactive : '-'}
                </TextBox>
            </div>
            <div
                className={`${classes.row} ${classes.secondRow} ${classes.lastRow}`}
            >
                <div className={classes.headerRow}>
                    {data.undiscovered && data.undiscovered > 0 ? (
                        <WarningMediumIcon />
                    ) : (
                        <WarningGrayIcon />
                    )}
                    <TextBox className={classes.headerText}>
                        Undiscovered
                    </TextBox>
                </div>
                <TextBox
                    variant="h3"
                    className={`${classes.number} ${
                        data.undiscovered && data.undiscovered > 0
                            ? classes.undiscovered
                            : classes.noNumber
                    }`}
                >
                    {data.undiscovered && data.undiscovered > 0
                        ? data.undiscovered
                        : '-'}
                </TextBox>
            </div>
        </div>
    )
}

export default SmallDataBox
