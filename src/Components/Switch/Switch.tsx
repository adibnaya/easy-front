import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch'
import { createStyles, Theme, withStyles } from '@material-ui/core'
import React from 'react'

interface Styles extends Partial<Record<SwitchClassKey, string>> {
    focusVisible?: string
}

interface Props extends SwitchProps {
    classes: Styles
}

const AppSwitch = withStyles((theme: Theme) =>
    createStyles({
        root: {
            transform: 'rotate(180deg)',
        },
        switchBase: {
            backgroundColor: '#A3A5BD',
            top: '8px',
            left: '7px',
            border: '1px solid #FFFFFF',
            padding: '7px',
            '&:hover': {
                backgroundColor: '#A3A5BD',
            },
            '& + $track': {
                backgroundColor: '#A3A5BD',
                opacity: 1,
            },
            '&$checked': {
                backgroundColor: theme.colors.primary,
                '&:hover': {
                    backgroundColor: theme.colors.primary,
                },
                '& + $track': {
                    opacity: 1,
                    backgroundColor: theme.colors.primary,
                },
            },
        },
        track: {},
        thumb: {
            width: '7px',
            height: '7px',
            backgroundColor: '#FFFFFF',
        },
        checked: {},
    })
)(({ classes, ...props }: Props) => {
    return (
        <Switch
            focusVisibleClassName={classes.focusVisible}
            disableRipple
            classes={{
                root: classes.root,
                switchBase: classes.switchBase,
                thumb: classes.thumb,
                track: classes.track,
                checked: classes.checked,
            }}
            {...props}
        />
    )
})

export default AppSwitch
