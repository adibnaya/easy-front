import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        table: {
            minWidth: 500,
            height: 100,
        },
        tableSpinnerContainer: {
            width: '100%',
            height: '500px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
        tableSpinner: {
            width: '100px !important',
            height: '100px !important',
        },
        headerRowText: {
            fontSize: theme.sizes.smallText,
            fontWeight: 'bold',
            backgroundColor: theme.colors.white,
            color: '#A3A5BD',
            textTransform: 'uppercase',
            fontFamily: theme.fonts.primaryText,
        },
        lastHeaderRowText: {
            paddingLeft: 0,
            paddingRight: 0,
        },
        bodyRowText: {
            fontSize: theme.sizes.secondaryText,
            color: '#40414E',
            paddingLeft: 15,
            fontFamily: theme.fonts.primaryText,
        },
        rootBodyRow: {
            height: 55,
            '&:hover': {
                backgroundColor: '#67B1FA0F',
            },
        },
        disabledRootBodyRow: {
            height: 55,
            opacity: 0.4,
        },
        selectedBodyRow: {
            backgroundColor: '#67B1FA24 !important',
        },
        exportButton: {
            marginLeft: 30,
            height: 29,
            width: 29,
            backgroundColor: 'blue',
        },
        actionsRow: {
            minHeight: 52,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderBottom: '1px solid rgba(224, 224, 224, 1)',
            padding: '16px 0 6px 0',
        },
        row: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
        },
        ellipsis: {
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
        },
        rowsIndicators: {
            alignSelf: 'flex-end',
            marginLeft: 7,
            fontSize: '10px',
            color: '#8C8C97',
        },
        title: {
            marginLeft: 15,
        },
    })
)

export default useStyles
