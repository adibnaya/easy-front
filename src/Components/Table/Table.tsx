import {
    TableContainer,
    TableBody,
    TableCell,
    TableRow,
    TableHead,
    Table,
    CircularProgress,
} from '@material-ui/core'
import React, { useState, useRef, useEffect } from 'react'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import { CSVLink } from 'react-csv'
import useStyles from './Table.style'
import AppButton from '../Button/Button'
import SearchInput from '../SearchInput/SearchInput'
import TextBox from '../TextBox/TextBox'

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
        return -1
    }
    if (b[orderBy] > a[orderBy]) {
        return 1
    }
    return 0
}

type Order = 'asc' | 'desc'

function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key
): (
    // eslint-disable-next-line
    a: { [key in Key]: number | string },
    // eslint-disable-next-line
    b: { [key in Key]: number | string }
) => number {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy)
}

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number])
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0])
        if (order !== 0) return order
        return a[1] - b[1]
    })
    return stabilizedThis.map((el) => el[0])
}

export interface Column {
    id: string
    label: string
    minWidth?: number
    align?: 'left' | 'right'
    renderCell?: (value, index, row, dispatch?) => React.ReactNode
    sortable?: boolean
    ellipsisWidth?: string
    showOnHover?: boolean
    noPadding?: boolean
    maxWidth?: number
}

interface Props {
    rows: any
    columns: any
    title: string
    totalRows: number
    selectedRow?: number
    onRowClick?: (rowData: any, index: number) => void
    defaultSortBy?: any
    renderPagination?: () => React.ReactNode
    headerRightSideComponent?: () => React.ReactNode
    exportToExcelFunction?: () => Promise<string>
    onSearchChanged?: (val: string) => void
    containerClassName?: string
    minHeight?: string
    maxHeight?: string
    search?: boolean
    dispatch?: any
    isLoading?: boolean
}

const CustomTable = ({
    rows,
    columns,
    title,
    selectedRow,
    onRowClick,
    defaultSortBy,
    renderPagination,
    headerRightSideComponent,
    totalRows,
    exportToExcelFunction,
    containerClassName,
    minHeight,
    maxHeight,
    search,
    onSearchChanged,
    isLoading,
    dispatch,
}: Props) => {
    const classes = useStyles()
    const [order, setOrder] = React.useState<Order>('asc')
    const [showAction, setShowAction] = React.useState(-1)
    const [orderBy, setOrderBy] = React.useState<any>(defaultSortBy)
    const [csvData, setCsvData] = useState('')
    const csvLink = useRef<CSVLink>(null)

    useEffect(() => {
        if (csvData !== '') {
            setTimeout(() => csvLink?.current?.link?.click(), 10)
        }
    }, [csvData])

    const handleRequestSort = (
        event: React.MouseEvent<unknown>,
        property: any
    ) => {
        const isAsc = orderBy === property && order === 'asc'
        setOrder(isAsc ? 'desc' : 'asc')
        setOrderBy(property)
    }

    const createSortHandler = (property: any) => (
        event: React.MouseEvent<unknown>
    ) => {
        handleRequestSort(event, property)
    }

    const renderTableCell = (
        column: Column,
        index: number,
        value,
        row,
        dispatch?
    ) => {
        if (column.showOnHover) {
            if (index === showAction) {
                if (column.renderCell) {
                    return column.renderCell(value, index, row, dispatch)
                }
                return value
            }
            return ''
        }
        if (column.renderCell) {
            return column.renderCell(value, index, row, dispatch)
        }

        return value
    }

    const handleExport = async () => {
        if (exportToExcelFunction) {
            exportToExcelFunction().then((data) => {
                setCsvData(data)
            })
        }
    }

    return (
        <div className={containerClassName}>
            <CSVLink
                ref={csvLink}
                filename="test.csv"
                data={csvData}
                target="_blank"
            />

            <div className={classes.actionsRow}>
                <div className={classes.row}>
                    <TextBox
                        variant="h4"
                        display="inline"
                        className={classes.title}
                    >
                        {title}
                    </TextBox>
                    <TextBox
                        className={classes.rowsIndicators}
                        display="inline"
                    >
                        ({rows.length}
                        {totalRows !== 0 && `/${totalRows}`})
                    </TextBox>
                </div>
                <div className={classes.row}>
                    {search && onSearchChanged && (
                        <SearchInput
                            placeholder="Search in table"
                            onInputChanged={onSearchChanged}
                        />
                    )}
                    {renderPagination && renderPagination()}
                    {exportToExcelFunction && (
                        <AppButton
                            onClick={handleExport}
                            className={classes.exportButton}
                        >
                            Export to Excel
                        </AppButton>
                    )}
                    {headerRightSideComponent && headerRightSideComponent()}
                </div>
            </div>
            <TableContainer style={{ maxHeight, minHeight, paddingRight: 20 }}>
                {isLoading ? (
                    <div className={classes.tableSpinnerContainer}>
                        <CircularProgress className={classes.tableSpinner} />
                    </div>
                ) : (
                    <Table
                        className={classes.table}
                        aria-label="custom pagination table"
                        stickyHeader
                    >
                        <TableHead>
                            <TableRow>
                                {columns.map((column, i) => (
                                    <TableCell
                                        key={`${column.id}${column.title}`}
                                        align={column.align}
                                        style={{
                                            minWidth: column.minWidth,
                                            maxWidth: column.maxWidth,
                                        }}
                                        className={`${classes.headerRowText} ${
                                            i === columns.length - 1
                                                ? classes.lastHeaderRowText
                                                : ''
                                        }`}
                                    >
                                        {!column.sortable && column.label}
                                        {column.sortable && (
                                            <TableSortLabel
                                                active={orderBy === column.id}
                                                direction={
                                                    orderBy === column.id
                                                        ? order
                                                        : 'asc'
                                                }
                                                onClick={createSortHandler(
                                                    column.id
                                                )}
                                            >
                                                {column.label}
                                            </TableSortLabel>
                                        )}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {stableSort(
                                rows,
                                getComparator(order, orderBy)
                            ).map((row, rowIndex) => {
                                return (
                                    <TableRow
                                        onMouseEnter={() =>
                                            setShowAction(rowIndex)
                                        }
                                        onMouseLeave={() => setShowAction(-1)}
                                        selected={selectedRow === rowIndex}
                                        role="checkbox"
                                        tabIndex={-1}
                                        key={rowIndex}
                                        classes={{
                                            root: row.disable
                                                ? classes.disabledRootBodyRow
                                                : classes.rootBodyRow,
                                            selected: classes.selectedBodyRow,
                                        }}
                                        onClick={() =>
                                            onRowClick &&
                                            !row.disable &&
                                            onRowClick(row, rowIndex)
                                        }
                                    >
                                        {columns.map((column) => {
                                            const value = row[column.id]
                                            return (
                                                <TableCell
                                                    className={`${
                                                        classes.bodyRowText
                                                    } ${
                                                        column.ellipsisWidth
                                                            ? classes.ellipsis
                                                            : ''
                                                    }`}
                                                    style={{
                                                        maxWidth:
                                                            column.ellipsisWidth ||
                                                            column.maxWidth ||
                                                            'auto',
                                                        width:
                                                            column.ellipsisWidth ||
                                                            column.maxWidth ||
                                                            'auto',
                                                        padding: column.noPadding
                                                            ? 0
                                                            : 'auto',
                                                    }}
                                                    key={column.id}
                                                    align={column.align}
                                                >
                                                    {renderTableCell(
                                                        column,
                                                        rowIndex,
                                                        value,
                                                        row,
                                                        dispatch
                                                    )}
                                                </TableCell>
                                            )
                                        })}
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                )}
            </TableContainer>
        </div>
    )
}

export default CustomTable
