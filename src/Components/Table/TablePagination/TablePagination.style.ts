import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        selectRoot: {
            backgroundColor: '#F4F5FC',
            height: '27px',
            fontSize: '13px',
            color: '#8C8C97',
            padding: '0 20px 0 15px !important',
            fontFamily: theme.fonts.primaryText,
            border: '1px solid white',
            borderRadius: 200,
            textAlign: 'center',
            display: 'flex',
            alignItems: 'center',
            width: 121,
            '&:hover': {
                border: '1px solid #D9DAE4',
            },
            '&:focus': {
                border: '1px solid #D9DAE4',
                color: '#40414E',
                borderRadius: 200,
            },
        },
        selectIcon: {
            marginRight: 10,
        },
        paginationSelect: {
            marginRight: 12,
        },
        paginationRoot: {
            border: 0,
            padding: 0,
        },
        options: {
            padding: 0,
            margin: 0,
            '& li': {
                fontSize: '13px',
                borderRadius: 3,
                padding: '6px 0 0 15px',
                backgroundColor: theme.colors.selectBackground,
                minHeight: 39,
                lineHeight: '39px',
            },
            '& li:hover': {
                backgroundColor: theme.colors.secondary,
                color: theme.colors.selectTextSelected,
                cursor: 'pointer',
            },
            '& li[aria-selected="true"]': {
                backgroundColor: theme.colors.primary,
                color: theme.colors.selectTextSelected,
            },
            '& li[aria-selected="true"]:hover': {
                backgroundColor: theme.colors.primary,
                color: theme.colors.selectTextSelected,
            },
        },
    })
)

export default useStyles
