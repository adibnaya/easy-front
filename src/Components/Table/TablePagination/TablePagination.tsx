import TablePagination from '@material-ui/core/TablePagination'
import React from 'react'
import CustomTablePaginationActions from '../TablePaginationActions/TablePaginationActions'
import useStyles from './TablePagination.style'

interface Props {
    handleBackPress: () => void
    handleFirstPress: () => void
    handleLastPress: () => void
    handleNextPress: () => void
    lastPage: number
    currentPage: number
    handleChangeRowsPerPage: (val: number) => void
}
const CustomTablePagination = ({
    handleBackPress,
    handleFirstPress,
    handleLastPress,
    handleNextPress,
    lastPage,
    currentPage,
    handleChangeRowsPerPage,
}: Props) => {
    const [page, setPage] = React.useState(0)
    const [rowsPerPage, setRowsPerPage] = React.useState(100)

    const componentHandleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        handleChangeRowsPerPage(parseInt(event.target.value, 10))
        setRowsPerPage(parseInt(event.target.value, 10))
        setPage(0)
    }
    const classes = useStyles()
    return (
        <TablePagination
            labelRowsPerPage={false}
            rowsPerPageOptions={[
                { label: '100 Rows / Page', value: 100 },
                { label: '150 Rows / Page', value: 150 },
                { label: '300 Rows / Page', value: 300 },
            ]}
            classes={{
                root: classes.paginationRoot,
                selectRoot: classes.paginationSelect,
            }}
            colSpan={3}
            count={1000}
            rowsPerPage={rowsPerPage}
            page={page}
            labelDisplayedRows={() => ''}
            SelectProps={{
                classes: {
                    root: classes.selectRoot,
                    icon: classes.selectIcon,
                },
                MenuProps: {
                    classes: {
                        list: classes.options,
                    },
                },
            }}
            onChangePage={() => {}}
            onChangeRowsPerPage={componentHandleChangeRowsPerPage}
            ActionsComponent={() => (
                <CustomTablePaginationActions
                    currentPage={currentPage}
                    lastPage={lastPage}
                    onPressBack={handleBackPress}
                    onPressFirst={handleFirstPress}
                    onPressLast={handleLastPress}
                    onPressNext={handleNextPress}
                />
            )}
        />
    )
}

export default CustomTablePagination
