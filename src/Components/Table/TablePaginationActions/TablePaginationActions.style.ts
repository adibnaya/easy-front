import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexShrink: 0,
            marginLeft: theme.spacing(2.5),
        },
        paginationButton: {
            padding: 0,
        },
        pageNumber: {
            backgroundColor: theme.colors.primary,
            width: 23,
            height: 23,
            lineHeight: '23px',
            borderRadius: 20,
            color: 'white',
            display: 'inline-block',
            textAlign: 'center',
        },
    })
)

export default useStyles
