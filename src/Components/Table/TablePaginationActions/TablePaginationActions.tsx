import React from 'react'
import IconButton from '@material-ui/core/IconButton'
import FirstPageIcon from '@material-ui/icons/FirstPage'
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'
import LastPageIcon from '@material-ui/icons/LastPage'
import useStyles from './TablePaginationActions.style'
import TextBox from '../../TextBox/TextBox'

interface TablePaginationActionsProps {
    lastPage: number
    currentPage: number
    onPressNext: (event: React.MouseEvent<HTMLButtonElement>) => void
    onPressBack: (event: React.MouseEvent<HTMLButtonElement>) => void
    onPressFirst: (event: React.MouseEvent<HTMLButtonElement>) => void
    onPressLast: (event: React.MouseEvent<HTMLButtonElement>) => void
}

const CustomTablePaginationActions = (props: TablePaginationActionsProps) => {
    const classes = useStyles()
    const {
        lastPage,
        currentPage,
        onPressNext,
        onPressBack,
        onPressFirst,
        onPressLast,
    } = props
    return (
        <div className={classes.root}>
            <IconButton
                className={classes.paginationButton}
                onClick={onPressFirst}
                disabled={currentPage === 1}
                aria-label="first page"
            >
                <FirstPageIcon style={{ height: 32, width: 20 }} />
            </IconButton>
            <IconButton
                className={classes.paginationButton}
                onClick={onPressBack}
                disabled={currentPage === 1}
                aria-label="previous page"
            >
                <KeyboardArrowLeft style={{ height: 32, width: 20 }} />
            </IconButton>
            <TextBox
                variant="body2"
                display="inline"
                className={classes.pageNumber}
            >
                {currentPage}
            </TextBox>
            <IconButton
                className={classes.paginationButton}
                onClick={onPressNext}
                disabled={currentPage === lastPage}
                aria-label="next page"
            >
                <KeyboardArrowRight style={{ height: 32, width: 20 }} />
            </IconButton>
            <IconButton
                className={classes.paginationButton}
                onClick={onPressLast}
                disabled={currentPage === lastPage}
                aria-label="last page"
            >
                <LastPageIcon style={{ height: 32, width: 20 }} />
            </IconButton>
        </div>
    )
}
export default CustomTablePaginationActions
