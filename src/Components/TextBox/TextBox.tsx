import { withStyles, Theme as AugmentedTheme } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const TextBox = withStyles((theme: AugmentedTheme) => ({
    colorInherit: {
        color: theme.colors.selectText,
    },
    root: {
        fontFamily: theme.fonts.primaryText,
    },
    body1: {
        fontSize: '16px',
    },
    body2: {
        fontSize: '13px',
    },
    h1: {
        fontSize: '36px',
        fontWeight: 'bold',
    },
    h2: {
        fontSize: '31px',
        fontWeight: 'bold',
    },
    h3: {
        fontSize: '26px',
        fontWeight: 'bold',
    },
    h4: {
        fontSize: '16px',
        fontWeight: 'bold',
    },
    subtitle1: {
        fontSize: '14px',
    },
    subtitle2: {
        fontSize: '15px',
        color: '#8C8C97',
    },
    colorSecondary: {
        color: theme.colors.white,
    },
}))(Typography)

export default TextBox
