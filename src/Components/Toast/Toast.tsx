import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert'
import { makeStyles, Theme } from '@material-ui/core/styles'

function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />
}

interface Props extends AlertProps {
    text: string
    isOpen: boolean
    toggleToast: () => void
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}))

const Toast = ({ severity, text, isOpen, toggleToast, ...props }: Props) => {
    const classes = useStyles()

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
            return
        }
        toggleToast()
    }

    return (
        <div className={classes.root}>
            <Snackbar
                open={isOpen}
                autoHideDuration={3000}
                onClose={handleClose}
            >
                <Alert onClose={handleClose} severity={severity}>
                    {text}
                </Alert>
            </Snackbar>
        </div>
    )
}

export default Toast
