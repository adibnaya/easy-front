import React, { useState } from 'react'
import { Popper, PopperProps } from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import useStyles from './TooltipButton.style'
import AppButton from '../Button/Button'

interface Props {
    textValue: string
    popperPlacement?: PopperProps['placement']
    renderPopper: (closeFunc: () => void) => React.ReactNode
    underline?: boolean
    disablePortal?: boolean
    disabled?: boolean
    dataOutline?: boolean
    buttonStyle?: React.CSSProperties
}

const TooltipButton = ({
    textValue,
    popperPlacement = 'right',
    renderPopper,
    underline,
    disablePortal,
    disabled,
    dataOutline = false,
    buttonStyle,
}: Props) => {
    const anchorRef = React.useRef(null)
    const [arrowRef, setArrowRef] = React.useState(null)
    const setNewArrowRef = (ref) => {
        setArrowRef(ref)
    }

    const [open, togglePopper] = useState(false)

    const id = open ? 'input-popper' : undefined

    const classes = useStyles()
    return (
        <div>
            <AppButton
                className={`${classes.text} ${
                    underline ? classes.underline : ''
                }`}
                ref={anchorRef}
                disabled={disabled}
                data-outline={dataOutline}
                onClick={() => togglePopper(!open)}
                style={buttonStyle}
            >
                {textValue}
            </AppButton>
            <Popper
                id={id}
                open={open}
                anchorEl={anchorRef.current}
                placement={popperPlacement}
                className={classes.popper}
                disablePortal={disablePortal ? disablePortal : false}
                modifiers={{
                    flip: {
                        enabled: true,
                    },
                    arrow: {
                        enabled: true,
                        element: arrowRef,
                    },
                    preventOverflow: {
                        enabled: false,
                        boundariesElement: 'scrollParent',
                    },
                }}
            >
                <span className={classes.arrow} ref={setNewArrowRef} />
                <Paper className={classes.paper}>
                    {renderPopper(() => togglePopper(!open))}
                </Paper>
            </Popper>
        </div>
    )
}

export default TooltipButton
