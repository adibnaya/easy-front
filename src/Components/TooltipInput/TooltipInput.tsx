import React, { useState, useRef } from 'react'
import { TooltipProps } from '@material-ui/core/Tooltip'
import { Popper, PopperProps } from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import useStyles from './TooltipInput.style'

interface Props {
    tooltipText?: TooltipProps['title']
    handleInputChange: (e) => void
    inputValue: string
    inputPlaceHolder: string
    popperPlacement?: PopperProps['placement']
    inputMaxLength?: number
    open: boolean
    renderPopper: () => React.ReactNode
    validateFunc?: (value) => boolean
}

const TooltipInput = ({
    handleInputChange,
    inputValue,
    inputPlaceHolder,
    popperPlacement = 'right',
    inputMaxLength,
    open,
    renderPopper,
    validateFunc,
}: Props) => {
    const anchorRef = useRef(null)
    const [arrowRef, setArrowRef] = useState(null)
    const [isValid, setIsValid] = useState(true)

    const setNewArrowRef = (ref) => {
        setArrowRef(ref)
    }

    const id = open ? 'input-popper' : undefined

    const onBlurValidation = () => {
        if (validateFunc) {
            setIsValid(validateFunc(inputValue))
        }
    }

    const onInputChange = (val) => {
        setIsValid(true)
        handleInputChange(val)
    }

    const classes = useStyles()
    return (
        <div>
            <input
                ref={anchorRef}
                className={isValid ? classes.input : classes.inputInvalid}
                value={inputValue}
                onChange={onInputChange}
                placeholder={inputPlaceHolder}
                maxLength={inputMaxLength || -1}
                onBlur={onBlurValidation}
            />
            <Popper
                id={id}
                open={open || !isValid}
                anchorEl={anchorRef.current}
                placement={popperPlacement}
                className={classes.popper}
                modifiers={{
                    flip: {
                        enabled: true,
                    },
                    arrow: {
                        enabled: true,
                        element: arrowRef,
                    },
                    preventOverflow: {
                        enabled: true,
                        boundariesElement: 'scrollParent',
                    },
                }}
            >
                <span className={classes.arrow} ref={setNewArrowRef} />
                <Paper className={classes.paper}>{renderPopper()}</Paper>
            </Popper>
        </div>
    )
}

export default TooltipInput
