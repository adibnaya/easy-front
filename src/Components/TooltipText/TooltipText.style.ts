import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        text: {
            fontFamily: theme.fonts.primaryText,
            fontSize: 13,
            color: '#67B1FA', // '#3E7BB7'
            cursor: 'pointer',
            lineHeight: '29px',
        },
        root: {
            flexGrow: 1,
        },
        scrollContainer: {
            height: 400,
            marginBottom: theme.spacing(3),
        },
        scroll: {
            position: 'relative',
            backgroundColor: theme.palette.background.paper,
        },
        legend: {
            marginTop: theme.spacing(2),
            maxWidth: 300,
        },
        paper: {
            maxWidth: 400,
            backgroundColor: theme.colors.white,
            boxShadow: '4px 7px 29px #29292E4A',
            borderRadius: 3,
            border: 0,
            // padding: 20,
        },
        underline: {
            textUnderlinePosition: 'under',
            textDecoration: 'underline',
        },
        select: {
            width: 200,
        },
        popper: {
            borderRadius: 3,
            zIndex: 99999999,
            borderTop: '2px solid #67B1FA',
            backgroundColor: 'transparent',
            '&[x-placement*="bottom"]': {
                borderTop: 'unset !important',
                borderBottom: '3px solid #67B1FA',
            },
            '&[x-placement*="bottom"] $arrow': {
                top: 0,
                left: 0,
                marginTop: '-0.9em',
                width: '3em',
                height: '1em',
                '&::before': {
                    borderWidth: '0 1em 1em 1em',
                    borderColor: `transparent transparent ${theme.palette.background.paper} transparent`,
                },
            },
            '&[x-placement*="top"] $arrow': {
                bottom: 0,
                left: 0,
                marginBottom: '-0.9em',
                width: '3em',
                height: '1em',
                '&::before': {
                    borderWidth: '1em 1em 0 1em',
                    borderColor: `${theme.palette.background.paper} transparent transparent transparent`,
                },
            },
            '&[x-placement*="right"]': {
                left: '20px !important',
                top: '5px !important',
            },
            '&[x-placement*="right"] $arrow': {
                left: 0,
                marginLeft: '-0.9em',
                height: '3em',
                width: '1em',

                '&::before': {
                    borderWidth: '1em 1em 1em 0',
                    borderColor: `transparent ${theme.palette.background.paper} transparent transparent`,
                },
            },
            '&[x-placement*="left"] $arrow': {
                right: 0,
                marginRight: '-0.9em',
                height: '3em',
                width: '1em',
                '&::before': {
                    borderWidth: '1em 0 1em 1em',
                    borderColor: `transparent transparent transparent ${theme.palette.background.paper}`,
                },
            },
        },
        arrow: {
            position: 'absolute',
            fontSize: 7,
            width: '3em',
            height: '3em',
            '&::before': {
                content: '""',
                margin: 'auto',
                display: 'block',
                width: 0,
                height: 0,
                borderStyle: 'solid',
            },
        },
    })
)

export default useStyles
