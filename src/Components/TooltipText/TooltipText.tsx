import React, { useState } from 'react'
import { Popper, PopperProps } from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import useStyles from './TooltipText.style'
import TextBox from '../TextBox/TextBox'

interface Props {
    textValue: string
    popperPlacement?: PopperProps['placement']
    renderPopper: (closeFunc: () => void) => React.ReactNode
    underline?: boolean
    disablePortal?: boolean
}

const TooltipText = ({
    textValue,
    popperPlacement = 'right',
    renderPopper,
    underline,
    disablePortal,
}: Props) => {
    const anchorRef = React.useRef(null)
    const [arrowRef, setArrowRef] = React.useState(null)
    const setNewArrowRef = (ref) => {
        setArrowRef(ref)
    }

    const [open, togglePopper] = useState(false)

    const id = open ? 'input-popper' : undefined

    const classes = useStyles()
    return (
        <div>
            <TextBox
                className={`${classes.text} ${
                    underline ? classes.underline : ''
                }`}
                ref={anchorRef}
                onClick={() => togglePopper(!open)}
            >
                {textValue}
            </TextBox>
            <Popper
                id={id}
                open={open}
                anchorEl={anchorRef.current}
                placement={popperPlacement}
                className={classes.popper}
                disablePortal={disablePortal ? disablePortal : false}
                modifiers={{
                    flip: {
                        enabled: true,
                    },
                    arrow: {
                        enabled: true,
                        element: arrowRef,
                    },
                    preventOverflow: {
                        enabled: false,
                        boundariesElement: 'scrollParent',
                    },
                }}
            >
                <span className={classes.arrow} ref={setNewArrowRef} />
                <Paper className={classes.paper}>
                    {renderPopper(() => togglePopper(!open))}
                </Paper>
            </Popper>
        </div>
    )
}

export default TooltipText
