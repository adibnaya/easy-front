import {Route, Switch} from 'react-router-dom'
import React from 'react'
import Dashboard from './Dashboard/Dashboard'
import {Page} from '../DrawerAndBar/DrawerReducer'
import GetAllEndpoints from './GetAllEndpoints/GetAllEndpoints'
import LeafInterfaceDetails from "./LeafInterfaceDetails/LeafInterfaceDetails";
import DeployStandardEndpoint from './DeployStandardEndpoint/DeployStandardEndpoint'
import SpineLeafInterfaceStats from "./SpineLeafInterfaceStats/SpineLeafInterfaceStats";

const renderPageBySlug = (
    slug: string,
    pageId: string,
    isFavorite: boolean
) => {
    switch (slug) {
        case 'aci-deploy-standard-endpoint':
            return (
                <DeployStandardEndpoint isFavorite={isFavorite} pageId={pageId}/>
            )
        case 'aci-get-standard-endpoints':
            return <GetAllEndpoints isFavorite={isFavorite} pageId={pageId}/>
        case 'aci-leaf-interface-details':
            return <LeafInterfaceDetails isFavorite={isFavorite} pageId={pageId}/>
        case 'aci-spine-leaf-interface-stats':
            return <SpineLeafInterfaceStats isFavorite={isFavorite} pageId={pageId}/>
        default:
            return <Dashboard/>
    }
}

const renderRoutesPerPage = (
    page: Page,
    parentSlug = '/solutions/:solutionId'
) => {
    if (page.sub_pages.length === 0) {
        return (
            <Route
                key={`${parentSlug}/${page.slug}`}
                path={`${parentSlug}/${page.slug}`}
            >
                {renderPageBySlug(page.slug, page.id, page.is_favorite)}
            </Route>
        )
    }
    return page.sub_pages.map((p) =>
        renderRoutesPerPage(p, `${parentSlug}/${page.slug}`)
    )
}

interface Props {
    solutionPages: Page[]
}

const Aci = ({solutionPages}: Props) => {
    return (
        <Switch>
            {solutionPages &&
            solutionPages.map((page) => {
                return renderRoutesPerPage(page)
            })}
            <Route path="/solutions/:solutionId">
                <Dashboard/>
            </Route>
        </Switch>
    )
}

export default Aci
