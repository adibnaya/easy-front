// POST /solutions/{{solution_id}}/aci-standard-endpoints
export type DeployCreateStandardEndpoints = {
    standardEndpoints: StandardEndpointForDeploy[]
}
// Deploy model
export type AciStandardEndpointsBody = {
    endpoints: StandardEndpointForDeploy[]
}
export type DeployCreateStandardEndpointsResponse = {
    standardEndpointsResponses: StandardEndpointForDeployResponse[]
}
// Deploy response model
export type AciStandardEndpointsResponse = {
    status: boolean
    data: string
}
export type ParsedResponseData = {
    totalCount: string
    imdata: imdata[]
}
export type imdata = {
    error: error
}
export type error = {
    attributes: errorAtrributes
}
export type errorAtrributes = {
    code: string
    text: string
}

export type StandardEndpointForDeploy = {
    leaf: LeafForDeploy
    policy_name: string // [policy_name]
    interface_name: string // [interface_name]
    server_name: string // [server_name]
    deployErrorMsg?: string
}
export type LeafForDeploy = {
    id: string // [leaf][id]
    name: string // [leaf][name]
    pod_id: string // [leaf][pod_id]
}

export type StandardEndpointForDeployResponse = {
    status: boolean
    data: string
}

// POST /solutions/{{solution_id}}/aci-aaeps
export type CreateAaep = {
    name: string // aaep_name_{{counter}}
}
export type CreateAeaepResponse = {
    status?: boolean // success = status: true
    message?: string // failure = message: "{{error message}}"
}

// POST /solutions/{{solution_id}}/aci-policies
export type CreatePolicy = {
    name: string // policy_name_{{counter}}
    aaep_name: string // {{aaep_name}}
    connected_to_switch: boolean // true/flase/0/1
}
