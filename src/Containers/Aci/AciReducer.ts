import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {MetaAndLinks} from '../DrawerAndBar/DrawerReducer'
import {AciLeaf, InterfaceData} from "./Models/AciModels";
import {LeafInterface} from "../../Components/AddStandardEndPiontModal/AddStandardEndPointModalModel";

interface AciState {
    solutionLeafs: AciLeaf[]
    leafInterfaces: LeafInterface[]
    leaf: AciLeaf
    metaAndLinks: MetaAndLinks | undefined
    fetchingData: boolean
}

const initialState: AciState = {
    solutionLeafs: [],
    leafInterfaces: [],
    metaAndLinks: undefined,
    leaf: {id: '', name: '', pod_id: ''},
    fetchingData: false,
}

const aciSlice = createSlice({
    name: 'aciSlice',
    initialState,
    reducers: {
        setSolutionLeafs: (
            state,
            action: PayloadAction<AciLeaf[]>
        ) => {
            state.solutionLeafs = action.payload
        },
        setLeafInterfaces: (state, action: PayloadAction<InterfaceData[]>) => {
            state.leafInterfaces = action.payload
        },
        setMetaAndLinks: (state, action: PayloadAction<MetaAndLinks>) => {
            state.metaAndLinks = action.payload
        },
        setLeaf: (state, action: PayloadAction<AciLeaf>) => {
            state.leaf = action.payload
        },
        setFetchingData: (state, action: PayloadAction<boolean>) => {
            state.fetchingData = action.payload
        },
        resetEndpoints: () => {
            return initialState
        },
    },
})

export const {
    setSolutionLeafs,
    setLeafInterfaces,
    setMetaAndLinks,
    resetEndpoints,
    setLeaf,
    setFetchingData,
} = aciSlice.actions


export default aciSlice.reducer
