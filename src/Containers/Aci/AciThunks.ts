import {AppThunk} from "../../Redux/Store";
import api from "../../Api/Api";
import {AxiosResponse} from "axios";
import {ApiResponse} from "./Models/AciModels";
import {resetLeafInterfaces} from "./DeployStandardEndpoint/DeployStandardEndpointReducer";
import {setLeafInterfaces, setSolutionLeafs} from "./AciReducer";

export const getSolutionLeafs = (): AppThunk => async (
    dispatch, getState
) => {
    const solutionLeafs = getState().aci.solutionLeafs;

    if (solutionLeafs.length > 0)
        return;

    let response
    const { currentSolution } = getState().drawer

    try {
        response = await api.get<any, AxiosResponse<ApiResponse>>(
            `/solutions/${currentSolution?.id}/aci-leafs`
        )
    } catch (e) {
        return
    }

    const apiResponse = response.data
    const leafs = apiResponse.data

    dispatch(setSolutionLeafs(leafs))
}



export const getLeafInterfaces = (params: {
    solutionId: string
    leafId: string
    podId: string
}): AppThunk => async (dispatch) => {
    let response
    try {
        response = await api.get(
            `/solutions/${params.solutionId}/aci-leafs/${params.leafId}/pods/${params.podId}`
        )
    } catch (e) {
        return
    }
    dispatch(setLeafInterfaces(response.data.data))
}


export const disableLeafInterface = (interfaceName: string): AppThunk => async (
    dispatch,
    getState
) => {
    const store = getState()
    const { currentSolution } = store.drawer
    const { wizardLeaf } = store.dse
    try {
        dispatch(resetLeafInterfaces())
        await api.get(
            `/solutions/${currentSolution?.id}/aci-leafs/${wizardLeaf?.id}/pods/${wizardLeaf?.pod_id}/interfaces/${interfaceName}/disable`
        )
    } catch (e) {
        dispatch(
            getLeafInterfaces({
                solutionId: currentSolution?.id || '',
                leafId: wizardLeaf?.id || '',
                podId: wizardLeaf?.pod_id || '',
            })
        )
        return
    }
    dispatch(
        getLeafInterfaces({
            solutionId: currentSolution?.id || '',
            leafId: wizardLeaf?.id || '',
            podId: wizardLeaf?.pod_id || '',
        })
    )
}


export const exportEndpointsData = (solutionId: string): AppThunk => {
    return async () => {
        let response
        try {
            response = await api.get(
                `/solutions/${solutionId}/aci-standard-endpoints?export-csv=true`
            )
            return response.data
        } catch (e) {
            return ''
        }
    }
}