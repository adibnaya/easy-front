import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        homeContentContainer: {
            display: 'flex',
            flexDirection: 'row',
            padding: '18px 40px 40px 40px',
            justifyContent: 'space-between',
            maxWidth: 1432,
            margin: '0 auto',
        },
        homeTopic: {
            flex: 12,
        },
        leftContent: {
            flex: 10,
            display: 'flex',
            minWidth: 1097,
            flexDirection: 'column',
            marginRight: 40,
        },
        dataBoxesWrapper: {
            flex: 12,
            minWidth: 1097,
        },
        dataBoxesContainer: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        leftContentRow: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
        },
        rightContent: {
            flex: 2,
            display: 'flex',
            flexDirection: 'row',
            marginTop: 75,
        },
        boxMargin: {
            margin: '40px 40px 0 0',
        },
        lastBoxMargin: {
            margin: '40px 0 0 0',
        },
    })
)

export default useStyles
