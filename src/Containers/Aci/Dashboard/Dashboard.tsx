import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { push } from 'connected-react-router'
import HomeTopic from '../../../Components/HomeTopic/HomeTopic'
import SmallDataBox from '../../../Components/SmallDataBox/SmallDataBox'
import BigDataBox from '../../../Components/BigDataBox/BigDataBox'
import FavoriteList from '../../../Components/FavoriteList/FavoriteList'
import useStyles from './Dashboard.style'
import { RootState } from '../../../Redux/Store'

import { DashboardData, getSolutionsDashboardData } from './DashboardReducer'
import {
    getFavoritePages,
    deleteFavoritePage,
} from '../../DrawerAndBar/DrawerReducer'

const renderDashboardData = (
    data: DashboardData[],
    classes,
    solutionId,
    dispatch
) => {
    const dataBoxData: any[] = []
    return data.map((d, i) => {
        switch (d.type) {
            case 'inactive_undiscovered':
                return (
                    <SmallDataBox
                        key={i}
                        onClick={() =>
                            dispatch(push(`/solutions/${solutionId}/${d.link}`))
                        }
                        className={
                            (i + 1) % 3 === 0
                                ? classes.lastBoxMargin
                                : classes.boxMargin
                        }
                        data={d.data}
                        title={d.title}
                        icon={d.icon}
                    />
                )
            case 'faults_pie_chart':
                Object.entries(d.data).forEach(([key, value]) =>
                    dataBoxData.push({
                        icon: '',
                        title: key,
                        value,
                    })
                )

                return (
                    <BigDataBox
                        key={i}
                        onClick={() =>
                            dispatch(push(`/solutions/${solutionId}/${d.link}`))
                        }
                        className={
                            (i + 1) % 3 === 0
                                ? classes.lastBoxMargin
                                : classes.boxMargin
                        }
                        data={dataBoxData}
                        title={d.title}
                        type="faults_pie_chart"
                    />
                )

            case 'general_pie_chart':
                return (
                    <BigDataBox
                        key={i}
                        onClick={() =>
                            dispatch(push(`/solutions/${solutionId}/${d.link}`))
                        }
                        className={
                            (i + 1) % 3 === 0
                                ? classes.lastBoxMargin
                                : classes.boxMargin
                        }
                        data={d.data}
                        title={d.title}
                        type="general_pie_chart"
                    />
                )
            default:
                return <div key={i} />
        }
    })
}

const Dashboard = () => {
    const classes = useStyles()
    const dispatch = useDispatch()

    const { drawer, aciDashboard } = useSelector((state: RootState) => state)

    const {
        userName,
        currentSolution,
        favoritePages,
        favoritesMetaAndLinks,
    } = drawer
    const { dashboardData } = aciDashboard

    useEffect(() => {
        if (currentSolution) {
            dispatch(getSolutionsDashboardData(currentSolution.id))
        }
    }, [dispatch, currentSolution])

    return (
        <div className={classes.homeContentContainer}>
            <div className={classes.leftContent}>
                <HomeTopic
                    name={userName || ''}
                    className={classes.homeTopic}
                />
                <div className={classes.dataBoxesWrapper}>
                    <div className={classes.dataBoxesContainer}>
                        {dashboardData &&
                            renderDashboardData(
                                dashboardData,
                                classes,
                                currentSolution.id,
                                dispatch
                            )}
                    </div>
                </div>
            </div>

            <div className={classes.rightContent}>
                {favoritesMetaAndLinks && favoritePages && (
                    <FavoriteList
                        hasMore={favoritesMetaAndLinks.links.next !== null}
                        fetchMoreData={() =>
                            dispatch(
                                getFavoritePages({
                                    id: currentSolution.id,
                                    page:
                                        favoritesMetaAndLinks.meta
                                            .current_page + 1,
                                })
                            )
                        }
                        onRemovePage={(id) =>
                            dispatch(
                                deleteFavoritePage({
                                    pageId: id,
                                    solutionId: currentSolution.id,
                                })
                            )
                        }
                        onClickPage={(pageUrl) =>
                            dispatch(
                                push(
                                    `/solutions/${currentSolution.id}/${pageUrl}`
                                )
                            )
                        }
                        pages={favoritePages.map((fp) => ({
                            id: fp.id,
                            title: fp.name,
                            url: fp.slug,
                        }))}
                    />
                )}
            </div>
        </div>
    )
}

export default Dashboard
