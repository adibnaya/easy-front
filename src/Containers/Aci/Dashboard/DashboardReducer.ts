import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import api from '../../../Api/Api'
import { AppThunk } from '../../../Redux/Store'

export type InactiveUndiscovered = {
    total: number
    inactive: number
    undiscovered: number
}

export type GeneralPieChartData = {
    icon: string
    title: string
    value: number
}

export type FaultsPieChartData = {
    high: number
    medium: number
    low: number
}

export type DashboardData = {
    type: string
    icon: string | null
    link: string
    title: string
    data: FaultsPieChartData | GeneralPieChartData[] | InactiveUndiscovered
}

type DashboardSliceState = {
    dashboardData: undefined | DashboardData[]
}

const initialState: DashboardSliceState = {
    dashboardData: undefined,
}

const dashboard = createSlice({
    name: 'dashboard',
    initialState,
    reducers: {
        setDashboardData: (state, action: PayloadAction<DashboardData[]>) => {
            state.dashboardData = action.payload
        },
    },
})

export const { setDashboardData } = dashboard.actions
export const getSolutionsDashboardData = (id: string): AppThunk => async (
    dispatch
) => {
    let response
    try {
        response = await api.get(`/solutions/${id}/dashboard`)
    } catch (e) {
        return
    }
    dispatch(setDashboardData(response.data.data))
}

export default dashboard.reducer
