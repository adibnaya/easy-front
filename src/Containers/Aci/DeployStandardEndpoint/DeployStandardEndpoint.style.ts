import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        table: {
            backgroundColor: theme.colors.white,
            padding: '0 20px',
            marginTop: 30,
        },
        titleContainer: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
        },
        favoriteIconContainer: {
            marginLeft: 15,
            width: 15,
            height: 15,
            borderRadius: 20,
            backgroundColor: '#FFCF2C',
            marginRight: 10,
            padding: 3,
            '&:hover': {
                cursor: 'pointer',
                backgroundColor: '#A3A5BD',
            },
        },
        isFavorite: {
            backgroundColor: '#FFCF2C',
            '&:hover': {
                cursor: 'pointer',
                backgroundColor: '#A3A5BD',
            },
        },
        isNotFavorite: {
            backgroundColor: '#A3A5BD',
            '&:hover': {
                cursor: 'pointer',
                backgroundColor: '#FFCF2C',
            },
        },
        someLogo: {
            marginLeft: 15,
            width: 36,
            height: 10,
            padding: 7,
            borderRadius: 30,
            backgroundColor: '#67B1FA',
        },
        container: {
            backgroundColor: '#F7F9FC',
            padding: '40px 40px 0 40px',
            height: 'calc(100% - 108px)',
        },
        actionButtonsContainer: {
            marginRight: 15,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
        },
        actionButton: {
            cursor: 'pointer',
        },
        errorButtonContainer: {
            marginRight: 15,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
        },
        noEndpoints: {
            backgroundColor: 'white',
            height: 400,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
        },
        noEndpointsTitle: {
            color: '#8C8C97',
            marginBottom: 15,
        },
        bottomContainer: {
            marginTop: '30px',
            width: '100%',
            display: 'flex',
            justifyContent: 'flex-end',
        },
    })
)
export default useStyles
