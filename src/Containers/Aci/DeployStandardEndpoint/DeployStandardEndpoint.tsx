import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import AddStandardEndPointModal from '../../../Components/AddStandardEndPiontModal/AddStandardEndPiontModal'
import CustomTable, { Column } from '../../../Components/Table/Table'
import TextBox from '../../../Components/TextBox/TextBox'
import { RootState } from '../../../Redux/Store'
import useStyles from './DeployStandardEndpoint.style'
import ActionButtons from '../../../Components/DeployStandardEndpoint/TableButtons/ActionButtons/ActionButton'
import ErrorButton from '../../../Components/DeployStandardEndpoint/TableButtons/ErrorButton/ErrorButton'
import ASEButton from '../../../Components/DeployStandardEndpoint/ASEButton/ASEButton'
import { ReactComponent as SomeLogoIcon } from '../../../Assets/some_logo.svg'
import { ReactComponent as FavoriteIcon } from '../../../Assets/favorite_white.svg'
import {
    getSolutionAaeps,
    getSolutionLeafs,
    setIsModalOpenState,
    getSolutionPolicies,
    setAttachableAccessEntityProfile,
    setWizardEndpointName,
    setInterfacePolicyGroupName,
    setLeaf,
    setWizardPolicyName,
    getLeafInterfaces,
    setWizardInterfaceName,
    addNewStandardEndpointToDeployList,
    setPolicySelectedTab,
    setConnectedToSwitch,
    createAaep,
    createPolicy,
    deployStandardEndpoints,
    resetEndpointsToDeploy,
    setIsEpAndLeafValid,
    setIsPolicyValid,
    setIsInterfaceValid,
    editStandardEndpointToDeployList,
    removeStandardEndpointToDeployList,
    InitializeModalWithEndpointValues,
    resetWizard,
    setEditingIndex,
} from './DeployStandardEndpointReducer'
import {
    AciLeaf,
    CreateAaepPostReq,
    CreatePolicyPostReq,
} from '../Models/AciModels'
import { StandardEndpointForDeploy } from '../AciApi'
import { Box } from '@material-ui/core'
import AppButton from '../../../Components/Button/Button'
import {
    addFavoritePage,
    deleteFavoritePage,
} from '../../DrawerAndBar/DrawerReducer'
import Toast from '../../../Components/Toast/Toast'
import { toggleToast } from '../../Login/LoginReducer'
import TooltipButton from '../../../Components/TooltipButton/TooltipButton'

const columns: Column[] = [
    { id: 'deviceName', label: 'DEVICE NAME', minWidth: 240 },
    { id: 'leafId', label: 'LEAF ID', minWidth: 240 },
    { id: 'interfaceId', label: 'INTERFACE ID', minWidth: 240 },
    { id: 'policy', label: 'POLICY', minWidth: 240 },
    {
        id: 'actionButtons',
        label: '',
        showOnHover: true,
        minWidth: 50,
        maxWidth: 50,
        noPadding: true,
        renderCell: (value, index: number, row, dispatch) => {
            return (
                <ActionButtons
                    onDelete={() => {
                        dispatch(removeStandardEndpointToDeployList(index))
                    }}
                    onEdit={() => {
                        dispatch(InitializeModalWithEndpointValues(index))
                        dispatch(setEditingIndex(index))
                    }}
                />
            )
        },
    },
    {
        id: 'errorButtons',
        label: '',
        // showOnHover: true,
        minWidth: 70,
        maxWidth: 50,
        noPadding: true,
        renderCell: (value, index, row) => {
            if (value) {
                return <ErrorButton errorText={value} />
            }
            return ''
        },
    },
]

export type StandardEndpointToDeployRow = {
    deviceName: string
    leafId: string
    interfaceId: string
    policy: string
    errorButtons?: string
}

interface Props {
    pageId: string
    isFavorite: boolean
}

const DeployStandardEndpoint = ({ pageId, isFavorite }: Props) => {
    const classes = useStyles()
    const { t } = useTranslation()
    const dispatch = useDispatch()

    const { dse, drawer, user } = useSelector((state: RootState) => state)
    const { isToastOpen } = user
    const { currentSolution } = drawer
    const {
        solutionLeafs,
        modalOpen,
        isEpAndLeafValid,
        isInterfaceValid,
        isPolicyValid,
        solutionPolicies,
        solutionAaeps,
        wizardEndpointName,
        wizardLeaf,
        wizardInterfaceName,
        interfacePolicyGroupName,
        wizardPolicyName,
        policySelectedTab,
        wizardAaep,
        leafInterfaces,
        standardEndpointsToDeploy,
        connectedToSwitch,
        createAaepResponse,
        deployStatus,
        deployMsg,
        editingIndex,
    } = dse

    useEffect(() => {
        if (currentSolution) {
            dispatch(getSolutionLeafs(currentSolution.id))
            dispatch(getSolutionPolicies(currentSolution.id))
            dispatch(getSolutionAaeps(currentSolution.id))
            // remove this if you want to persist the modal toggler on refresh
            dispatch(setIsModalOpenState(false))
        }
    }, [dispatch, currentSolution])

    const [isPageFavorite, setIsPageFavorite] = useState(isFavorite)

    const handleOnDoneModal = () => {
        // check if edit mode
        if (editingIndex >= 0) {
            dispatch(editStandardEndpointToDeployList(editingIndex))
        } else {
            const endpoint: StandardEndpointForDeploy = {
                leaf: {
                    id: wizardLeaf.id, // [leaf][id]
                    name: wizardLeaf.name, // [leaf][name]
                    pod_id: wizardLeaf.pod_id, // [leaf][pod_id]
                },
                policy_name: wizardPolicyName, // [policy_name]
                interface_name: wizardInterfaceName, // [interface_name]
                server_name: wizardEndpointName, // [server_name]
            }
            dispatch(addNewStandardEndpointToDeployList(endpoint))
        }

        dispatch(resetWizard())
        dispatch(setIsModalOpenState(false))
    }

    const handleFavoriteClick = async (isFav: boolean) => {
        let res
        if (isFav) {
            await dispatch(
                deleteFavoritePage({
                    pageId,
                    solutionId: currentSolution.id,
                })
            ).then((response) => {
                res = response
            })
        } else {
            await dispatch(
                addFavoritePage({
                    pageId,
                    solutionId: currentSolution.id,
                })
            ).then((response) => {
                res = response
            })
        }
        if (res === 'ok') {
            setIsPageFavorite(!isPageFavorite)
        }
        // return res
    }

    const handleClearAll = () => {
        dispatch(resetEndpointsToDeploy())
    }

    return (
        <div className={classes.container}>
            <div className={classes.titleContainer}>
                <TextBox variant="h3">{t('dse.pageTitle')}</TextBox>

                <SomeLogoIcon className={classes.someLogo} />
                <FavoriteIcon
                    onClick={() => handleFavoriteClick(isPageFavorite)}
                    className={`${classes.favoriteIconContainer} ${
                        isPageFavorite
                            ? classes.isFavorite
                            : classes.isNotFavorite
                    }`}
                />
            </div>
            <TextBox variant="subtitle2">{t('dse.pageSubtitle1')}</TextBox>
            <TextBox variant="subtitle2">{t('dse.pageSubtitle2')}</TextBox>
            <AddStandardEndPointModal
                dispatch={dispatch}
                modalOpen={modalOpen}
                isEpAndLeafValid={isEpAndLeafValid}
                setIsEpAndLeafValid={(val: boolean) =>
                    dispatch(setIsEpAndLeafValid(val))
                }
                isInterfaceValid={isInterfaceValid}
                setIsInterfaceValid={(val: boolean) =>
                    dispatch(setIsInterfaceValid(val))
                }
                isPolicyValid={isPolicyValid}
                setIsPolicyValid={(val: boolean) =>
                    dispatch(setIsPolicyValid(val))
                }
                toggleModal={() => dispatch(setIsModalOpenState(!modalOpen))}
                solutionLeafs={solutionLeafs}
                solutionPolicies={solutionPolicies}
                solutionAaeps={solutionAaeps}
                wizardEndpointName={wizardEndpointName}
                setWizardEndpointName={(val: string) =>
                    dispatch(setWizardEndpointName(val))
                }
                leafId={wizardLeaf?.id}
                wizardLeaf={wizardLeaf}
                leafInterfaces={leafInterfaces}
                policySelectedTab={policySelectedTab}
                setPolicySelectedTab={(val: number) =>
                    dispatch(setPolicySelectedTab(val))
                }
                setLeaf={(val: AciLeaf) => dispatch(setLeaf(val))}
                wizardAaep={wizardAaep}
                setAttachableAccessEntityProfile={(val: string) =>
                    dispatch(setAttachableAccessEntityProfile(val))
                }
                wizardInterfaceName={wizardInterfaceName}
                setWizardInterfaceName={(value) =>
                    dispatch(setWizardInterfaceName(value))
                }
                interfacePolicyGroupName={interfacePolicyGroupName}
                setInterfacePolicyGroupName={(val: string) =>
                    dispatch(setInterfacePolicyGroupName(val))
                }
                wizardPolicyName={wizardPolicyName}
                setWizardPolicyName={(val: string) =>
                    dispatch(setWizardPolicyName(val))
                }
                getLeafInterfaces={(leafId, podId) =>
                    dispatch(
                        getLeafInterfaces({
                            solutionId: currentSolution.id,
                            leafId,
                            podId,
                        })
                    )
                }
                connectedToSwitch={connectedToSwitch}
                setConnectedToSwitch={(val: boolean) =>
                    dispatch(setConnectedToSwitch(val))
                }
                createAaep={(aaep: CreateAaepPostReq) =>
                    dispatch(createAaep(aaep))
                }
                createPolicy={(policy: CreatePolicyPostReq) =>
                    dispatch(createPolicy(policy))
                }
                createAaepResponse={createAaepResponse}
                onDone={handleOnDoneModal}
            />
            <CustomTable
                headerRightSideComponent={() => (
                    <ASEButton
                        onClick={() =>
                            dispatch(setIsModalOpenState(!modalOpen))
                        }
                    />
                )}
                minHeight={
                    standardEndpointsToDeploy &&
                    standardEndpointsToDeploy.length <= 0
                        ? ''
                        : '500px'
                }
                maxHeight="650px"
                containerClassName={classes.table}
                title={t('dse.tableTitle')}
                columns={columns}
                rows={standardEndpointsToDeploy.map(
                    (
                        ep: StandardEndpointForDeploy,
                        epIndex: number
                    ): StandardEndpointToDeployRow => {
                        return {
                            deviceName: ep.server_name,
                            leafId: ep.leaf.id,
                            interfaceId: ep.interface_name,
                            policy: ep.policy_name,
                            errorButtons: ep.deployErrorMsg,
                        }
                    }
                )}
                totalRows={standardEndpointsToDeploy.length}
                dispatch={dispatch}
            />
            {standardEndpointsToDeploy &&
                standardEndpointsToDeploy.length <= 0 && (
                    <div className={classes.noEndpoints}>
                        <TextBox
                            variant="h4"
                            className={classes.noEndpointsTitle}
                        >
                            {t('dse.noDataTitle1')}
                        </TextBox>
                        <TextBox className={classes.noEndpointsTitle}>
                            {t('dse.noDataTitle2')}
                        </TextBox>
                        <ASEButton
                            onClick={() =>
                                dispatch(setIsModalOpenState(!modalOpen))
                            }
                        />
                    </div>
                )}
            <Box component="div" className={classes.bottomContainer}>
                <TooltipButton
                    disabled={
                        standardEndpointsToDeploy &&
                        standardEndpointsToDeploy.length <= 0
                            ? true
                            : false
                    }
                    renderPopper={(closeFunc) => (
                        <ClearAllValidationPopper
                            onConfirm={() => {
                                handleClearAll()
                                closeFunc()
                            }}
                            closePopper={closeFunc}
                        />
                    )}
                    popperPlacement="top"
                    textValue="Clear All"
                    buttonStyle={{ marginRight: '15px' }}
                />
                <AppButton
                    disabled={
                        standardEndpointsToDeploy &&
                        standardEndpointsToDeploy.length <= 0
                            ? true
                            : false
                    }
                    onClick={() =>
                        dispatch(
                            deployStandardEndpoints(standardEndpointsToDeploy)
                        )
                    }
                >
                    {t('dse.tableDeployBtn')}
                </AppButton>
            </Box>

            <Box>
                {deployStatus === 'failed' && (
                    <Toast
                        isOpen={isToastOpen}
                        toggleToast={() => dispatch(toggleToast())}
                        severity="error"
                        text={deployMsg}
                    />
                )}
                {deployStatus === 'succeed' && (
                    <Toast
                        isOpen={isToastOpen}
                        toggleToast={() => dispatch(toggleToast())}
                        severity="success"
                        text={deployMsg}
                    />
                )}
            </Box>
        </div>
    )
}

interface ClearAllValidationPopperProps {
    closePopper: () => void
    onConfirm: () => void
}

const ClearAllValidationPopper = ({
    closePopper,
    onConfirm,
}: ClearAllValidationPopperProps) => {
    return (
        <Box component="div" padding="20px">
            <TextBox>Are you sure?</TextBox>
            <Box
                component="div"
                display="flex"
                justifyContent="flex-end"
                marginTop="30px"
            >
                <AppButton
                    style={{ marginRight: '10px' }}
                    onClick={closePopper}
                    data-outline
                >
                    Cancel
                </AppButton>
                <AppButton onClick={onConfirm}>Confirm</AppButton>
            </Box>
        </Box>
    )
}

export default DeployStandardEndpoint
