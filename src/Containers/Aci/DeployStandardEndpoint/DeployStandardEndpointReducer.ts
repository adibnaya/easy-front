import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { AxiosResponse } from 'axios'
import api from '../../../Api/Api'
import {
    AciStandardEndpointsBody,
    AciStandardEndpointsResponse,
    CreateAeaepResponse,
    ParsedResponseData,
    StandardEndpointForDeploy,
} from '../AciApi'
import {
    AciLeaf,
    ApiResponse,
    InterfaceData,
    CreatePolicyPostReq,
    CreateAaepPostReq,
} from '../Models/AciModels'
import { AppThunk } from '../../../Redux/Store'
import { setIsPageLoading } from '../../DrawerAndBar/DrawerReducer'
import { toggleToast } from '../../Login/LoginReducer'

type DeployStandardEndpointSliceState = {
    // list to deploy
    standardEndpointsToDeploy: StandardEndpointForDeploy[]
    // modal state
    modalOpen: boolean
    // steps validation state
    isEpAndLeafValid: boolean
    isInterfaceValid: boolean
    isPolicyValid: boolean

    // end point data for the wizard
    wizardLeaf: AciLeaf // {leafId, leafName, leafPodId}
    wizardPolicyName: string
    wizardInterfaceName: string
    wizardEndpointName: string // server name

    // End Point & Leaf - leafs list for dropdown
    solutionLeafs: AciLeaf[]
    // Interface - leaf interfaces list for table
    leafInterfaces: InterfaceData[]
    // Policy - policies list for for existing policies dropdown & aaep list for creating new policy
    solutionPolicies: { name: string }[]
    solutionAaeps: { name: string }[]

    // fields to create new policy
    policySelectedTab: number
    wizardAaep: string
    interfacePolicyGroupName: string
    connectedToSwitch: boolean

    // create response from server
    createAaepResponse: string
    createPolicyResponse: string

    deployStatus: string
    deployMsg: string

    editingIndex: number
}

const initialState: DeployStandardEndpointSliceState = {
    standardEndpointsToDeploy: [],
    modalOpen: false,
    isEpAndLeafValid: false,
    isInterfaceValid: false,
    isPolicyValid: false,
    wizardLeaf: { id: '', name: '', pod_id: '' },
    wizardInterfaceName: '',
    wizardEndpointName: '',
    wizardPolicyName: '',

    solutionLeafs: [],
    leafInterfaces: [],
    solutionPolicies: [],
    solutionAaeps: [],

    policySelectedTab: 0,
    wizardAaep: '',
    interfacePolicyGroupName: '',
    connectedToSwitch: false,

    createAaepResponse: '',
    createPolicyResponse: '',

    deployStatus: '',
    deployMsg: '',

    editingIndex: -1,
}

const dse = createSlice({
    name: 'dse',
    initialState,
    reducers: {
        resetPage: () => {
            return initialState
        },
        setIsModalOpenState: (state, action: PayloadAction<boolean>) => {
            state.modalOpen = action.payload
        },
        // initialize wizard data
        setSolutionLeafs: (state, action: PayloadAction<AciLeaf[]>) => {
            state.solutionLeafs = action.payload
        },
        setSolutionPolicies: (
            state,
            action: PayloadAction<{ name: string }[]>
        ) => {
            state.solutionPolicies = action.payload
        },
        setSolutionAaeps: (
            state,
            action: PayloadAction<{ name: string }[]>
        ) => {
            state.solutionAaeps = action.payload
        },

        // set steps validations
        setIsEpAndLeafValid: (state, action: PayloadAction<boolean>) => {
            state.isEpAndLeafValid = action.payload
        },
        setIsInterfaceValid: (state, action: PayloadAction<boolean>) => {
            state.isInterfaceValid = action.payload
        },
        setIsPolicyValid: (state, action: PayloadAction<boolean>) => {
            state.isPolicyValid = action.payload
        },

        // set current endpoint value for wizard
        setWizardEndpointName: (state, action: PayloadAction<string>) => {
            state.wizardEndpointName = action.payload
        },
        setLeaf: (state, action: PayloadAction<AciLeaf>) => {
            state.wizardLeaf = action.payload
            // reset the value in interface step
            state.wizardInterfaceName = ''
        },
        setPolicySelectedTab: (state, action: PayloadAction<number>) => {
            state.policySelectedTab = action.payload
            // reset values
            state.wizardPolicyName = ''
            state.wizardAaep = ''
            state.interfacePolicyGroupName = ''
            state.connectedToSwitch = false
        },
        setAttachableAccessEntityProfile: (
            state,
            action: PayloadAction<string>
        ) => {
            state.wizardAaep = action.payload
        },
        setInterfacePolicyGroupName: (state, action: PayloadAction<string>) => {
            state.interfacePolicyGroupName = action.payload
        },
        setConnectedToSwitch: (state, action: PayloadAction<boolean>) => {
            state.connectedToSwitch = action.payload
        },
        setWizardInterfaceName: (state, action: PayloadAction<string>) => {
            state.wizardInterfaceName = action.payload
        },
        setWizardPolicyName: (state, action: PayloadAction<string>) => {
            state.wizardPolicyName = action.payload
        },
        setLeafInterfaces: (state, action: PayloadAction<InterfaceData[]>) => {
            state.leafInterfaces = action.payload
        },
        resetLeafInterfaces: (state) => {
            state.leafInterfaces = []
        },
        resetEndpointsToDeploy: (state) => {
            state.standardEndpointsToDeploy = []
        },
        setCreateAaepResponse: (state, action: PayloadAction<string>) => {
            state.createAaepResponse = action.payload
        },
        setCreatePolicyResponse: (state, action: PayloadAction<string>) => {
            state.createPolicyResponse = action.payload
        },
        resetWizard: (state) => {
            state.wizardLeaf = { id: '', name: '', pod_id: '' }
            state.wizardInterfaceName = ''
            state.wizardEndpointName = ''
            state.wizardPolicyName = ''
            state.leafInterfaces = []
            state.solutionAaeps = []
            state.policySelectedTab = 0
            state.wizardAaep = ''
            state.interfacePolicyGroupName = ''
            state.connectedToSwitch = false
            state.isEpAndLeafValid = false
            state.isInterfaceValid = false
            state.isPolicyValid = false
        },
        addNewStandardEndpointToDeployList: (
            state,
            action: PayloadAction<StandardEndpointForDeploy>
        ) => {
            state.standardEndpointsToDeploy = [
                ...state.standardEndpointsToDeploy,
                action.payload,
            ]
        },
        removeStandardEndpointToDeployList: (
            state,
            action: PayloadAction<number>
        ) => {
            var newArray = [...state.standardEndpointsToDeploy]
            newArray.splice(action.payload, 1)
            state.standardEndpointsToDeploy = newArray
        },
        editStandardEndpointToDeployList: (
            state,
            action: PayloadAction<number>
        ) => {
            var newArray = [...state.standardEndpointsToDeploy]
            // get index
            const epToEditIndex = action.payload
            console.log('epToEditIndex', epToEditIndex)
            if (epToEditIndex >= 0) {
                // change values
                newArray[epToEditIndex].interface_name =
                    state.wizardInterfaceName
                newArray[epToEditIndex].leaf = {
                    id: state.wizardLeaf.id,
                    name: state.wizardLeaf.name,
                    pod_id: state.wizardLeaf.pod_id,
                }
                newArray[epToEditIndex].policy_name = state.wizardPolicyName
                newArray[epToEditIndex].server_name = state.wizardEndpointName
                // assign the new array
                state.standardEndpointsToDeploy = newArray
            }
            // reset editing index value
            state.editingIndex = -1
        },
        setDeployStatus: (state, action: PayloadAction<string>) => {
            state.deployStatus = action.payload
        },
        setDeployMsg: (state, action: PayloadAction<string>) => {
            state.deployMsg = action.payload
        },
        setStandardEndpointsToDeploy: (
            state,
            action: PayloadAction<StandardEndpointForDeploy[]>
        ) => {
            state.standardEndpointsToDeploy = [...action.payload]
        },
        setEditingIndex: (state, action: PayloadAction<number>) => {
            state.editingIndex = action.payload
        },
    },
})

export const {
    resetPage,
    setIsModalOpenState,
    setSolutionLeafs,
    setIsEpAndLeafValid,
    setIsInterfaceValid,
    setIsPolicyValid,
    setWizardEndpointName,
    setLeaf,
    setSolutionPolicies,
    setPolicySelectedTab,
    setSolutionAaeps,
    setAttachableAccessEntityProfile,
    setInterfacePolicyGroupName,
    setWizardInterfaceName,
    setWizardPolicyName,
    setConnectedToSwitch,
    setLeafInterfaces,
    resetLeafInterfaces,
    resetEndpointsToDeploy,
    setCreateAaepResponse,
    setCreatePolicyResponse,
    addNewStandardEndpointToDeployList,
    resetWizard,
    editStandardEndpointToDeployList,
    removeStandardEndpointToDeployList,
    setDeployStatus,
    setDeployMsg,
    setStandardEndpointsToDeploy,
    setEditingIndex,
} = dse.actions

export const getSolutionLeafs = (solutionId: string): AppThunk => async (
    dispatch
) => {
    let response
    try {
        response = await api.get<any, AxiosResponse<ApiResponse>>(
            `/solutions/${solutionId}/aci-leafs`
        )
    } catch (e) {
        return
    }
    const apiResponse = response.data
    const leafs = apiResponse.data
    dispatch(setSolutionLeafs(leafs))
}

export const getSolutionPolicies = (solutionId: string): AppThunk => async (
    dispatch
) => {
    let response
    try {
        response = await api.get<any, AxiosResponse<ApiResponse>>(
            `/solutions/${solutionId}/aci-policies`
        )
    } catch (e) {
        return
    }
    const apiResponse = response.data
    const policies = apiResponse.data as { name: string }[]
    dispatch(setSolutionPolicies(policies))
}

export const getSolutionAaeps = (solutionId: string): AppThunk => async (
    dispatch
) => {
    try {
        const axiosResponse = await api.get<any, AxiosResponse<ApiResponse>>(
            `/solutions/${solutionId}/aci-aaeps?dropdown=true`
        )
        const apiResponse = axiosResponse.data
        const aaepItems = apiResponse.data as { name: string }[]
        dispatch(setSolutionAaeps(aaepItems))
    } catch (e) {}
}

export const getLeafInterfaces = (params: {
    solutionId: string
    leafId: string
    podId: string
}): AppThunk => async (dispatch) => {
    let response
    try {
        response = await api.get(
            `/solutions/${params.solutionId}/aci-leafs/${params.leafId}/pods/${params.podId}`
        )
    } catch (e) {
        return
    }
    dispatch(setLeafInterfaces(response.data.data))
}

export const disableLeafInterface = (interfaceName: string): AppThunk => async (
    dispatch,
    getState
) => {
    const store = getState()
    const { currentSolution } = store.drawer
    const { wizardLeaf } = store.dse
    try {
        dispatch(resetLeafInterfaces())
        await api.get(
            `/solutions/${currentSolution?.id}/aci-leafs/${wizardLeaf?.id}/pods/${wizardLeaf?.pod_id}/interfaces/${interfaceName}/disable`
        )
    } catch (e) {
        dispatch(
            getLeafInterfaces({
                solutionId: currentSolution?.id || '',
                leafId: wizardLeaf?.id || '',
                podId: wizardLeaf?.pod_id || '',
            })
        )
        return
    }
    dispatch(
        getLeafInterfaces({
            solutionId: currentSolution?.id || '',
            leafId: wizardLeaf?.id || '',
            podId: wizardLeaf?.pod_id || '',
        })
    )
}

export const createAaep = (aaep: CreateAaepPostReq): AppThunk => async (
    dispatch,
    getState
) => {
    const store: any = getState()
    const { currentSolution } = store.drawer

    try {
        if (aaep) {
            dispatch(setIsPageLoading(true))
            const res = await api.post(
                `/solutions/${currentSolution.id}/aci-aaeps`,
                {
                    name: aaep.name,
                }
            )
            if (res.status === 200) {
                let data: CreateAeaepResponse = res.data
                data &&
                    data.status &&
                    dispatch(setCreateAaepResponse(data.status.toString()))
                // refresh the Aaep dropdown values with the added aaep
                await dispatch(getSolutionAaeps(currentSolution.id))
                dispatch(setAttachableAccessEntityProfile(aaep.name))
            } else {
                let data: CreateAeaepResponse = res.data
                data &&
                    data.message &&
                    dispatch(setCreateAaepResponse(data.message))
            }
            dispatch(setIsPageLoading(false))
        }
        return true
    } catch (e) {
        dispatch(setIsPageLoading(false))
        return false
    }
}

export const createPolicy = (policy: CreatePolicyPostReq): AppThunk => async (
    dispatch,
    getState
) => {
    const store: any = getState()
    const { currentSolution } = store.drawer
    const { wizardLeaf, wizardInterfaceName, wizardEndpointName } = store.dse

    try {
        if (policy) {
            dispatch(setIsPageLoading(true))
            const res = await api.post(
                `/solutions/${currentSolution.id}/aci-policies`,
                policy
            )

            if (res.status === 200) {
                let data: CreateAeaepResponse = res.data
                data &&
                    data.status &&
                    dispatch(setCreatePolicyResponse(data.status.toString()))
                // refresh the Policies dropdown values with the added aaep
                await dispatch(getSolutionPolicies(currentSolution.id))
                await dispatch(setWizardPolicyName(policy.name))

                // add ep to list and close modal
                dispatch(setIsPageLoading(true))
                let newEP: StandardEndpointForDeploy = {
                    leaf: {
                        id: wizardLeaf.id,
                        name: wizardLeaf.name,
                        pod_id: wizardLeaf.pod_id,
                    },
                    policy_name: policy.name,
                    interface_name: wizardInterfaceName,
                    server_name: wizardEndpointName,
                }
                await dispatch(addNewStandardEndpointToDeployList(newEP))
                dispatch(resetWizard())
                dispatch(setIsModalOpenState(false))
                dispatch(setIsPageLoading(false))
            } else {
                let data: CreateAeaepResponse = res.data
                data &&
                    data.message &&
                    dispatch(setCreatePolicyResponse(data.message))
            }
            dispatch(setIsPageLoading(false))
        }
        return true
    } catch (e) {
        dispatch(setIsPageLoading(false))
        return false
    }
}

export const deployStandardEndpoints = (
    endpointsToDeploy: StandardEndpointForDeploy[]
): AppThunk => async (dispatch, getState) => {
    const store: any = getState()
    const { currentSolution } = store.drawer

    try {
        const body: AciStandardEndpointsBody = { endpoints: endpointsToDeploy }
        dispatch(setIsPageLoading(true))
        const res = await api.post<
            any,
            AxiosResponse<AciStandardEndpointsResponse[]>
        >(`/solutions/${currentSolution.id}/aci-standard-endpoints`, body)
        // console.log('deployStandardEndpoints', body, res)
        if (res.status === 200) {
            const endpointsResponses = res.data

            let failedDeploysArray: StandardEndpointForDeploy[] = []

            endpointsResponses.forEach((epResponse, index) => {
                const status = epResponse.status
                if (status) {
                    console.log(`EP index ${index} status ${status}`)
                } else {
                    const parsedInfo: ParsedResponseData = JSON.parse(
                        epResponse.data
                    )
                    const errorMsg =
                        parsedInfo?.imdata[0]?.error?.attributes?.text
                    // push the failed ep at {index} to the failed array with the error msg
                    let failedEp = {
                        ...endpointsToDeploy[index],
                        deployErrorMsg: errorMsg,
                    }
                    failedDeploysArray.push(failedEp)
                }
            })
            dispatch(setIsPageLoading(false))

            // if no fails - the toast success!
            if (failedDeploysArray.length <= 0) {
                dispatch(
                    setDeployMsg(
                        `Successfully deployed ${endpointsToDeploy.length} endpoints!`
                    )
                )
                dispatch(setDeployStatus('succeed'))
            } else {
                // failed
                dispatch(
                    setDeployMsg(
                        `Deploy failed for ${failedDeploysArray.length}/${endpointsToDeploy.length} Endpoints!`
                    )
                )
                dispatch(setDeployStatus('failed'))
            }
            dispatch(setStandardEndpointsToDeploy(failedDeploysArray))
            dispatch(toggleToast())
        } else {
            console.log('deployStandardEndpoints status:', res.status)
        }
    } catch (e) {
        dispatch(setIsPageLoading(false))
        console.log('deployStandardEndpoints catch e:', e)
    }
}

export const InitializeModalWithEndpointValues = (
    rowIndex: number
): AppThunk => async (dispatch, getState) => {
    const store: any = getState()
    const { currentSolution } = store.drawer
    const { standardEndpointsToDeploy } = store.dse

    try {
        // get the ep info
        const ep: StandardEndpointForDeploy =
            standardEndpointsToDeploy[rowIndex]

        // set the wizard values
        dispatch(setWizardEndpointName(ep.server_name))
        dispatch(setLeaf(ep.leaf))
        dispatch(
            getLeafInterfaces({
                solutionId: currentSolution.id,
                leafId: ep.leaf.id,
                podId: ep.leaf.pod_id,
            })
        )
        dispatch(setWizardInterfaceName(ep.interface_name))
        dispatch(setPolicySelectedTab(0))
        dispatch(setWizardPolicyName(ep.policy_name))
        // open wizard
        dispatch(setIsModalOpenState(true))
    } catch (e) {
        // debugger
    }
}

export default dse.reducer
