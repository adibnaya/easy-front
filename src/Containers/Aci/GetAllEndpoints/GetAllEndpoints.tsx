import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import TextBox from '../../../Components/TextBox/TextBox'
import useStyles from './GetAllEndpoints.style'
import CustomTable, { Column } from '../../../Components/Table/Table'
import CustomTablePagination from '../../../Components/Table/TablePagination/TablePagination'
import AppLink from '../../../Components/Link/Link'
import { RootState } from '../../../Redux/Store'
import { ReactComponent as SomeLogoIcon } from '../../../Assets/some_logo.svg'
import {
    getEndpointsData,
    resetEndpoints,
    exportEndpointsData,
} from './GetAllEndpointsReducer'
import { ReactComponent as FavoriteIcon } from '../../../Assets/favorite_white.svg'
import {
    deleteFavoritePage,
    addFavoritePage,
} from '../../DrawerAndBar/DrawerReducer'

const columns: Column[] = [
    { id: 'tenant', label: 'TENANT' },
    { id: 'ap', label: 'App. Profile' },
    { id: 'epg', label: 'EPG' },
    { id: 'ip', label: 'IP Address' },

    {
        id: 'mac',
        label: 'MAC Address',
        renderCell: (value: boolean) => (
            <AppLink underline="always" onClick={() => console.log(value)}>
                {value}
            </AppLink>
        ),
    },
    { id: 'created', label: 'Created', sortable: true },
    {
        id: 'path',
        label: 'Learn at path',
    },
    { id: 'vlan', label: 'VLAN' },
    { id: 'learned_by', label: 'EP Learned By' },
    {
        id: 'vm_host_verification["esx"]',
        label: 'VM host verification',
        ellipsisWidth: '20px',
        minWidth: 130,
    },
]

interface Props {
    pageId: string
    isFavorite: boolean
}

const GetAllEndpoints = ({ pageId, isFavorite }: Props) => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const [isPageFavorite, setIsPageFavorite] = useState(isFavorite)

    const { drawer, getAllEndpoints } = useSelector((state: RootState) => state)
    const { currentSolution } = drawer
    const { endpoints, metaAndLinks, fetchingData } = getAllEndpoints

    useEffect(() => {
        if (currentSolution) {
            dispatch(resetEndpoints())
            dispatch(getEndpointsData({ solutionId: currentSolution.id }))
        }
    }, [dispatch, currentSolution])

    const handleBackPress = () => {
        dispatch(
            getEndpointsData({
                solutionId: currentSolution.id,
                page: metaAndLinks.meta.current_page - 1,
                perPage: metaAndLinks.meta.per_page,
            })
        )
    }
    const handleFirstPress = () => {
        dispatch(
            getEndpointsData({
                solutionId: currentSolution.id,
                page: 1,
                perPage: metaAndLinks.meta.per_page,
            })
        )
    }
    const handleLastPress = () => {
        dispatch(
            getEndpointsData({
                solutionId: currentSolution.id,
                page: metaAndLinks.meta.last_page,
                perPage: metaAndLinks.meta.per_page,
            })
        )
    }
    const handleNextPress = () => {
        dispatch(
            getEndpointsData({
                solutionId: currentSolution.id,
                page: metaAndLinks.meta.current_page + 1,
                perPage: metaAndLinks.meta.per_page,
            })
        )
    }

    const handleChangeRowsPerPage = (perPage: number) => {
        dispatch(
            getEndpointsData({
                solutionId: currentSolution.id,
                page: 1,
                perPage,
            })
        )
    }

    const handleSearch = (str: string) => {
        if (str.length > 2) {
            dispatch(
                getEndpointsData({
                    solutionId: currentSolution.id,
                    page: 1,
                    perPage: metaAndLinks.meta.per_page,
                    filterString: str,
                })
            )
        } else if (str.length === 0) {
            handleFirstPress()
        }
    }

    const exportToCSV = async (): Promise<string> => {
        let res = ''
        await dispatch(exportEndpointsData(currentSolution.id)).then(
            (csvData) => {
                res = csvData
            }
        )
        return res
    }

    const handleFavoriteClick = async (isFav: boolean) => {
        let res
        if (isFav) {
            await dispatch(
                deleteFavoritePage({
                    pageId,
                    solutionId: currentSolution.id,
                })
            ).then((response) => {
                res = response
            })
        } else {
            await dispatch(
                addFavoritePage({
                    pageId,
                    solutionId: currentSolution.id,
                })
            ).then((response) => {
                res = response
            })
        }
        if (res === 'ok') {
            setIsPageFavorite(!isPageFavorite)
        }
        // return res
    }

    return (
        <div className={classes.container}>
            <div className={classes.titleContainer}>
                <TextBox display="inline" variant="h3">
                    Get All Endpoints From APIC
                </TextBox>
                <SomeLogoIcon className={classes.someLogo} />
                <FavoriteIcon
                    onClick={() => handleFavoriteClick(isPageFavorite)}
                    className={`${classes.favoriteIconContainer} ${
                        isPageFavorite
                            ? classes.isFavorite
                            : classes.isNotFavorite
                    }`}
                />
            </div>
            <TextBox variant="subtitle2">
                This Page List All Fabric Discovered Endpoints
            </TextBox>
            <CustomTable
                totalRows={metaAndLinks?.meta?.total || 0}
                maxHeight="650px"
                containerClassName={classes.table}
                title="Fabric Discovered Endpoints"
                columns={columns}
                rows={endpoints}
                onSearchChanged={handleSearch}
                exportToExcelFunction={exportToCSV}
                renderPagination={() => (
                    <CustomTablePagination
                        handleBackPress={handleBackPress}
                        handleFirstPress={handleFirstPress}
                        handleLastPress={handleLastPress}
                        handleNextPress={handleNextPress}
                        currentPage={metaAndLinks?.meta?.current_page || 1}
                        lastPage={metaAndLinks?.meta?.last_page || 1}
                        handleChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                )}
                search
                isLoading={fetchingData}
                // defaultSortBy="created"
            />
        </div>
    )
}

export default GetAllEndpoints
