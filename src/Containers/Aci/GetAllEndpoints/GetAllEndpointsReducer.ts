import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import api from '../../../Api/Api'
import { MetaAndLinks } from '../../DrawerAndBar/DrawerReducer'
import { AppThunk } from '../../../Redux/Store'

export interface StandardEndpointData {
    tenant: string
    ap: string
    epg: string
    ip: string
    mac: string
    created: string
    path: string
    vlan: string
    learned_by: string
    vm_host_verification: {
        esx: string
        vm_id: string
    }
}

interface GetAllEndpointsSliceState {
    endpoints: StandardEndpointData[]
    metaAndLinks: MetaAndLinks | undefined
    fetchingData: boolean
}

const initialState: GetAllEndpointsSliceState = {
    endpoints: [],
    metaAndLinks: undefined,
    fetchingData: false,
}

const getAllEndpoints = createSlice({
    name: 'getAllEndpoints',
    initialState,
    reducers: {
        setEndpoints: (
            state,
            action: PayloadAction<StandardEndpointData[]>
        ) => {
            state.endpoints = action.payload
        },
        setMetaAndLinks: (state, action: PayloadAction<MetaAndLinks>) => {
            state.metaAndLinks = action.payload
        },
        setFetchingData: (state, action: PayloadAction<boolean>) => {
            state.fetchingData = action.payload
        },
        resetEndpoints: () => {
            return initialState
        },
    },
})

export const {
    setEndpoints,
    setMetaAndLinks,
    resetEndpoints,
    setFetchingData,
} = getAllEndpoints.actions

export const getEndpointsData = (params: {
    solutionId: string
    page?: number
    filterString?: string
    perPage?: number
}): AppThunk => async (dispatch) => {
    let response
    try {
        dispatch(setFetchingData(true))
        response = await api.get(
            `/solutions/${params.solutionId}/aci-standard-endpoints?page=${
                params.page || '1'
            }&per_page=${params.perPage || 100}&str=${
                params.filterString || ''
            }`
        )
        dispatch(setFetchingData(false))
    } catch (e) {
        dispatch(setFetchingData(false))
        return
    }
    dispatch(setEndpoints(response.data.data))
    dispatch(
        setMetaAndLinks({
            meta: response.data.meta,
            links: response.data.links,
        })
    )
}

export const exportEndpointsData = (solutionId: string): AppThunk => {
    return async () => {
        let response
        try {
            response = await api.get(
                `/solutions/${solutionId}/aci-standard-endpoints?export-csv=true`
            )
            return response.data
        } catch (e) {
            return ''
        }
    }
}

export default getAllEndpoints.reducer
