import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import api from '../../../Api/Api'
import { MetaAndLinks } from '../../DrawerAndBar/DrawerReducer'
import { AppThunk } from '../../../Redux/Store'
import {AxiosResponse} from "axios";
import {AciLeaf, ApiResponse, InterfaceData} from "../Models/AciModels";
import {
    resetLeafInterfaces,
} from "../DeployStandardEndpoint/DeployStandardEndpointReducer";
import {LeafInterface} from "../../../Components/AddStandardEndPiontModal/AddStandardEndPointModalModel";

export interface StandardEndpointData {
    tenant: string
    ap: string
    epg: string
    ip: string
    mac: string
    created: string
    path: string
    vlan: string
    learned_by: string
    vm_host_verification: {
        esx: string
        vm_id: string
    }
}

interface LeafInterfaceDetailsSliceState {
    solutionLeafs: AciLeaf[]
    leafInterfaces: LeafInterface[]
    leaf: AciLeaf
    metaAndLinks: MetaAndLinks | undefined
    fetchingData: boolean
}

const initialState: LeafInterfaceDetailsSliceState = {
    solutionLeafs: [],
    leafInterfaces: [],
    metaAndLinks: undefined,
    leaf: { id: '', name: '', pod_id: '' },
    fetchingData: false,
}

const leafInterfaceDetails = createSlice({
    name: 'leafInterfaceDetails',
    initialState,
    reducers: {
        setSolutionLeafs: (
            state,
            action: PayloadAction<AciLeaf[]>
        ) => {
            state.solutionLeafs = action.payload
        },
        setLeafInterfaces: (state, action: PayloadAction<InterfaceData[]>) => {
            state.leafInterfaces = action.payload
        },
        setMetaAndLinks: (state, action: PayloadAction<MetaAndLinks>) => {
            state.metaAndLinks = action.payload
        },
        setLeaf: (state, action: PayloadAction<AciLeaf>) => {
            state.leaf = action.payload
        },
        setFetchingData: (state, action: PayloadAction<boolean>) => {
            state.fetchingData = action.payload
        },
        resetEndpoints: () => {
            return initialState
        },
    },
})

export const {
    setSolutionLeafs,
    setLeafInterfaces,
    setMetaAndLinks,
    resetEndpoints,
    setLeaf,
    setFetchingData,
} = leafInterfaceDetails.actions




export const getSolutionLeafs = (solutionId: string): AppThunk => async (
    dispatch
) => {
    let response
    try {
        response = await api.get<any, AxiosResponse<ApiResponse>>(
            `/solutions/${solutionId}/aci-leafs`
        )
    } catch (e) {
        return
    }
    const apiResponse = response.data
    const leafs = apiResponse.data
    dispatch(setSolutionLeafs(leafs))
}



export const getLeafInterfaces = (params: {
    solutionId: string
    leafId: string
    podId: string
}): AppThunk => async (dispatch) => {
    let response
    try {
        response = await api.get(
            `/solutions/${params.solutionId}/aci-leafs/${params.leafId}/pods/${params.podId}`
        )
    } catch (e) {
        return
    }
    dispatch(setLeafInterfaces(response.data.data))
}


export const disableLeafInterface = (interfaceName: string): AppThunk => async (
    dispatch,
    getState
) => {
    const store = getState()
    const { currentSolution } = store.drawer
    const { wizardLeaf } = store.dse
    try {
        dispatch(resetLeafInterfaces())
        await api.get(
            `/solutions/${currentSolution?.id}/aci-leafs/${wizardLeaf?.id}/pods/${wizardLeaf?.pod_id}/interfaces/${interfaceName}/disable`
        )
    } catch (e) {
        dispatch(
            getLeafInterfaces({
                solutionId: currentSolution?.id || '',
                leafId: wizardLeaf?.id || '',
                podId: wizardLeaf?.pod_id || '',
            })
        )
        return
    }
    dispatch(
        getLeafInterfaces({
            solutionId: currentSolution?.id || '',
            leafId: wizardLeaf?.id || '',
            podId: wizardLeaf?.pod_id || '',
        })
    )
}


export const exportEndpointsData = (solutionId: string): AppThunk => {
    return async () => {
        let response
        try {
            response = await api.get(
                `/solutions/${solutionId}/aci-standard-endpoints?export-csv=true`
            )
            return response.data
        } catch (e) {
            return ''
        }
    }
}

export default leafInterfaceDetails.reducer
