export type ApiResponse = {
    data: any[]
    links: {
        first: string
        last: string
        prev: string
        next: string
    }
    meta: {
        current_page: number
        from: number
        last_page: number
        path: string
        per_page: number
        to: number
        total: number
        export_link: string
    }
}

export type AciLeaf = {
    id: string
    name: string
    pod_id: string
}

export type StandardEndpointsTable = {
    columns: any[]
    standardEndpoints: StandardEndpoint[]
}

export type StandardEndpoint = {
    endpoint: Endpoint
    leaf: Leaf
    policy: Policy
}

export type Endpoint = {
    name: string
}

export type Leaf = {
    id: string
    interfaces?: InterfaceData[]
}

export type Policy = {
    aaep?: string
    interfacePolicyGroupName: string
    isConnectedToSwitch: boolean
}

export type CreateAaepPostReq = {
    name: string // aaep_name_{{counter}}
}

export type CreatePolicyPostReq = {
    name: string // policy_name_{{counter}}
    aaep_name: string // {{aaep_name}}
    connected_to_switch: boolean // 1
}

export interface InterfaceData {
    admin_status: string
    interface: string
    description: string
    actual_status: string
    duplex: string
    speed: string
    status_reason: string
    last_change: string
    byte_tx: string
    byte_rx: string
    broadcast_packets: string
    crc_errors: string
    collisions: string
    drop_events: string
    fragments: string
    jabbers: string
    multicast_packets: string
    last_cleared: string
}
