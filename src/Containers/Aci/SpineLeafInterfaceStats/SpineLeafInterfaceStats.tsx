import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import TextBox from '../../../Components/TextBox/TextBox'
import useStyles from './SpineLeafInterfaceStats.style'
import CustomTable, { Column } from '../../../Components/Table/Table'
import CustomTablePagination from '../../../Components/Table/TablePagination/TablePagination'
import AppLink from '../../../Components/Link/Link'
import { RootState } from '../../../Redux/Store'
import { ReactComponent as SomeLogoIcon } from '../../../Assets/some_logo.svg'
import {
    resetEndpoints,
    exportEndpointsData,
    getSolutionLeafs,
    setLeaf,
    getLeafInterfaces,
} from './SpineLeafInterfaceStatsReducer'
import { ReactComponent as FavoriteIcon } from '../../../Assets/favorite_white.svg'
import {
    deleteFavoritePage,
    addFavoritePage,
} from '../../DrawerAndBar/DrawerReducer'
import ActualStatus
    from "../../../Components/AddStandardEndPiontModal/Interface/ActualStatus/ActualStatus";
import AdminStatus
    from "../../../Components/AddStandardEndPiontModal/Interface/AdminStatus/AdminStatus";
import {
    disableLeafInterface,
    // getSolutionLeafs
} from "../DeployStandardEndpoint/DeployStandardEndpointReducer";
import LeafDropdown from "../../../Components/Aci/LeafDropdown/LeafDropdown";
import {AciLeaf} from "../Models/AciModels";

const columns: Column[] = [
    { id: 'interface', label: 'INTERFACE' },
    { id: 'description', label: 'DESCRIPTION' },
    {
        id: 'actual_status',
        label: 'ACTUAL STATUS',
        noPadding: true,
        renderCell: (value, index, row) => {
            return <ActualStatus value={value} />
        },
    },
    {
        id: 'admin_status',
        label: 'ADMIN STATUS',
        renderCell: (value, index, row, dispatch) => {
            return (
                <AdminStatus
                    value={value}
                    disableFunction={() =>
                        dispatch(disableLeafInterface(row.interface))
                    }
                />
            )
        },
    },
    { id: 'duplex', label: 'DUPLEX' },
    { id: 'speed', label: 'SPEED' },
    { id: 'status_reason', label: 'STATUS REASON' },
    {
        id: 'last_change',
        label: 'LINK STATUS LAST CHANGED',
    },
]

interface Props {
    pageId: string
    isFavorite: boolean
}

const SpineLeafInterfaceStats = ({ pageId, isFavorite }: Props) => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const [isPageFavorite, setIsPageFavorite] = useState(isFavorite)

    const { drawer, spineLeafInterfaceStats } = useSelector((state: RootState) => state)
    const { currentSolution } = drawer
    const { solutionLeafs, leafInterfaces, metaAndLinks, fetchingData } = spineLeafInterfaceStats

    useEffect(() => {
        if (currentSolution) {
            dispatch(getSolutionLeafs(currentSolution.id))
        }
    }, [dispatch, currentSolution])

    const handleBackPress = () => {
        // dispatch(
        //     getEndpointsData({
        //         solutionId: currentSolution.id,
        //         page: metaAndLinks.meta.current_page - 1,
        //         perPage: metaAndLinks.meta.per_page,
        //     })
        // )
    }
    const handleFirstPress = () => {
        // dispatch(
        //     getEndpointsData({
        //         solutionId: currentSolution.id,
        //         page: 1,
        //         perPage: metaAndLinks.meta.per_page,
        //     })
        // )
    }
    const handleLastPress = () => {
        // dispatch(
        //     getEndpointsData({
        //         solutionId: currentSolution.id,
        //         page: metaAndLinks.meta.last_page,
        //         perPage: metaAndLinks.meta.per_page,
        //     })
        // )
    }
    const handleNextPress = () => {
        // dispatch(
        //     getEndpointsData({
        //         solutionId: currentSolution.id,
        //         page: metaAndLinks.meta.current_page + 1,
        //         perPage: metaAndLinks.meta.per_page,
        //     })
        // )
    }

    const handleChangeRowsPerPage = (perPage: number) => {
        // dispatch(
        //     getEndpointsData({
        //         solutionId: currentSolution.id,
        //         page: 1,
        //         perPage,
        //     })
        // )
    }

    const handleSearch = (str: string) => {
        // if (str.length > 2) {
        //     dispatch(
        //         getEndpointsData({
        //             solutionId: currentSolution.id,
        //             page: 1,
        //             perPage: metaAndLinks.meta.per_page,
        //             filterString: str,
        //         })
        //     )
        // } else if (str.length === 0) {
        //     handleFirstPress()
        // }
    }

    const exportToCSV = async (): Promise<string> => {
        let res = ''
        await dispatch(exportEndpointsData(currentSolution.id)).then(
            (csvData) => {
                res = csvData
            }
        )
        return res
    }

    const handleFavoriteClick = async (isFav: boolean) => {
        let res
        if (isFav) {
            await dispatch(
                deleteFavoritePage({
                    pageId,
                    solutionId: currentSolution.id,
                })
            ).then((response) => {
                res = response
            })
        } else {
            await dispatch(
                addFavoritePage({
                    pageId,
                    solutionId: currentSolution.id,
                })
            ).then((response) => {
                res = response
            })
        }
        if (res === 'ok') {
            setIsPageFavorite(!isPageFavorite)
        }
        // return res
    }

    return (
        <div className={classes.container}>
            <div className={classes.titleContainer}>
                <TextBox display="inline" variant="h3">
                    Get All Endpoints From APIC
                </TextBox>
                <SomeLogoIcon className={classes.someLogo} />
                <FavoriteIcon
                    onClick={() => handleFavoriteClick(isPageFavorite)}
                    className={`${classes.favoriteIconContainer} ${
                        isPageFavorite
                            ? classes.isFavorite
                            : classes.isNotFavorite
                    }`}
                />
            </div>
            <TextBox variant="subtitle2">
                This Page List All Fabric Discovered Endpoints
            </TextBox>
            <LeafDropdown getLeafInterfaces={(leafId, podId) =>
                dispatch(
                    getLeafInterfaces({
                        solutionId: currentSolution.id,
                        leafId,
                        podId,
                    })
                )
            } setLeaf={(val: AciLeaf) => dispatch(setLeaf(val))}/>
            <CustomTable
                totalRows={metaAndLinks?.meta?.total || 0}
                maxHeight="650px"
                containerClassName={classes.table}
                title="Fabric Discovered Endpoints"
                columns={columns}
                rows={leafInterfaces}
                onSearchChanged={handleSearch}
                exportToExcelFunction={exportToCSV}
                renderPagination={() => (
                    <CustomTablePagination
                        handleBackPress={handleBackPress}
                        handleFirstPress={handleFirstPress}
                        handleLastPress={handleLastPress}
                        handleNextPress={handleNextPress}
                        currentPage={metaAndLinks?.meta?.current_page || 1}
                        lastPage={metaAndLinks?.meta?.last_page || 1}
                        handleChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                )}
                search
                isLoading={fetchingData}
                // defaultSortBy="created"
            />
        </div>
    )
}

export default SpineLeafInterfaceStats
