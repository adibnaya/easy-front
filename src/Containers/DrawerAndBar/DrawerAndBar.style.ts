import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        pageSpinner: {
            position: 'fixed',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            zIndex: 9999,
            backgroundColor: 'rgba(0,0,0,0.5)',
        },
        spinner: {
            position: 'absolute',
            width: '300px !important',
            height: '300px !important',
            top: 'calc((100% - 300px)/2)',
            left: 'calc((100% - 300px)/2)',
            color: theme.colors.secondary,
        },
        pageContainer: {
            display: 'flex',
            flexDirection: 'row',
            height: '100%',
        },
        drawerContainer: {
            flexDirection: 'column',
            transition: 'width 0.1s',
        },
        drawerPaper: {
            overflowY: 'hidden',
        },
        appBarRow: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
        },
        userSelectContainer: { width: 'fit-content' },
        appBarSeparator: {
            height: 28,
            width: 1,
            backgroundColor: '#EBEBF0',
            margin: '0 20px',
        },
        appBarSearchContainer: {
            width: 260,
        },
        appBarTenantsContainer: {
            width: 156,
        },
        appBarLogoContainer: {
            width: 'fit-content',
            maxWidth: 300,
            height: 33,
            marginRight: 10,
        },
        appBarAndContent: {
            flexGrow: 1,
            flexDirection: 'column',
            minWidth: '1660px',
        },
        appBarSolutionsContainer: {
            width: 156,
            marginRight: 10,
        },
        drawerTab: {
            width: 130,
            minWidth: 130,
            opacity: 1,
            padding: 0,
        },
        selectedDrawerTab: {
            color: '#67B1FA',
            borderBottom: '2px solid #67B1FA',
            // paddingTop: '7px',
        },
        tabImg: {
            marginBottom: 7,
        },
        overflowY: {
            overflowY: 'auto',
        },
    })
)

export default useStyles
