import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useParams, Route } from 'react-router-dom'
import Tab from '@material-ui/core/Tab'
import { push } from 'connected-react-router'
import { useTranslation } from 'react-i18next'
import { CircularProgress } from '@material-ui/core'
import { RootState } from '../../Redux/Store'
import {
    closeDrawer,
    getUser,
    openDrawer,
    switchDrawerVariant,
    getSolutionPages,
    Solution,
    getSolutionNextDropdown,
    setCurrentSolution,
    Page,
    resetDrawer,
    setTenant,
    resetTenant,
    getFavoritePages,
    resetFavoritePages,
} from './DrawerReducer'
import CustomDrawer from '../../Components/Drawer/Drawer'
import DrawerHeader from '../../Components/Drawer/DrawerHeader/DrawerHeader'
import DrawerList from '../../Components/Drawer/DrawerList/DrawerList'
import AppBar from '../../Components/AppBar/AppBar'
import flooopLogo from '../../Assets/flooop.png'
import useStyles from './DrawerAndBar.style'
import SelectAutocomplete from '../../Components/SelectAutocomplete/SelectAutocomplete'
import SettingsPopper from '../../Components/SettingsPopper/SettingsPopper'
import DrawerTabsContainer from '../../Components/Drawer/DrawerTabsContainer/DrawerTabsContainer'
import DrawerTabContent from '../../Components/Drawer/DrawerTabContent/DrawerTabContent'
import DrawerTabPanel from '../../Components/Drawer/DrawerTabPanel/DrawerTabPanel'
import { ReactComponent as FavoriteWhiteIcon } from '../../Assets/favorite_white.svg'
import { ReactComponent as FavoriteBlueIcon } from '../../Assets/favorite_blue.svg'
import { ReactComponent as AllPagesWhiteIcon } from '../../Assets/all_pages_white.svg'
import { ReactComponent as AllPagesBlueIcon } from '../../Assets/all_pages_blue.svg'
import DrawerFavoriteList from '../../Components/Drawer/DrawerFavoriteList/DrawerFavoriteList'
import SearchAutocomplete, {
    Option,
} from '../../Components/SearchAutocomplete/SearchAutocomplete'
import Aci from '../Aci/Aci'
import { signOut } from '../Login/LoginReducer'

const getPagesForSearch = (pages: Page[], solutionId) => {
    const routes: Option[] = []
    const func = (page, parentSlug = `/solutions/${solutionId}`) => {
        if (page.sub_pages.length === 0) {
            const route = {
                key: routes.length,
                title: `${page.name}`,
                slug: `${parentSlug}/${page.slug}`,
            }

            routes.push(route)
        } else {
            page.sub_pages.forEach((p) => {
                func(p, `${parentSlug}/${page.slug}`)
            })
        }
    }
    if (pages && pages.length) {
        pages.forEach((pa) => {
            func(pa)
        })
    }

    return routes
}

const DrawerAndBar = () => {
    const dispatch = useDispatch()
    const { t } = useTranslation()
    const { router, drawer } = useSelector((state: RootState) => state)

    const {
        isDrawerOpen,
        variant,
        userName,
        solutions,
        currentSolution,
        solutionPages,
        nextDropdown,
        favoritePages,
        favoritesMetaAndLinks,
        isPageLoading,
    } = drawer

    const { solutionId } = useParams()
    const { location } = router

    useEffect(() => {
        if (!userName) {
            dispatch(getUser())
        }
        dispatch(getSolutionPages(solutionId))

        if (currentSolution && currentSolution.next_dropdown) {
            dispatch(
                getSolutionNextDropdown({
                    endpoint: currentSolution.next_dropdown.endpoint,
                    dropdownTitle: currentSolution.next_dropdown.title,
                })
            )
        }
    }, [currentSolution, dispatch, solutionId, userName])

    const [value, setValue] = React.useState(0)
    const a11yProps = (index: any) => {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        }
    }
    const handleChange = (event: React.ChangeEvent<any>, newValue: number) => {
        setValue(newValue)
    }

    const unPinDrawer = () => {
        dispatch(closeDrawer())
        dispatch(switchDrawerVariant('temporary'))
    }

    const handleSolutionChanged = ({ key }: { title: string; key: number }) => {
        const solution = solutions[key]
        dispatch(setCurrentSolution(solution))
        dispatch(resetFavoritePages())
        dispatch(getFavoritePages({ id: solution.id }))
        dispatch(push(`/solutions/${solution.id}`))
    }

    const handleChildClick = (slug: string) => {
        dispatch(push(slug))
        dispatch(closeDrawer())
    }

    const handleLogout = () => {
        dispatch(resetDrawer())
        localStorage.removeItem('token')
        dispatch(signOut())
        dispatch(push('/login'))
    }

    const handleNextDropdownClick = (tenant: {
        key: number
        title: string
    }) => {
        if (tenant.key !== -1) {
            dispatch(setTenant(tenant.title))
        } else {
            dispatch(resetTenant())
        }
    }

    const solutionIndex =
        solutions && solutions.findIndex((s) => s.id === currentSolution.id)

    const classes = useStyles()

    return (
        <>
            {isPageLoading && (
                <div className={classes.pageSpinner}>
                    <CircularProgress className={classes.spinner} />
                </div>
            )}
            <div className={classes.pageContainer}>
                <div
                    style={{
                        width: variant === 'permanent' ? 260 : 0,
                    }}
                    className={classes.drawerContainer}
                >
                    <CustomDrawer
                        open={isDrawerOpen}
                        openDrawer={() => dispatch(openDrawer())}
                        closeDrawer={() => dispatch(closeDrawer())}
                        variant={variant}
                        switchDrawerVariant={() => switchDrawerVariant()}
                    >
                        <DrawerHeader
                            variant={variant}
                            pinDrawer={() =>
                                dispatch(switchDrawerVariant('permanent'))
                            }
                            unPinDrawer={unPinDrawer}
                        />
                        <DrawerTabsContainer
                            handleTabsChange={handleChange}
                            value={value}
                        >
                            <Tab
                                disableRipple
                                classes={{
                                    root: classes.drawerTab,
                                    selected: classes.selectedDrawerTab,
                                }}
                                label={
                                    <DrawerTabContent
                                        index={0}
                                        tabsValue={value}
                                        title={t(
                                            'drawerAndBar.tabsTitle.firstTab'
                                        )}
                                        renderIcon={() => (
                                            <AllPagesWhiteIcon
                                                className={classes.tabImg}
                                            />
                                        )}
                                        renderSelectedIcon={() => (
                                            <AllPagesBlueIcon
                                                className={classes.tabImg}
                                            />
                                        )}
                                    />
                                }
                                {...a11yProps(0)}
                            />
                            <Tab
                                disableRipple
                                classes={{
                                    root: classes.drawerTab,
                                    selected: classes.selectedDrawerTab,
                                }}
                                label={
                                    <DrawerTabContent
                                        index={1}
                                        tabsValue={value}
                                        title={t(
                                            'drawerAndBar.tabsTitle.secondTab'
                                        )}
                                        renderIcon={() => (
                                            <FavoriteWhiteIcon
                                                className={classes.tabImg}
                                            />
                                        )}
                                        renderSelectedIcon={() => (
                                            <FavoriteBlueIcon
                                                className={classes.tabImg}
                                            />
                                        )}
                                    />
                                }
                                {...a11yProps(1)}
                            />
                        </DrawerTabsContainer>
                        <DrawerTabPanel
                            className={classes.overflowY}
                            value={value}
                            index={0}
                        >
                            {solutionPages && (
                                <DrawerList
                                    handleChildClick={(slug) =>
                                        handleChildClick(slug)
                                    }
                                    pathName={location.pathname}
                                    solutionId={currentSolution.id}
                                    pagesList={solutionPages}
                                />
                            )}
                        </DrawerTabPanel>
                        <DrawerTabPanel value={value} index={1}>
                            {favoritePages && favoritesMetaAndLinks && (
                                <DrawerFavoriteList
                                    hasMore={
                                        favoritesMetaAndLinks.links.next !==
                                        null
                                    }
                                    fetchMoreData={() =>
                                        dispatch(
                                            getFavoritePages({
                                                id: currentSolution.id,
                                                page:
                                                    favoritesMetaAndLinks.meta
                                                        .current_page + 1,
                                            })
                                        )
                                    }
                                    onClickPage={(pageUrl) =>
                                        dispatch(
                                            push(
                                                `/solutions/${currentSolution.id}/${pageUrl}`
                                            )
                                        )
                                    }
                                    pages={favoritePages.map((fp) => ({
                                        id: fp.id,
                                        title: fp.name,
                                        url: fp.slug,
                                    }))}
                                />
                            )}
                        </DrawerTabPanel>
                    </CustomDrawer>
                </div>
                <div
                    className={classes.appBarAndContent}
                    style={{
                        marginLeft: variant === 'permanent' ? '260px ' : 'auto',
                    }}
                >
                    <AppBar>
                        <div className={classes.appBarRow}>
                            <div className={classes.appBarLogoContainer}>
                                <img src={flooopLogo} alt="bynet logo" />
                            </div>
                            {solutions && (
                                <div
                                    className={classes.appBarSolutionsContainer}
                                >
                                    <SelectAutocomplete
                                        onOptionClicked={(val) =>
                                            handleSolutionChanged(val)
                                        }
                                        data={solutions.map(
                                            (s: Solution, i) => ({
                                                key: i,
                                                title: s.name,
                                            })
                                        )}
                                        selectedOptionIndex={
                                            solutionIndex >= 0
                                                ? solutionIndex
                                                : 0
                                        }
                                        disableClearable
                                    />
                                </div>
                            )}
                            {nextDropdown && (
                                <div className={classes.appBarTenantsContainer}>
                                    <SelectAutocomplete
                                        onOptionClicked={(r) =>
                                            handleNextDropdownClick(r)
                                        }
                                        data={nextDropdown}
                                        selectedOptionIndex={0}
                                        disableClearable
                                    />
                                </div>
                            )}
                        </div>
                        <div className={classes.appBarRow}>
                            {solutionPages && (
                                <div className={classes.appBarSearchContainer}>
                                    <SearchAutocomplete
                                        data={getPagesForSearch(
                                            solutionPages,
                                            currentSolution.id
                                        )}
                                        placeholder={t(
                                            'drawerAndBar.searchPlaceholder'
                                        )}
                                        onOptionClicked={(r) =>
                                            dispatch(push(r.slug))
                                        }
                                    />
                                </div>
                            )}
                            <div className={classes.appBarSeparator} />
                            <div>
                                <div className={classes.userSelectContainer}>
                                    <SettingsPopper
                                        name={userName}
                                        handleLogOutClick={() => handleLogout()}
                                    />
                                </div>
                            </div>
                        </div>
                    </AppBar>

                    <Route path="/solutions/:solutionId">
                        {currentSolution?.type === 'aci' && (
                            <Aci solutionPages={solutionPages} />
                        )}
                    </Route>
                </div>
            </div>
        </>
    )
}

export default DrawerAndBar
