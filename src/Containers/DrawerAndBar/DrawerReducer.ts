import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { DrawerProps } from '@material-ui/core'
import { push } from 'connected-react-router'
import api from '../../Api/Api'
import { AppThunk } from '../../Redux/Store'

export type NextDropdown = {
    key: number
    title: string
}

export type FavoritePage = {
    id: string
    order_column: number
    slug: string
    name: string
    icon_open: string | null
    icon_close: string | null
}

export type Page = {
    id: string
    order_column: number
    slug: string
    name: string
    sub_pages: Page[]
    is_favorite: boolean
    icon_open: null | string
    icon_close: null | string
}

export type Solution = {
    id: string
    name: string
    comment: string
    type: string
    nextDropdown: {
        endpoint: string
        store_name: string
        title: string
    }
}

export type Meta = {
    current_page: number
    from: number
    last_page: number
    path: string
    per_page: number
    to: number
    total: number
}

export type Links = {
    first: string
    last: string
    prev: string
    next: string
}

export type MetaAndLinks = { meta: Meta; links: Links }

type DrawerSliceState = {
    isDrawerOpen: boolean
    variant: DrawerProps['variant']
    userName: string
    solutions: Solution[] | undefined
    currentSolution: Solution | undefined
    solutionPages: Page[] | undefined
    nextDropdown: NextDropdown[] | undefined
    tenant: undefined | string
    favoritePages: FavoritePage[]
    favoritesMetaAndLinks: MetaAndLinks | undefined
    isPageLoading: boolean
}

const initialState: DrawerSliceState = {
    isDrawerOpen: false,
    variant: 'temporary',
    userName: '',
    solutions: undefined,
    currentSolution: undefined,
    solutionPages: undefined,
    nextDropdown: undefined,
    tenant: undefined,
    favoritePages: [],
    favoritesMetaAndLinks: undefined,
    isPageLoading: false,
}

const drawer = createSlice({
    name: 'drawer',
    initialState,
    reducers: {
        openDrawer: (state) => {
            state.isDrawerOpen = true // mutate the state all you want with immer
        },
        closeDrawer: (state) => {
            state.isDrawerOpen = false
        },
        resetDrawer: () => {
            return initialState
        },
        switchDrawerVariant: (
            state,
            action: PayloadAction<DrawerProps['variant']>
        ) => {
            state.variant = action.payload
        },
        setUserName: (state, action: PayloadAction<string>) => {
            state.userName = action.payload
        },
        setSolutions: (state, action: PayloadAction<Solution[]>) => {
            state.solutions = action.payload
        },
        setSolutionPages: (state, action: PayloadAction<Page[]>) => {
            state.solutionPages = action.payload
        },
        setCurrentSolution: (state, action: PayloadAction<Solution>) => {
            state.currentSolution = action.payload
        },
        setIsPageLoading: (state, action: PayloadAction<boolean>) => {
            state.isPageLoading = action.payload
        },
        setSolutionNextDropdown: (
            state,
            action: PayloadAction<NextDropdown[]>
        ) => {
            state.nextDropdown = action.payload
        },
        setTenant: (state, action: PayloadAction<string>) => {
            state.tenant = action.payload
        },
        resetTenant: (state) => {
            state.tenant = undefined
        },
        setFavoritePages: (state, action: PayloadAction<FavoritePage[]>) => {
            state.favoritePages = [...state.favoritePages, ...action.payload]
        },
        setFavoritesMetaAndLinks: (
            state,
            action: PayloadAction<MetaAndLinks>
        ) => {
            state.favoritesMetaAndLinks = action.payload
        },
        resetFavoritePages: (state) => {
            state.favoritePages = []
        },
    },
})

export const {
    openDrawer,
    closeDrawer,
    switchDrawerVariant,
    setUserName,
    setSolutions,
    setSolutionPages,
    setCurrentSolution,
    setSolutionNextDropdown,
    resetDrawer,
    setTenant,
    resetTenant,
    setFavoritePages,
    setFavoritesMetaAndLinks,
    resetFavoritePages,
    setIsPageLoading,
} = drawer.actions

export const getUser = (): AppThunk => async (dispatch) => {
    let response
    try {
        response = await api.get('/auth/me')
    } catch (e) {
        return
    }
    dispatch(setUserName(response.data.data.name))
}

export const getFavoritePages = (params: {
    id: string
    page?: number
}): AppThunk => async (dispatch) => {
    let response
    try {
        response = await api.get(
            `/solutions/${params.id}/favorites?page=${
                params.page || '1'
            }&sort=["first_name","ASC"]`
        )
    } catch (e) {
        return
    }
    dispatch(setFavoritePages(response.data.data))
    dispatch(
        setFavoritesMetaAndLinks({
            meta: response.data.meta,
            links: response.data.links,
        })
    )
}

export const getSolutionsDropdown = (): AppThunk => async (dispatch) => {
    let response
    try {
        response = await api.get('/solutions?dropdown=true&page=2')
    } catch (e) {
        return
    }
    if (response.data.data && response.data.data.length > 0) {
        dispatch(setSolutions(response.data.data))
        dispatch(setCurrentSolution(response.data.data[0]))
        dispatch(getFavoritePages({ id: response.data.data[0].id }))
        dispatch(getUser())
        dispatch(push(`/solutions/${response.data.data[0].id}`))
    }
}

export const getSolutionPages = (id: string): AppThunk => async (dispatch) => {
    let response
    try {
        dispatch(setIsPageLoading(true))
        response = await api.get(`/solutions/${id}/pages`)
        if (response && response.data && response.data.data) {
            dispatch(setSolutionPages(response.data.data))
            dispatch(setIsPageLoading(false))
        }
    } catch (e) {
        dispatch(setIsPageLoading(false))
    }
}

export const getSolutionNextDropdown = (params: {
    endpoint: string
    dropdownTitle: string
}): AppThunk => async (dispatch) => {
    let response
    try {
        response = await api.get(params.endpoint.substring(33))
    } catch (e) {
        return
    }

    const nextDropdownData = [
        { key: -1, title: params.dropdownTitle },
        ...response.data.data.map((dat, index) => ({
            key: index,
            title: dat.name,
        })),
    ]
    dispatch(setSolutionNextDropdown(nextDropdownData))
}

export const deleteFavoritePage = (params: {
    pageId: string
    solutionId: string
}): AppThunk => async (dispatch) => {
    let response
    try {
        response = await api.delete(
            `/solutions/${params.solutionId}/favorites`,
            { data: { page_id: params.pageId } }
        )
    } catch (e) {
        return ''
    }
    dispatch(resetFavoritePages())
    dispatch(setFavoritePages(response.data.data))
    dispatch(
        setFavoritesMetaAndLinks({
            meta: response.data.meta,
            links: response.data.links,
        })
    )
    return 'ok'
}

export const addFavoritePage = (params: {
    pageId: string
    solutionId: string
}): AppThunk => async (dispatch) => {
    let response
    try {
        response = await api.post(`/solutions/${params.solutionId}/favorites`, {
            page_id: params.pageId,
        })
    } catch (e) {
        return ''
    }
    dispatch(resetFavoritePages())
    dispatch(setFavoritePages(response.data.data))
    dispatch(
        setFavoritesMetaAndLinks({
            meta: response.data.meta,
            links: response.data.links,
        })
    )
    return 'ok'
}

export default drawer.reducer
