import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        container: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
            height: '100%',
            backgroundColor: 'white',
        },
        card: {
            width: 'calc(300px - 4%)',
            display: 'flex',
            alignSelf: 'center',
            backgroundColor: '#2C2D3CDB',
            padding: '20px 4%',
            borderRadius: 3,
            boxShadow: '4px 7px 29px #29292E4A',
            fontFamily: theme.fonts.primaryText,
        },
    })
)

export default useStyles
