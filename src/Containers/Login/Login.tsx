import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { login, toggleToast } from './LoginReducer'
import LoginForm, {
    LoginFormValues,
} from '../../Components/LoginForm/LoginForm'
import useStyles from './Login.style'
import Toast from '../../Components/Toast/Toast'
import { RootState } from '../../Redux/Store'

type HandleLogin = (cred: LoginFormValues) => Promise<string>

const Login = () => {
    const dispatch = useDispatch()
    const classes = useStyles()
    const handleLogin: HandleLogin = async (cred) => {
        await dispatch(login(cred)).catch((serializedError) => {
            dispatch(toggleToast())
            return serializedError.message
        })
        return ''
    }

    const { isToastOpen } = useSelector((state: RootState) => state.user)

    return (
        <div className={classes.container}>
            <div className={classes.card}>
                <LoginForm
                    handleSubmit={(cred) => handleLogin(cred)}
                    message="Login"
                />
            </div>
            <div>
                <div style={{ marginTop: 20 }}>
                    <Toast
                        isOpen={isToastOpen}
                        toggleToast={() => dispatch(toggleToast())}
                        severity="error"
                        text="The username or password is incorrect!"
                    />
                </div>
            </div>
        </div>
    )
}

export default Login
