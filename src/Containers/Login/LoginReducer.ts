import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import api from '../../Api/Api'
import { LoginFormValues } from '../../Components/LoginForm/LoginForm'
import {
    getSolutionsDropdown,
    resetDrawer,
} from '../DrawerAndBar/DrawerReducer'
import { AppThunk } from '../../Redux/Store'

type UserSliceState = {
    isLoading: boolean
    error: string
    isAuthenticated: boolean
    user: any
    isToastOpen: boolean
}

const initialState: UserSliceState = {
    isLoading: false,
    error: '',
    isAuthenticated: true,
    user: {},
    isToastOpen: false,
}

const user = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setIsAuthenticated: (state, action: PayloadAction<boolean>) => {
            state.isAuthenticated = action.payload
        },
        startLoading: (state) => {
            state.isLoading = true
        },
        hasError: (state, action) => {
            state.error = action.payload
            state.isLoading = false
        },
        successSignIn: (state) => {
            state.isAuthenticated = true
            state.isLoading = false
        },
        signOut: (state) => {
            state.isAuthenticated = false
        },
        toggleToast: (state) => {
            state.isToastOpen = !state.isToastOpen
        },
    },
    extraReducers: (builder) => {
        // builder.addCase(login.pending, (state, action) => {
        //     state.isAuthenticated = true
        //     // both `state` and `action` are now correctly typed
        //     // based on the slice state and the `pending` action creator
        // })
        // builder.addCase(login.fulfilled, (state, action) => {
        //     // both `state` and `action` are now correctly typed
        //     // based on the slice state and the `pending` action creator
        // })
    },
})

export const {
    successSignIn,
    signOut,
    hasError,
    setIsAuthenticated,
    toggleToast,
} = user.actions

//      userprincipalname: 'easyRO@ps-lab.local',
//     password: 'a:123456',
export const login = (cred: LoginFormValues): AppThunk => async (dispatch) => {
    localStorage.removeItem('token')
    dispatch(resetDrawer())
    let response
    try {
        response = await api.post('/auth/login', {
            userprincipalname: cred.email,
            password: cred.password,
        })
    } catch (e) {
        dispatch(toggleToast())
        return
    }
    localStorage.setItem('token', response.data.token)
    dispatch(successSignIn())
    dispatch(getSolutionsDropdown())
}

export default user.reducer
