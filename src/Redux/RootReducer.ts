import { combineReducers } from '@reduxjs/toolkit'
import { connectRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import drawerReducer from '../Containers/DrawerAndBar/DrawerReducer'
import loginReducer from '../Containers/Login/LoginReducer'
import aciDashboardReducer from '../Containers/Aci/Dashboard/DashboardReducer'
import getAllEndpointsReducer from '../Containers/Aci/GetAllEndpoints/GetAllEndpointsReducer'
import dseReducer from '../Containers/Aci/DeployStandardEndpoint/DeployStandardEndpointReducer'
import leafInterfaceDetails from '../Containers/Aci/LeafInterfaceDetails/LeafInterfaceDetailsReducer'
import aciReducer from '../Containers/Aci/AciReducer'
import spineLeafInterfaceStats from '../Containers/Aci/SpineLeafInterfaceStats/SpineLeafInterfaceStatsReducer'

export const history = createBrowserHistory()

const rootReducer = combineReducers({
    router: connectRouter(history),
    drawer: drawerReducer,
    user: loginReducer,
    aciDashboard: aciDashboardReducer,
    dse: dseReducer,
    getAllEndpoints: getAllEndpointsReducer,
    leafInterfaceDetails: leafInterfaceDetails,
    spineLeafInterfaceStats: spineLeafInterfaceStats,
    aci: aciReducer,
})

export default rootReducer
