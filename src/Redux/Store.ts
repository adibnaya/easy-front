import {
    Action,
    configureStore,
    getDefaultMiddleware,
    ThunkAction,
} from '@reduxjs/toolkit'
import LogRocket from 'logrocket'
import { routerMiddleware } from 'connected-react-router'
import storage from 'redux-persist/lib/storage'
import {
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist'
import rootReducer, { history } from './RootReducer'

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['drawer', 'dse'], // Things u want to persist
    // blacklist: [], // Things u dont
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const logRocketMiddleware = LogRocket.reduxMiddleware()
const middlewares = [logRocketMiddleware, routerMiddleware(history)]

const store = configureStore({
    reducer: persistedReducer,
    middleware: [
        ...getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [
                    FLUSH,
                    REHYDRATE,
                    PAUSE,
                    PERSIST,
                    PURGE,
                    REGISTER,
                ],
            },
        }),
        ...middlewares,
    ],
})
export type AppDispatch = typeof store.dispatch

export default store
export type RootState = ReturnType<typeof store.getState>

export type AppThunk = ThunkAction<void, RootState, unknown, Action<string>>
