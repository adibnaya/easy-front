const COLORS = {
    blue: '#67B1FA',
    deepBlue: '#3E7BB7',
    deepGray: '#40414E',
    darkGray: '#262C31',
    lightBlack: '#1C1C1C',
    white: '#FFFFFF',
    lightGray: '#F4F5FC',
}

export default COLORS
