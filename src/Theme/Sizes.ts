const SIZES = {
    title26: '26px',
    primaryText: '16px',
    mediumText: '14px',
    secondaryText: '13px',
    smallText: '12px',
}

export default SIZES
