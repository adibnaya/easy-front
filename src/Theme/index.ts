import { createMuiTheme } from '@material-ui/core/styles'
import COLORS from './Colors'
import SIZES from './Sizes'
import FONTS from './Fonts'

declare module '@material-ui/core/styles/createMuiTheme' {
    interface Theme {
        colors: {
            primary: string
            secondary: string
            selectText: string
            selectTextSelected: string
            selectBackground: string
            white: string
            drawerHeader: string
            drawerTabsText: string
            drawerItemBackground: string
        }
        sizes: {
            title26: string
            primaryText: string
            secondaryText: string
            mediumText: string
            smallText: string
        }
        fonts: {
            primaryText: string
        }
    }
    // allow configuration using `createMuiTheme`
    export interface ThemeOptions {
        colors?: {
            primary: string
            secondary: string
            selectText: string
            selectTextSelected: string
            selectBackground: string
            white: string
            drawerHeader: string
            drawerTabsText: string
            drawerItemBackground: string
        }
        sizes?: {
            title26: string
            primaryText: string
            secondaryText: string
            mediumText: string
            smallText: string
        }
        fonts?: {
            primaryText: string
        }
    }
}
const theme = createMuiTheme({
    colors: {
        primary: COLORS.blue,
        secondary: COLORS.deepBlue,
        selectText: COLORS.deepGray,
        selectTextSelected: COLORS.white,
        selectBackground: COLORS.lightGray,
        white: COLORS.white,
        drawerHeader: COLORS.darkGray,
        drawerTabsText: COLORS.blue,
        drawerItemBackground: COLORS.lightBlack,
    },
    sizes: {
        title26: SIZES.title26,
        primaryText: SIZES.primaryText,
        secondaryText: SIZES.secondaryText,
        mediumText: SIZES.mediumText,
        smallText: SIZES.smallText,
    },
    fonts: {
        primaryText: FONTS.primaryText,
    },
})
export default theme
