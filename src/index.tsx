import * as React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { ThemeProvider } from '@material-ui/core/styles'
import LogRocket from 'logrocket'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { persistStore } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import App from './App/App'
import * as serviceWorker from './serviceWorker'
import theme from './Theme'
import store from './Redux/Store'
import { history } from './Redux/RootReducer'
import './i18n'

LogRocket.init('k4effv/bynet')
const persistor = persistStore(store)

ReactDOM.render(
    // <React.StrictMode>
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <PersistGate loading={null} persistor={persistor}>
                <ThemeProvider theme={theme}>
                    <App />
                </ThemeProvider>
            </PersistGate>
        </ConnectedRouter>
    </Provider>,
    // </React.StrictMode>,
    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
